from input_data_file import InputDataSettings, DataArray
from copy import deepcopy
import numpy as np
import matplotlib.pyplot as plt
from make_plots import MakePlot
from pathlib import Path as Os_path
import pickle
from useful_methods import Usefulmethods
import collections

def clear_list_none(l):
    out = []
    for ll in l:
        if ll is None:
            out.append(np.nan)
            continue
        if isinstance(ll, list):
             ll = clear_list_none(ll)
             if not ll:
                 continue
        out.append(ll)
    return out

def memoizelistofdict(function):
    print('function', function)
    def wrapper(*args, **kwargs):
        """ args and kwargs must be dict
        This is not a general function, args must be:
        1. self or nothing, so a metod or funciton can be used
        2. filename to load/store
        3. The args to memoize

        kwargs are other general things not taken into account for memoizer, they must not effect the answer if changed
        """

        # Find if a method or function args[0] is self if a method
        if hasattr(args[0], '__dict__'):#isinstance(args[0], MainRun):
            arguments_sep = args[1:]
        else:
            arguments_sep = args

        filename = arguments_sep[0]

        filename_data_pl = Os_path(filename)
        if filename_data_pl.is_file():
            cache = pickle.load(open(str(filename_data_pl), "rb"))
            cache_to_be_done_first_time = False
        else:
            cache = {'input_args':([]), 'output_vals':[]}
            cache_to_be_done_first_time = True
        args_memoize = arguments_sep[1:]
        args_used = args_memoize
        # Check if the args match the args in the cache
        redo_cache = False
        for i in range(len(args_used)):

            if not cache['input_args']:
                redo_cache = True
            elif args_used[i]: # not an empty list
                args_used_i = clear_list_none(args_used[i])
                cache_used_i = clear_list_none(cache['input_args'][i])
                try:
                    allclose = [np.allclose(x, y, equal_nan=True) for x, y in zip(args_used_i, cache_used_i)]
                    if not np.all(allclose):
                        redo_cache = True
                except:
                    redo_cache = True
        if cache_to_be_done_first_time:
            redo_cache = True
        if kwargs['overwrite_cache_and_run']:
            redo_cache = True
        if redo_cache:
            result_out = deepcopy(function(*args, **kwargs))
            result_dict = {'input_args':args_used, 'output_vals':result_out}
            # update the cache file
            pickle.dump(result_dict, open(str(filename), "wb"))
            result = result_dict['output_vals']
        else:
            result = cache['output_vals']
        return result
    return wrapper
    return decorator

class EmptyLoaderClass:
    def __init__(self, input_data_obj_list, data_type_to_index_dict):
        self.data_empty = []
        self.time_empty = []
        self.error_empty = None
        self.lowest_z_range = [-0.5, 0.0]

class AnalysisObj:
    def __init__(self):
        self.x = np.array([1.0, 2.2, 3.5, 2.5, 4.1])
        self.x1 = np.array([1.2, 2.4, 3.7, 2.7, 4.3])
        self.x_err = np.array([.10, .22, .35, .25, .41])
        self.time = np.array([51.0, 51.5, 52.0, 52.5, 53.0])
        self.y = np.array([5.0, 5.3, 5.9, 6.1, 6.5])
        self.z = np.array([1.0, 2.3, 4.9, 6.1, 6.7])

        self.te = np.arange(0, 8, 0.2)
        self.n = np.tile(np.arange(1, 5, 0.1), (5, 1))
        self.r = np.linspace(0, 100, 5)
        self.r1 = np.linspace(0, 100, 5)
        self.te1 = np.linspace(0, 8, 5)
        self.t_bounds = [51.9, 52.5]
        self.test_label_x = 0.1
        self.test_label_y = 0.2
        self.test_label_z = 'Print string'

        self.test_label_x2 = 0.4
        self.test_label_y2 = 0.4
        self.test_label_z2 = 'Print string2'


class ClassAllDataObj:
    def __init__(self, input_data_obj_list, data_type_to_index_dict):
        self.analysis_obj = AnalysisObj()

    def get_filter_prerequisite(self, input_data_obj_list, input_data_settings, filter_source_key):
        raise ValueError("no default, you need to overwrite this class in the one you use if you want to filter data")


class MainRun:
    """

    """
    def __init__(self, InputDataSettingsOverW, settings_args_dict_list, subsequent_class_dict_list=[[{}]]):
        expand_different_input_list = np.arange(0, np.shape(subsequent_class_dict_list)[0], 1)
        input_data_settings_list = []
        input_data_obj_list_of_list = []
        current_uid_list = None
        for k in range(len(expand_different_input_list)):
            # Intialise the data object
            input_data_settings = InputDataSettingsOverW(**settings_args_dict_list[k])
            self.cache_path = input_data_settings.cache_path
            Usefulmethods.path_check_and_make(str(self.cache_path))

            input_data_obj = DataArray()
            # Make a list for of all the required fields to load
            input_data_obj_list = [deepcopy(input_data_obj) for _ in range(input_data_settings.n_signals_added)]
            for key, index in input_data_settings.data_type_to_index_dict.items():
                input_data_obj_list[index].key = key
                input_data_obj_list[index].get_data_settings(key, input_data_settings.dict_of_add_load_info, input_data_settings.dict_of_add_basic_info)

            for i in range(len(subsequent_class_dict_list[k])):  # loop through all other classes added
                subsequent_class_dict_list[k][i]['class_args']['input_data_obj_list'] = input_data_obj_list
                subsequent_class_dict_list[k][i]['class_args']['data_type_to_index_dict'] = input_data_settings.data_type_to_index_dict

                index_to_save_list = []
                for key, index in input_data_settings.data_type_to_index_dict.items():
                    if input_data_obj_list[index].required_class_to_load == subsequent_class_dict_list[k][i]['class_str']:
                        index_to_save_list.append(index)
                print('Running class ', subsequent_class_dict_list[k][i]['class_str'], index_to_save_list, k)
                list_dict_of_data = self.load_vals_to_list(subsequent_class_dict_list[k][i], index_to_save_list, input_data_obj_list, current_uid_list, input_data_settings.data_type_to_index_dict)
                if len(list_dict_of_data) != len(index_to_save_list):
                    cache_overwrite = True #If the number of saved or requested variables is changed
                else:
                    cache_overwrite = False
                if cache_overwrite:
                    list_dict_of_data = self.load_vals_to_list(subsequent_class_dict_list[k][i], index_to_save_list,
                                                               input_data_obj_list, current_uid_list,
                                                               input_data_settings.data_type_to_index_dict,
                                                               cache_overwrite=cache_overwrite)
                for which_i in range(len(index_to_save_list)):
                    which_i_data_obj_index = index_to_save_list[which_i]
                    for keys, values in list_dict_of_data[which_i].items():
                        setattr(input_data_obj_list[which_i_data_obj_index], keys, values)

                # Second set stuff
                index_to_save_list_second = []
                for key, index in input_data_settings.data_type_to_index_dict.items():
                    for other_part_index in range(len(input_data_settings.dict_of_add_load_info[key]['load_obj_next_list'])):
                        if input_data_obj_list[index].load_obj_next_required_class[other_part_index] == subsequent_class_dict_list[k][i]['class_str']:
                            index_to_save_list_second.append(index)
                if len(index_to_save_list_second) > 0:
                    list_dict_of_data_second = self.load_vals_to_list(subsequent_class_dict_list[k][i], index_to_save_list_second, input_data_obj_list, current_uid_list, input_data_settings.data_type_to_index_dict, secondary=True)
                for which_i in range(len(index_to_save_list_second)):
                    which_i_data_obj_index = index_to_save_list_second[which_i]
                    for keys, values in list_dict_of_data_second[which_i].items():
                        setattr(input_data_obj_list[which_i_data_obj_index], keys, values)

                # Adding Channels (miving away from this)
                add_basic_info_dict = {}
                join_type_to_index_dict = {}
                join_load_info_dict = {}
                for key, index in input_data_settings.data_type_to_index_dict.items():
                    if input_data_obj_list[index].required_class_to_load == subsequent_class_dict_list[k][i]['class_str']:
                        add_basic_info, join_type_to_index, join_load_info = input_data_obj_list[
                            index].add_channel_split_vals(input_data_obj_list, input_data_settings)
                        add_basic_info_dict = {**add_basic_info_dict, **add_basic_info}
                        join_type_to_index_dict = {**join_type_to_index_dict, **join_type_to_index}
                        join_load_info_dict = {**join_load_info_dict, **join_load_info}

                input_data_settings.data_type_to_index_dict = {**input_data_settings.data_type_to_index_dict,
                                                               **join_type_to_index_dict}
                input_data_settings.dict_of_add_basic_info = {**input_data_settings.dict_of_add_basic_info,
                                                              **add_basic_info_dict}
                input_data_settings.dict_of_add_load_info = {**input_data_settings.dict_of_add_load_info,
                                                             **join_load_info_dict}

                # Adding Time delay copies (mostly unused)
                for key, index in input_data_settings.data_type_to_index_dict.items():
                    if input_data_obj_list[index].required_class_to_load == subsequent_class_dict_list[k][i]['class_str']:
                        input_data_obj_list[index].add_time_delay_vals(input_data_obj_list, input_data_settings.data_type_to_index_dict)

                # Adding Filtered data
                for key, index in input_data_settings.data_type_to_index_dict.items():
                    if input_data_obj_list[index].required_class_to_load == subsequent_class_dict_list[k][i]['class_str']:
                        input_data_obj_list[index].add_filtered_vals(input_data_obj_list,
                                                                     input_data_settings.data_type_to_index_dict,
                                                                     input_data_settings.filter_around_source)

                # Adding Smoothed data
                for key, index in input_data_settings.data_type_to_index_dict.items():
                    if input_data_obj_list[index].required_class_to_load == subsequent_class_dict_list[k][i]['class_str']:
                        input_data_obj_list[index].add_smoothed_vals(input_data_obj_list,
                                                                 input_data_settings.data_type_to_index_dict)

                # Adding Interped data
                for key, index in input_data_settings.data_type_to_index_dict.items():
                    if input_data_obj_list[index].required_class_to_load == subsequent_class_dict_list[k][i]['class_str']:
                        input_data_obj_list[index].add_interped_vals(input_data_obj_list, input_data_settings.data_type_to_index_dict)
                        # if input_data_obj_list[index].add_interped:
                        #     if input_data_obj_list[index].key_interped == 'interferometer_data_lid5_filtered_ftype_0_smoothed_smtype_0':
                        #         print(input_data_obj_list[index].key)
                        #         print(input_data_obj_list[index].data)
                        #         print(input_data_obj_list[index].error)
                        #         quit()
                # Adding splitting into time ranges
                for key, index in input_data_settings.data_type_to_index_dict.items():
                    if input_data_obj_list[index].required_class_to_load == subsequent_class_dict_list[k][i]['class_str']:
                        input_data_obj_list[index].add_channel_split_vals_self_split(input_data_obj_list, input_data_settings.data_type_to_index_dict)
                        # if input_data_obj_list[index].add_split_channel:
                        #     if input_data_obj_list[index].key_source == 'interferometer_data_lid5_filtered_ftype_0_smoothed_smtype_0_interped_z_new_fc':
                        #         print(input_data_obj_list[index].key)
                        #         print(input_data_obj_list[index].data)
                        #         print(input_data_obj_list[index].error)
                        #         quit()
                check_key_just_loaded = False
                if check_key_just_loaded:
                    key_checkjl = 'ywd'#'z_new_'+'fc'+'_smoothed_smtype_5'+'_norm_between_zt_to_zx'
                    key_checkjl_index = input_data_settings.data_type_to_index_dict[key_checkjl]
                    if input_data_obj_list[key_checkjl_index].required_class_to_load == subsequent_class_dict_list[k][i]['class_str']:
                        object_check = input_data_obj_list[key_checkjl_index]
                        print('object_check found in class', input_data_obj_list[key_checkjl_index].required_class_to_load, object_check.data, object_check.time, np.shape(object_check.time), np.shape(object_check.data))
                        raise ValueError("check_done")
                        quit()
                    else:
                        print('class expected:')#, input_data_obj_list[key_checkjl_index].required_class_to_load, key_checkjl_index)
            input_data_obj_list = np.array(input_data_obj_list)
            input_data_settings_list.append(input_data_settings)
            input_data_obj_list_of_list.append(input_data_obj_list)

        self.expand_different_input_list = expand_different_input_list
        self.input_data_obj_list_of_list = input_data_obj_list_of_list
        self.input_data_settings_list = input_data_settings_list
        check_key = False
        if check_key:
            key_check = 'nu_hrts_filtered_ftype_1'
            for k in range(len(expand_different_input_list)):
                object_check = input_data_obj_list[input_data_settings.data_type_to_index_dict[key_check]]
                print(object_check.data, object_check.time)
            quit()
########################################
        # Update min_max_value_list with min_max_value_list_keys
        for k in range(len(expand_different_input_list)):
            for key, index in input_data_settings_list[k].data_type_to_index_dict.items():
                for bounds_i_list in range(len(input_data_obj_list_of_list[k][index].min_max_value_list)):
                    bound_list_of_list = input_data_obj_list_of_list[k][index].min_max_value_list[bounds_i_list]
                    for bounds_i in range(len(bound_list_of_list)):
                        if isinstance(bound_list_of_list[bounds_i][0], str): # If the bound is a string it must be a key
                            #TODO make a way to use data or time, use data for now
                            index_reference_data = input_data_settings_list[k].data_type_to_index_dict[bound_list_of_list[bounds_i][0]]
                            data_referenced = input_data_obj_list_of_list[k][index_reference_data].data
                            if isinstance(data_referenced, collections.Iterable):
                                raise ValueError("must be a scalar")
                            input_data_obj_list_of_list[k][index].min_max_value_list[bounds_i_list][bounds_i][0] = data_referenced
                        if isinstance(bound_list_of_list[bounds_i][1], str): # If the bound is a string it must be a key
                            #TODO make a way to use data or time, use data for now
                            index_reference_data = input_data_settings_list[k].data_type_to_index_dict[bound_list_of_list[bounds_i][1]]
                            data_referenced = input_data_obj_list_of_list[k][index_reference_data].data
                            if isinstance(data_referenced, collections.Iterable):
                                raise ValueError("must be a scalar")
                            input_data_obj_list_of_list[k][index].min_max_value_list[bounds_i_list][bounds_i][1] = data_referenced
                        bound_list_of_list[bounds_i][1]

        for k in range(len(expand_different_input_list)):
            for key_save, dict_save_set in input_data_settings_list[k].dict_save_single_index.items():
                if dict_save_set['save_on']:
                    if dict_save_set['save_type'] == 'npz_all':
                        self.save_npz_all(key_save, dict_save_set, input_data_settings_list[k], input_data_obj_list_of_list[k])

        # Make the plot object - setup only once
        # Now we have a list with all the data types requested
        plot_obj_list = np.empty(input_data_settings_list[k].n_plots_added, dtype=object)
        k = 0
        for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
            print('key_plot', key_plot)
            if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                plot_obj_list[index_plot] = MakePlot(
                    input_data_settings_list[k].dict_of_plot_settings[key_plot],
                    max_index_repeated=len(expand_different_input_list)
                )

        for k in range(len(expand_different_input_list)):
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    c_list_bool = np.full((np.shape(input_data_obj_list_of_list[k])), False)
                    for j in range(len(input_data_obj_list_of_list[k])):
                        if key_plot in input_data_obj_list_of_list[k][j].list_of_plots_used:
                            c_list_bool[j] = True
                    plot_obj_list[index_plot].add_data_unordered(input_data_obj_list_of_list[k][c_list_bool], rep_indx=k)
                    input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"] = plot_obj_list[index_plot].plot_on  # Update the plot on off if no data added
                    plot_obj_list[index_plot].get_ax_order_data_and_labels(rep_indx=k, figure_title=input_data_settings_list[k].figure_title)
                    plot_obj_list[index_plot].get_ax_order_dicts(rep_indx=k)
                    plot_obj_list[index_plot].get_ax_order_dicts(rep_indx=k, use_twin_ax=True)

            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    plot_obj_list[index_plot].do_overplotting(plot_obj_list[index_plot].data_plot_obj_list[k].stored_y, 1, rep_indx=k)  # plot_obj_list[index_plot].stored_y, 1 means that it takes from the ydatas settings for the plot)
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    plot_obj_list[index_plot].make_settings_dict(plot_obj_list[index_plot].data_plot_obj_list[k].stored_y, 1, rep_indx=k)  # plot_obj_list[index_plot].stored_y, 1 means that it takes from the ydatas settings for the plot
                    plot_obj_list[index_plot].make_settings_dict(plot_obj_list[index_plot].data_plot_obj_list[k].stored_y, 1, rep_indx=k, use_twin_ax=True)  # plot_obj_list[index_plot].stored_y, 1 means that it takes from the ydatas settings for the plot
                    plot_obj_list[index_plot].reset_data_unused_ax(rep_indx=k)

        for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
            if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                plot_obj_list[index_plot].get_plot_index()
        for k in range(len(expand_different_input_list)):
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    plot_obj_list[index_plot].get_ax_shifts(rep_indx=k)
        for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
            if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                plot_obj_list[index_plot].make_ax(max_repeated_ax=len(expand_different_input_list))
        for k in range(len(expand_different_input_list)):
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    plot_obj_list[index_plot].index_data(rep_indx=k)
                    plot_obj_list[index_plot].index_data(rep_indx=k, use_twin_ax=True)
                    plot_obj_list[index_plot].reformat_data(rep_indx=k)  # must be done before doing any axis normalisation
        for k in range(len(expand_different_input_list)):
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    plot_obj_list[index_plot].make_twin_ax(rep_indx=k)
        for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
            if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                plot_obj_list[index_plot].twin_ax_share_y()
        for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
            if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                plot_obj_list[index_plot].index_data_gen()
                plot_obj_list[index_plot].set_title_str()

        for k in range(len(expand_different_input_list)):
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    plot_obj_list[index_plot].calculate_where_ranges(rep_indx=k)  # must be done before doing any axis normalisation
                    plot_obj_list[index_plot].calculate_where_ranges(rep_indx=k, use_twin_ax=True)  # must be done before doing any axis normalisation
        for k in range(len(expand_different_input_list)):
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    plot_obj_list[index_plot].set_where_ranges(rep_indx=k)  # must be done before doing any axis normalisation
                    plot_obj_list[index_plot].set_where_ranges(rep_indx=k, use_twin_ax=True)  # must be done before doing any axis normalisation
        for k in range(len(expand_different_input_list)):
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    plot_obj_list[index_plot].find_min_max(rep_indx=k)
                    plot_obj_list[index_plot].find_min_max(rep_indx=k, use_twin_ax=True)
        for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
            if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                plot_obj_list[index_plot].update_min_max_gen_sizing()
                plot_obj_list[index_plot].colour_updater()

        for k in range(len(expand_different_input_list)):
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    plot_obj_list[index_plot].make_dict_kwargs(plot_obj_list[index_plot].data_plot_obj_list[k].stored_y, 1, rep_indx=k)  # plot_obj_list[index_plot].stored_y, 1 means that it takes from the ydatas settings for the plot
                    plot_obj_list[index_plot].make_dict_kwargs(plot_obj_list[index_plot].data_plot_obj_list[k].stored_y, 1, rep_indx=k, use_twin_ax=True)  # plot_obj_list[index_plot].stored_y, 1 means that it takes from the ydatas settings for the plot
                    plot_obj_list[index_plot].update_find_min_max(rep_indx=k)
                    plot_obj_list[index_plot].update_find_min_max(rep_indx=k, use_twin_ax=True)
                    plot_obj_list[index_plot].label_change(rep_indx=k)
                    plot_obj_list[index_plot].label_change(rep_indx=k, use_twin_ax=True)
                    plot_obj_list[index_plot].add_legend_label(rep_indx=k, x_or_y_or_z='y')
                    plot_obj_list[index_plot].add_legend_label(rep_indx=k, x_or_y_or_z='y', use_twin_ax=True)
                    plot_obj_list[index_plot].colorbar_generation(rep_indx=k)
        for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
            if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                plot_obj_list[index_plot].legend_sizing() # TODO swap to before plot_data
                plot_obj_list[index_plot].ax_specific_setting_updater()
                plot_obj_list[index_plot].ax_specific_setting_updater(use_twin_ax=True)

        for k in range(len(expand_different_input_list)):
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                   plot_obj_list[index_plot].plot_data(rep_indx=k)
                   plot_obj_list[index_plot].plot_data(rep_indx=k, use_twin_ax=True)

        for k in range(len(expand_different_input_list)):
            for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
                if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                    print(plot_obj_list[index_plot].plot_key)
                    plot_obj_list[index_plot].decorate_plot(rep_indx=k)
                    plot_obj_list[index_plot].decorate_plot(rep_indx=k, use_twin_ax=True)
                    plot_obj_list[index_plot].save_signal(rep_indx=k)
        for key_plot, index_plot in input_data_settings_list[k].plot_to_index_dict.items():
            if input_data_settings_list[k].dict_of_plot_settings[key_plot]["plot_on"]:
                plot_obj_list[index_plot].save_fig(input_data_settings.save_settings.gen_path)
        plt.show()

    def load_vals_to_list(self, subsequent_class_dict, index_to_save_list, input_data_obj_list, current_uid_list, data_type_to_index_dict, secondary=False, cache_overwrite=False):
        list_of_dict_of_data = []
        if secondary:
            extra_str = '_secondary'
        else:
            extra_str = ''
        parts = [subsequent_class_dict['class_str'] + extra_str + '_cache.p']
        filename = Os_path(self.cache_path).joinpath(*parts)
        data_array = []
        error_array = []
        time_array = []
        subsequent_class_dict_class_args = subsequent_class_dict['class_args']
        actually_used = False
        for key, val in subsequent_class_dict['class_args'].items():
            if key in subsequent_class_dict['is_key_in_list']:
                for data_key in subsequent_class_dict['class_args'][key]:
                    data_array.append(input_data_obj_list[data_type_to_index_dict[data_key]].data)
                    error_array.append(input_data_obj_list[data_type_to_index_dict[data_key]].error)
                    time_array.append(input_data_obj_list[data_type_to_index_dict[data_key]].time)
                    actually_used = True
        if subsequent_class_dict['debug']:
            print("debug", subsequent_class_dict['is_key_in_list'], subsequent_class_dict['class_args'].keys(), actually_used)

        if cache_overwrite:
            cache_overwrite_use = cache_overwrite
        else:
            cache_overwrite_use = subsequent_class_dict['overwrite_cache_and_run']
        list_of_dict_of_data = self.loader_to_memoize(filename, data_array, error_array, time_array, overwrite_cache_and_run=cache_overwrite_use, subsequent_class_dict_class_args=subsequent_class_dict_class_args, index_to_save_list=index_to_save_list, input_data_obj_list=input_data_obj_list, current_uid_list=current_uid_list, subsequent_class=subsequent_class_dict['class_in'], secondary=secondary)
        return list_of_dict_of_data

    @memoizelistofdict
    def loader_to_memoize(self, filename, data_array, error_array, time_array, overwrite_cache_and_run=False, subsequent_class_dict_class_args=None, index_to_save_list=None, input_data_obj_list=None, current_uid_list=None, subsequent_class=None, secondary=False):
        list_of_dict_of_data = []
        class_new = subsequent_class(**subsequent_class_dict_class_args)  # call the class
        for index_in in index_to_save_list:
            dict_of_data = input_data_obj_list[index_in].load_vals(object_in=class_new, current_uid_list=current_uid_list, secondary=secondary)
            list_of_dict_of_data.append(dict_of_data)
        return list_of_dict_of_data

    def save_npz_all(self, key_save, dict_save_set, input_data_settings, input_data_obj_list):
        """
        np.savez_compressed(filename, **dict_kwd_arguments)
        """
        dict_kwd_arguments = {}
        for i in range(len(dict_save_set['list_save_signals'])):
            dict_kwd_arguments[dict_save_set['list_save_signals'][i]+'_data'] = input_data_obj_list[input_data_settings.data_type_to_index_dict[dict_save_set['list_save_signals'][i]]].data
            dict_kwd_arguments[dict_save_set['list_save_signals'][i]+'_time'] = input_data_obj_list[input_data_settings.data_type_to_index_dict[dict_save_set['list_save_signals'][i]]].time
            dict_kwd_arguments[dict_save_set['list_save_signals'][i]+'_error'] = input_data_obj_list[input_data_settings.data_type_to_index_dict[dict_save_set['list_save_signals'][i]]].error
            dict_kwd_arguments[dict_save_set['list_save_signals'][i]+'_label'] = input_data_obj_list[input_data_settings.data_type_to_index_dict[dict_save_set['list_save_signals'][i]]].label

        parts = [str(input_data_settings.pulse)+'_'+key_save+'.npz']
        filename_gen = Os_path(input_data_settings.gen_path).joinpath(*parts)
        print("saving to :", filename_gen)
        np.savez_compressed(filename_gen, **dict_kwd_arguments)


if __name__ == '__main__':
    subsequent_class_dict_list_of_list = []
    repeats_list = [0]
    settings_args_dict_list = []
    for i in range(len(repeats_list)):
        settings_args_dict_list.append({})
    for i in range(len(repeats_list)):
        subsequent_class_dict_list = []

        subsequent_class_dict = {}
        subsequent_class_dict['class_in'] = ClassAllDataObj  # Don't input actual data, just links from outside
        subsequent_class_dict['class_str'] = 'ClassAllDataObj'
        subsequent_class_dict['class_args'] = {}
        subsequent_class_dict['is_key_in_list'] = []
        subsequent_class_dict['debug'] = False
        subsequent_class_dict['overwrite_cache_and_run'] = False
        subsequent_class_dict_list.append(subsequent_class_dict)

        ######################################## End
        subsequent_class_dict_list_of_list.append(subsequent_class_dict_list)

    main = MainRun(InputDataSettings, settings_args_dict_list=settings_args_dict_list, subsequent_class_dict_list=subsequent_class_dict_list_of_list)
