import numpy as np
import matplotlib.pyplot as plt
from matplotlib import text
from matplotlib.patches import Circle
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredDrawingArea
from useful_methods import UsefulClassSwapping, UsefulPlottingMethods, UsefulColorPallets, UsefulSIUnitConverter, PlotColourZ, UsefulStringMethods
import warnings
from pathlib import Path as Os_path
from copy import deepcopy
from matplotlib import rc, rcParams
from matplotlib.afm import AFM
from collections.abc import Iterable
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
# import matplotlib.font_manager
# flist = matplotlib.font_manager.get_fontconfig_fonts()
# names = [matplotlib.font_manager.FontProperties(fname=fname).get_name() for fname in flist]
latex_on = True
if latex_on:
    rc('font',**{'family':'Noto Serif'})
    rc('text', usetex=True)
    rc('axes', linewidth=1.5)  # mpl.rcParams['axes.linewidth'] = 1.
    rc('font', weight='bold')
    rcParams['text.latex.preamble'] = [r'\boldmath']
# Change tick size
rc('xtick', labelsize=20)#plt.rcParams['xtick.labelsize']=8
rc('ytick', labelsize=20)#plt.rcParams['xtick.labelsize']=8

# quit()
class DiagEachMethod(object):
    """
        Holds the data to Plot to
        """

    def __init__(self, plot_key):
        self.plot_key = plot_key
        self.current_test = 0

    def standard_print(self, ran=False):
        if ran:
            print('plot:', self.plot_key, 'current_test', str(self.current_test), 'Test has no result', self.prog_index_list_unfixed, self.method_string, self.result, self.value_to_test)
        else:
            print('plot:', self.plot_key, 'current_test', str(self.current_test), 'Test ran', self.prog_index_list_unfixed, self.method_string, self.result, self.value_to_test)

    def test_run(self, object, prog_index_list_unfixed):
        self.prog_index_list_unfixed = prog_index_list_unfixed
        index_list_fixed = [0, 0]
        value_to_test1 = getattr(object, 'data_plot_obj_list')[prog_index_list_unfixed[0]]
        # value_to_test = value_to_test1.dict_psettings_array[prog_index_list_unfixed[1]][prog_index_list_unfixed[2]]['important_plot']
        shapen = np.shape(value_to_test1.x_data[prog_index_list_unfixed[1]][prog_index_list_unfixed[2]])
        self.result = True
        self.value_to_test = shapen
        print(shapen)
        # if value_to_test:#if type(value_to_test) == tuple:
        #     self.result = True
        #     self.value_to_test = value_to_test
        # else:
        #     self.result = False
        #     self.value_to_test = value_to_test

    def run_test(self, method_string, dict_test_args):
        self.method_string = method_string
        self.result = None
        self.value_to_test = None
        self.prog_index_list_unfixed = None
        try:
            self.test_run(**dict_test_args)
            self.standard_print(ran=False)
        except:
            self.standard_print(ran=True)
        self.current_test = self.current_test + 1

class PlotDataHolder(object):
    """
        Holds the data to Plot to
        """

    def __init__(self):
        pass
    
class TwinSpecificItemHolder(object):
    """
        Holds the data to Plot to
        """

    def __init__(self):
        pass

class MakePlot(object):
    """
    Object to make the plots in
    """
    def __init__(self, plot_dict_entry, max_index_repeated):
        """

        """
        self.max_index_repeated = max_index_repeated  # For multiple repetitions alongside each other, shift in the direction specified in the plot
        internal_data_storage_obj = PlotDataHolder()
        self.data_plot_obj_list = [deepcopy(internal_data_storage_obj) for _ in range(self.max_index_repeated)]
        for rep_indx in range(self.max_index_repeated):
            self.data_plot_obj_list[rep_indx].specific_holder = [TwinSpecificItemHolder(), TwinSpecificItemHolder()]
        UsefulClassSwapping.set_attribute_in_storage_class_from_dict(plot_dict_entry, self)
        self.default_col_list = UsefulColorPallets.get_color_pallet()
        self.default_leg_loc = 'lower right'
        self.unit_converter = UsefulSIUnitConverter()
        self.size_of_plot_labels = 70.
        self.add_plot_size_shared = self.size_of_plot_labels*0.1
        self.max_legend_font_size = 20.
        self.default_fontsize_plots = 20
        # rc({'figure.subplot.bottom': 0.33})
        self.percent_to_add_around_ax_lim_change = 0.05
        self.bottom = 0.13
        self.left = 0.116
        self.right = 0.998
        parts = ['fonts', 'afm', 'ptmr8a.afm']
        afm_fname = Os_path(rcParams['datapath']).joinpath(*parts)
        try:
            with open(afm_fname, 'rb') as fh:
                self.afm = AFM(fh)
            self.use_afm = True
        except:
            self.use_afm = False

        self.make_bf_latex = True
        self.tester = DiagEachMethod(plot_key=self.plot_key)
        self.set_caption_abc_index = 0
        self.set_caption_independent = False
        self.abc_font_size = 20
        self.title_font_size = 35

    def get_ax_order_data_and_labels(self, rep_indx, figure_title):
        self.data_plot_obj_list[rep_indx].figure_title = figure_title
        # Make self.data_type_linking
        self.data_plot_obj_list[rep_indx].stored_x = []
        self.data_plot_obj_list[rep_indx].stored_y = []
        self.data_plot_obj_list[rep_indx].stored_z = []
        self.data_plot_obj_list[rep_indx].stored_x_side_dict = {}  # use this to change anything in self.data_plot_obj_list[rep_indx].stored_x, as they are not copies here
        self.data_plot_obj_list[rep_indx].stored_y_side_dict = {}  # use this to change anything in self.data_plot_obj_list[rep_indx].stored_x, as they are not copies here
        self.data_plot_obj_list[rep_indx].stored_z_side_dict = {}  # use this to change anything in self.data_plot_obj_list[rep_indx].stored_x, as they are not copies here
        self.data_plot_obj_list[rep_indx].n_x = 0
        self.data_plot_obj_list[rep_indx].n_y = 0
        self.data_plot_obj_list[rep_indx].n_z = 0
        # self.stored_x_to_nx_index = []

        stored_x_index = 0
        stored_y_index = 0
        stored_z_index = 0
        set_num_list = []
        for input_data_obj in self.data_plot_obj_list[rep_indx].input_data_obj_list:
            for set_num in input_data_obj.set_num:
                if not set_num in set_num_list:
                    set_num_list.append(set_num)

        set_used_replace = []
        for set_in in set_num_list:
            if set_in in self.sets_used: #####
                set_used_replace.append(set_in)
        set_num_list = np.sort(set_used_replace)
        print(set_num_list)
        self.set_num_list = set_num_list

        self.data_plot_obj_list[rep_indx].n_x = np.zeros(len(set_num_list), dtype=int)
        self.data_plot_obj_list[rep_indx].n_y = np.zeros(len(set_num_list), dtype=int)
        self.data_plot_obj_list[rep_indx].n_z = np.zeros(len(set_num_list), dtype=int)
        self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index = np.empty((len(set_num_list), ), dtype=list)
        self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index = np.empty((len(set_num_list), ), dtype=list)
        self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index = np.empty((len(set_num_list), ), dtype=list)
        for i in range(len(self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index)):
            self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index[i] = []
        for i in range(len(self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index)):
            self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index[i] = []
        for i in range(len(self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index)):
            self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index[i] = []
        for i in range(len(self.data_plot_obj_list[rep_indx].input_data_obj_list)):
            input_data_obj = self.data_plot_obj_list[rep_indx].input_data_obj_list[i]
            input_data_obj.xyz_list = np.array(input_data_obj.xyz_list)
            input_data_obj.list_of_plots_used = np.array(input_data_obj.list_of_plots_used)
            where_the_plot_key = input_data_obj.list_of_plots_used == self.plot_key
            where_x = input_data_obj.xyz_list == 'x'
            where_y = input_data_obj.xyz_list == 'y'
            where_z = input_data_obj.xyz_list == 'z'
            where_x_this_plot = np.where(np.logical_and(where_the_plot_key, where_x))[0]
            where_y_this_plot = np.where(np.logical_and(where_the_plot_key, where_y))[0]
            where_z_this_plot = np.where(np.logical_and(where_the_plot_key, where_z))[0]


            if len(where_x_this_plot) > 0:
                # self.n_x = self.n_x + len(where_x_this_plot)
                self.data_plot_obj_list[rep_indx].stored_x.append(input_data_obj)
                # self.stored_x_to_nx_index.append([i, np.arange(self.n_x - len(where_x_this_plot), self.n_x, 1)])
                for j in where_x_this_plot:
                    if input_data_obj.list_of_plots_used[j] == self.plot_key:
                        for set in range(len(set_num_list)):
                            if input_data_obj.set_num[j] == set_num_list[set]:
                                self.data_plot_obj_list[rep_indx].n_x[set] = self.data_plot_obj_list[rep_indx].n_x[set] + 1
                                self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index[set].append([stored_x_index, input_data_obj.set_num[j], j])
                stored_x_index = stored_x_index + 1
            if len(where_y_this_plot) > 0:
                # self.n_y = self.n_y + len(where_y_this_plot)
                self.data_plot_obj_list[rep_indx].stored_y.append(input_data_obj)
                # self.stored_y_to_ny_index.append([i, np.arange(self.n_y - len(where_y_this_plot), self.n_y, 1)])
                for j in where_y_this_plot:
                    if input_data_obj.list_of_plots_used[j] == self.plot_key:
                        for set in range(len(set_num_list)):
                            if input_data_obj.set_num[j] == set_num_list[set]:
                                self.data_plot_obj_list[rep_indx].n_y[set] = self.data_plot_obj_list[rep_indx].n_y[set] + 1
                                self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index[set].append([stored_y_index, input_data_obj.set_num[j], j])
                stored_y_index = stored_y_index + 1
            if len(where_z_this_plot) > 0:
                # self.n_z = self.n_z + len(where_z_this_plot)
                self.data_plot_obj_list[rep_indx].stored_z.append(input_data_obj)
                # self.stored_z_to_nz_index.append([i, np.arange(self.n_z - len(where_z_this_plot), self.n_z, 1)])
                for j in where_z_this_plot:
                    if input_data_obj.list_of_plots_used[j] == self.plot_key:
                        for set in range(len(set_num_list)):
                            if input_data_obj.set_num[j] == set_num_list[set]:
                                self.data_plot_obj_list[rep_indx].n_z[set] = self.data_plot_obj_list[rep_indx].n_z[set] + 1
                                self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index[set].append([stored_z_index, input_data_obj.set_num[j], j])
                stored_z_index = stored_z_index + 1

        # Sort into the sets
        self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted = self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index#np.empty((np.shape(set_num_list)[0], ), dtype=list)
        self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted = self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index#np.empty((np.shape(set_num_list)[0], ), dtype=list)
        self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index_sorted = self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index#np.empty((np.shape(set_num_list)[0], ), dtype=list)
        ############################################################################
        self.data_plot_obj_list[rep_indx].index_link = np.empty((len(self.set_num_list)), dtype=list)
        self.type_x_plot = np.empty((len(self.set_num_list)), dtype=list)  # time or data etc
        self.type_y_plot = np.empty((len(self.set_num_list)), dtype=list)  # time or data etc
        self.type_z_plot = np.empty((len(self.set_num_list)), dtype=list)  # time or data etc
        self.type_x_plot_err = np.empty((len(self.set_num_list)), dtype=list)  # error or time_error etc
        self.type_y_plot_err = np.empty((len(self.set_num_list)), dtype=list)  # error or time_error etc
        self.type_z_plot_err = np.empty((len(self.set_num_list)), dtype=list)  # error or time_error etc

        self.data_plot_obj_list[rep_indx].type_plot_ax = np.empty((len(self.set_num_list)), dtype=list)

        self.data_plot_obj_list[rep_indx].x_data = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].y_data = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].z_data = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].x_data_key_check = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].y_data_key_check = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].z_data_key_check = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].x_err = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].y_err = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].z_err = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].x_err_on = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].y_err_on = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].z_err_on = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].specific_holder[0].dict_kwargs_array = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].specific_holder[1].dict_kwargs_array = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].specific_holder[0].overwrite_legend_label = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].specific_holder[1].overwrite_legend_label = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].x_ax_min_max_sets = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].y_ax_min_max_sets = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].z_ax_min_max_sets = np.empty((len(self.set_num_list)), dtype=list)
        # self.data_plot_obj_list[rep_indx].x_key_bounds_sets = np.empty((len(self.set_num_list)), dtype=list)
        # self.data_plot_obj_list[rep_indx].y_key_bounds_sets = np.empty((len(self.set_num_list)), dtype=list)
        # self.data_plot_obj_list[rep_indx].z_key_bounds_sets = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].order_mag_x_data_sets = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].order_mag_y_data_sets = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].order_mag_z_data_sets = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].dict_psettings_array = np.empty((len(self.set_num_list)), dtype=list)

        self.data_plot_obj_list[rep_indx].specific_holder[0].bool_full = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].specific_holder[0].bool_full_max_dim = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].specific_holder[0].bool_full_or = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].specific_holder[1].bool_full = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].specific_holder[1].bool_full_max_dim = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].specific_holder[1].bool_full_or = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].post_plot_set = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].thin_error_bar_num_x = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].thin_error_bar_num_y = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].thin_error_bar_num_z = np.empty((len(self.set_num_list)), dtype=list)
        self.data_plot_obj_list[rep_indx].add_onto_twin_ax = np.empty((len(self.set_num_list)), dtype=list)

        self.current_overplot_index = np.empty((len(self.set_num_list)), dtype=list)
        self.x_data_set_type = np.empty((len(self.set_num_list)), dtype=list)
        self.y_data_set_type = np.empty((len(self.set_num_list)), dtype=list)
        self.z_data_set_type = np.empty((len(self.set_num_list)), dtype=list)

        self.data_plot_obj_list[rep_indx].ax_index_list = np.empty((len(self.set_num_list)), dtype=list)
        for set in range(len(self.set_num_list)):
            self.data_plot_obj_list[rep_indx].index_link[set] = []
            if self.x_type is not None:
                x_type = self.x_type[set]
                if x_type == 'time':
                    x_type_err = 'time_error'
                else:
                    x_type_err = 'error'
            else:
                x_type = 'data'
                x_type_err = 'error'
            if self.y_type is not None:
                y_type = self.y_type[set]
                if y_type == 'time':
                    y_type_err = 'time_error'
                else:
                    y_type_err = 'error'
            else:
                y_type = 'data'
                y_type_err = 'error'
            if self.z_type is not None:
                z_type = self.z_type[set]
                if z_type == 'time':
                    z_type_err = 'time_error'
                else:
                    z_type_err = 'error'
            else:
                z_type = 'data'
                z_type_err = 'error'

            if self.sorting_type_xyz_linking[set] == 'all_comb':
                for i in range(self.n_x[set]):

                    for j in range(self.n_y[set]):
                        if self.n_z[set] == 0:
                            self.data_plot_obj_list[rep_indx].index_link[set].append([self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][0],
                                                         self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][0],
                                                         None,
                                                         self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][2],
                                                         self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][2],
                                                         None,
                                                         i, j, None,
                                                         self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][1],
                                                         self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][1],
                                                         None])

                        for k in range(self.n_z[set]):
                            if self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index_sorted[set][k] is not None:
                                self.data_plot_obj_list[rep_indx].index_link[set].append([self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][0],
                                                             self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][0],
                                                             self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index_sorted[set][k][0],
                                                             self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][2],
                                                             self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][2],
                                                             self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index_sorted[set][k][2],
                                                             i, j, k,
                                                             self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][1],
                                                             self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][1],
                                                             self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index_sorted[set][k][1]])

            if self.sorting_type_xyz_linking[set] == 'index_link':  # usually only useful for variable vs time
                print(self.plot_key, set, self.set_num_list)
                for i in range(self.data_plot_obj_list[rep_indx].n_x[set]):
                    for j in range(self.data_plot_obj_list[rep_indx].n_y[set]):
                        if self.data_plot_obj_list[rep_indx].n_z[set] == 0:
                            if i == j:
                                self.data_plot_obj_list[rep_indx].index_link[set].append([self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][0],
                                                             self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][0],
                                                             None,
                                                             self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][2],
                                                             self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][2],
                                                             None,
                                                             i, j, None,
                                                             self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][1],
                                                             self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][1],
                                                             None,])
                        else:
                            for k in range(self.data_plot_obj_list[rep_indx].n_z[set]):
                                if i == j and i == k:
                                    self.data_plot_obj_list[rep_indx].index_link[set].append([self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][0],
                                                                 self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][0],
                                                                 self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index_sorted[set][k][0],
                                                                 self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][2],
                                                                 self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][2],
                                                                 self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index_sorted[set][k][2],
                                                                 i, j, k,
                                                                 self.data_plot_obj_list[rep_indx].n_x_to_stored_x_index_sorted[set][i][1],
                                                                 self.data_plot_obj_list[rep_indx].n_y_to_stored_y_index_sorted[set][j][1],
                                                                 self.data_plot_obj_list[rep_indx].n_z_to_stored_z_index_sorted[set][k][1]])

            self.type_x_plot[set] = np.full((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), x_type)  # time or data etc
            self.type_y_plot[set] = np.full((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), y_type)  # time or data etc
            self.type_z_plot[set] = np.full((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), z_type)  # time or data etc
            self.type_x_plot_err[set] = np.full((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), x_type_err)  # time or data etc
            self.type_y_plot_err[set] = np.full((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), y_type_err)  # time or data etc
            self.type_z_plot_err[set] = np.full((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), z_type_err)  # time or data etc

        for set in range(len(self.set_num_list)):
            self.data_plot_obj_list[rep_indx].type_plot_ax[set] = np.full((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), self.type_plot[set])
            self.data_plot_obj_list[rep_indx].x_data[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].y_data[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].z_data[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)

            self.data_plot_obj_list[rep_indx].x_data_key_check[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].y_data_key_check[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].z_data_key_check[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].x_err[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].y_err[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].z_err[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].x_err_on[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].y_err_on[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].z_err_on[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].specific_holder[0].dict_kwargs_array[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].specific_holder[1].dict_kwargs_array[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].specific_holder[0].overwrite_legend_label[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].specific_holder[1].overwrite_legend_label[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].x_ax_min_max_sets[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].y_ax_min_max_sets[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].z_ax_min_max_sets[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].dict_psettings_array[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)

            self.data_plot_obj_list[rep_indx].specific_holder[0].bool_full[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].specific_holder[0].bool_full_max_dim[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].specific_holder[0].bool_full_or[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].specific_holder[1].bool_full[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].specific_holder[1].bool_full_max_dim[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].specific_holder[1].bool_full_or[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].post_plot_set[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].thin_error_bar_num_x[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].thin_error_bar_num_y[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.data_plot_obj_list[rep_indx].thin_error_bar_num_z[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            self.x_data_set_type[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], 2), dtype=object)
            self.y_data_set_type[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], 2), dtype=object)
            self.z_data_set_type[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], 2), dtype=object)
            self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)

            for inx in range(len(self.data_plot_obj_list[rep_indx].stored_x)):
                self.data_plot_obj_list[rep_indx].stored_x_side_dict[str(inx)] = {}
            for iny in range(len(self.data_plot_obj_list[rep_indx].stored_y)):
                self.data_plot_obj_list[rep_indx].stored_y_side_dict[str(iny)] = {}
            for inz in range(len(self.data_plot_obj_list[rep_indx].stored_z)):
                self.data_plot_obj_list[rep_indx].stored_z_side_dict[str(inz)] = {}

            for inx in range(len(self.data_plot_obj_list[rep_indx].stored_x)):
                self.data_plot_obj_list[rep_indx].stored_x_side_dict[str(inx)]['color_data_type'] = None
            for iny in range(len(self.data_plot_obj_list[rep_indx].stored_y)):
                self.data_plot_obj_list[rep_indx].stored_y_side_dict[str(iny)]['color_data_type'] = None
            for inz in range(len(self.data_plot_obj_list[rep_indx].stored_z)):
                self.data_plot_obj_list[rep_indx].stored_z_side_dict[str(inz)]['color_data_type'] = None
            # Get the Data
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                if self.data_plot_obj_list[rep_indx].index_link[set][i][0] is not None:
                    print(np.shape(getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], self.type_x_plot[set][i])), np.shape(getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], 'time')))

                    self.data_plot_obj_list[rep_indx].x_data[set][i] = getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], self.type_x_plot[set][i])
                    if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None:
                        if not isinstance(self.data_plot_obj_list[rep_indx].x_data[set][i], Iterable):  # If float /int then make it a list
                            self.data_plot_obj_list[rep_indx].x_data[set][i] = np.array([self.data_plot_obj_list[rep_indx].x_data[set][i]])
                    self.data_plot_obj_list[rep_indx].x_data_key_check[set][i] = getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], 'key')
                    self.x_data_set_type[set][i] = [self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]].load_obj.x_set, self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]].load_obj.y_set]
                else:
                    self.data_plot_obj_list[rep_indx].x_data[set][i] = None
                    self.data_plot_obj_list[rep_indx].x_data_key_check[set][i] = None
                    self.x_data_set_type[set][i] = None
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                if self.data_plot_obj_list[rep_indx].index_link[set][i][1] is not None:
                    self.data_plot_obj_list[rep_indx].y_data[set][i] = getattr(self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]], self.type_y_plot[set][i])
                    if self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                        if not isinstance(self.data_plot_obj_list[rep_indx].y_data[set][i], Iterable):  # If float /int then make it a list
                            self.data_plot_obj_list[rep_indx].y_data[set][i] = np.array([self.data_plot_obj_list[rep_indx].y_data[set][i]])
                    self.data_plot_obj_list[rep_indx].y_data_key_check[set][i] = getattr(self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]], 'key')
                    self.y_data_set_type[set][i] = [self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]].load_obj.x_set, self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]].load_obj.y_set]
                else:
                    self.data_plot_obj_list[rep_indx].y_data[set][i] = None
                    self.data_plot_obj_list[rep_indx].y_data_key_check[set][i] = None
                    self.y_data_set_type[set][i] = None
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                if self.data_plot_obj_list[rep_indx].index_link[set][i][2] is not None:
                    self.data_plot_obj_list[rep_indx].z_data[set][i] = getattr(self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]], self.type_z_plot[set][i])
                    if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                        if not isinstance(self.data_plot_obj_list[rep_indx].z_data[set][i], Iterable):  # If float /int then make it a list
                            self.data_plot_obj_list[rep_indx].z_data[set][i] = np.array([self.data_plot_obj_list[rep_indx].z_data[set][i]])
                    self.data_plot_obj_list[rep_indx].z_data_key_check[set][i] = getattr(self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]], 'key')
                    self.z_data_set_type[set][i] = [self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]].load_obj.x_set, self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]].load_obj.y_set]
                else:
                    self.data_plot_obj_list[rep_indx].z_data[set][i] = None
                    self.data_plot_obj_list[rep_indx].z_data_key_check[set][i] = None
                    self.z_data_set_type[set][i] = None

            # Get the Errors
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                if self.data_plot_obj_list[rep_indx].index_link[set][i][0] is not None:
                    self.data_plot_obj_list[rep_indx].x_err[set][i] = getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], self.type_x_plot_err[set][i])
                    x_err_on = getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], 'error_on')
                    self.data_plot_obj_list[rep_indx].x_err_on[set][i] = x_err_on[self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
                    x_err_thin = getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], 'thin_error_bar_num')
                    self.data_plot_obj_list[rep_indx].thin_error_bar_num_x[set][i] = x_err_thin[self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
                else:
                    self.data_plot_obj_list[rep_indx].x_err[set][i] = None
                    self.data_plot_obj_list[rep_indx].x_err_on[set][i] = True
                    self.data_plot_obj_list[rep_indx].thin_error_bar_num_x[set][i] = 1

            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                if self.data_plot_obj_list[rep_indx].index_link[set][i][1] is not None:
                    self.data_plot_obj_list[rep_indx].y_err[set][i] = getattr(self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]], self.type_y_plot_err[set][i])
                    y_err_on = getattr(self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]], 'error_on')
                    self.data_plot_obj_list[rep_indx].y_err_on[set][i] = y_err_on[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                    y_err_thin = getattr(self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]], 'thin_error_bar_num')
                    self.data_plot_obj_list[rep_indx].thin_error_bar_num_y[set][i] = y_err_thin[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                else:
                    self.data_plot_obj_list[rep_indx].y_err[set][i] = None
                    self.data_plot_obj_list[rep_indx].y_err_on[set][i] = True
                    self.data_plot_obj_list[rep_indx].thin_error_bar_num_y[set][i] = 1
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                if self.data_plot_obj_list[rep_indx].index_link[set][i][2] is not None:
                    self.data_plot_obj_list[rep_indx].z_err[set][i] = getattr(self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]], self.type_z_plot_err[set][i])
                    z_err_on = getattr(self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]], 'error_on')
                    self.data_plot_obj_list[rep_indx].z_err_on[set][i] = z_err_on[self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
                    z_err_thin = getattr(self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]], 'thin_error_bar_num')
                    self.data_plot_obj_list[rep_indx].thin_error_bar_num_z[set][i] = z_err_thin[self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
                else:
                    self.data_plot_obj_list[rep_indx].z_err[set][i] = None
                    self.data_plot_obj_list[rep_indx].z_err_on[set][i] = True
                    self.data_plot_obj_list[rep_indx].thin_error_bar_num_z[set][i] = 1

            self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set] = np.full((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), False)
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # If x, y, or z is used, True is the default
                overwrite_legend = getattr(self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]], 'overwrite_legend_label')
                self.data_plot_obj_list[rep_indx].specific_holder[0].overwrite_legend_label[set][i] = overwrite_legend[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                self.data_plot_obj_list[rep_indx].specific_holder[1].overwrite_legend_label[set][i] = overwrite_legend[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]

                if self.data_plot_obj_list[rep_indx].index_link[set][i][0] is not None:
                    x_add_onto_twin_ax = getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], 'add_onto_twin_ax')
                    x_add_onto_twin_ax = x_add_onto_twin_ax[self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
                    if x_add_onto_twin_ax:
                        self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i] = True
                if self.data_plot_obj_list[rep_indx].index_link[set][i][1] is not None:
                    y_add_onto_twin_ax = getattr(self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]], 'add_onto_twin_ax')
                    y_add_onto_twin_ax = y_add_onto_twin_ax[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                    if y_add_onto_twin_ax:
                        self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i] = True
                if self.data_plot_obj_list[rep_indx].index_link[set][i][2] is not None:
                    z_add_onto_twin_ax = getattr(self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]], 'add_onto_twin_ax')
                    z_add_onto_twin_ax = z_add_onto_twin_ax[self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
                    if z_add_onto_twin_ax:
                        self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i] = True

            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                self.data_plot_obj_list[rep_indx].specific_holder[0].dict_kwargs_array[set][i] = {}
                self.data_plot_obj_list[rep_indx].specific_holder[1].dict_kwargs_array[set][i] = {}
                self.data_plot_obj_list[rep_indx].x_ax_min_max_sets[set][i] = {}
                self.data_plot_obj_list[rep_indx].y_ax_min_max_sets[set][i] = {}
                self.data_plot_obj_list[rep_indx].z_ax_min_max_sets[set][i] = {}
                self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i] = {}
                self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i] = {}
                self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i] = {}
                self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i] = {}

                self.data_plot_obj_list[rep_indx].specific_holder[0].bool_full[set][i] = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), True)  # TODO this might need changing if there is no xdata?
                self.data_plot_obj_list[rep_indx].specific_holder[0].bool_full_max_dim[set][i] = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), True)  # TODO this might need changing if there is no xdata?
                self.data_plot_obj_list[rep_indx].specific_holder[0].bool_full_or[set][i] = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), True)  # TODO this might need changing if there is no xdata?
                self.data_plot_obj_list[rep_indx].specific_holder[1].bool_full[set][i] = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), True)  # TODO this might need changing if there is no xdata?
                self.data_plot_obj_list[rep_indx].specific_holder[1].bool_full_max_dim[set][i] = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), True)  # TODO this might need changing if there is no xdata?
                self.data_plot_obj_list[rep_indx].specific_holder[1].bool_full_or[set][i] = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), True)  # TODO this might need changing if there is no xdata?
                self.data_plot_obj_list[rep_indx].post_plot_set[set][i] = None
        check_keys = False
        if check_keys:
            for set in range(len(self.set_num_list)):
                for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                    print(set, i, self.data_plot_obj_list[rep_indx].x_data_key_check[set][i], self.data_plot_obj_list[rep_indx].y_data_key_check[set][i], self.data_plot_obj_list[rep_indx].z_data_key_check[set][i])
            quit()
        last_index = 0
        for set in range(len(self.set_num_list)):
            # Set the auto index
            self.data_plot_obj_list[rep_indx].ax_index_list[set] = []
            if self.sorting_type_ax_index[set] == 'appending':
                range_end = np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]
                self.data_plot_obj_list[rep_indx].ax_index_list[set] = np.arange(last_index, last_index + range_end, 1)
                last_index = last_index + range_end

    def get_ax_order_dicts(self, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]

        for i in range(len(self.data_plot_obj_list[rep_indx].input_data_obj_list)):
            spec_h_use.dict_x_settings_array = np.empty((len(self.set_num_list)), dtype=list)
            spec_h_use.dict_y_settings_array = np.empty((len(self.set_num_list)), dtype=list)
            spec_h_use.dict_z_settings_array = np.empty((len(self.set_num_list)), dtype=list)
        for set in range(len(self.set_num_list)):
            spec_h_use.dict_x_settings_array[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            spec_h_use.dict_y_settings_array[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
            spec_h_use.dict_z_settings_array[set] = np.empty((np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0], ), dtype=object)
        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                spec_h_use.dict_x_settings_array[set][i] = {}
                spec_h_use.dict_y_settings_array[set][i] = {}
                spec_h_use.dict_z_settings_array[set][i] = {}
    def do_overplotting(self, stored_obj, stored_obj_index, rep_indx):
        overplot_already_used = []
        overplot_index_overwrite = []
        for set in range(len(self.set_num_list)):
            if self.sorting_type_ax_index[set] == 'appending':
                for i in range(len(self.data_plot_obj_list[rep_indx].index_link[set])):
                    if self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index] is not None:
                        current_overplot_index = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].overplot_link_num[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                    else:
                        current_overplot_index = None

                    if current_overplot_index is not None:
                        if not current_overplot_index in overplot_already_used:
                            overplot_already_used.append(current_overplot_index)
                            overplot_index_overwrite.append([set, i])
                        else:
                            which_i = overplot_already_used.index(current_overplot_index)
                            self.data_plot_obj_list[rep_indx].ax_index_list[set][i] = self.data_plot_obj_list[rep_indx].ax_index_list[overplot_index_overwrite[which_i][0]][overplot_index_overwrite[which_i][1]]

        if len(overplot_already_used) > 0:
            self.recount_ax_index(rep_indx)

    def recount_ax_index(self, rep_indx):
        # Collapse
        set_bounds = [0]
        for set in range(len(self.set_num_list)):
            set_bounds.append(set_bounds[-1]+len(self.data_plot_obj_list[rep_indx].index_link[set]))
        collapsed_index_list = []
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].index_link[set])):
                collapsed_index_list.append(self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
        collapsed_index_list = np.array(collapsed_index_list)
        if len(collapsed_index_list) > 1:
            while np.nanmax(np.abs(np.diff(np.sort(collapsed_index_list)))) > 1:
                sorted = np.sort(collapsed_index_list)
                argnan_greatest_val = np.nanargmax(np.abs(np.diff(sorted)))
                where_flat_index_2_higher = np.where(collapsed_index_list > sorted[argnan_greatest_val])
                collapsed_index_list[where_flat_index_2_higher] = collapsed_index_list[where_flat_index_2_higher] - 1

            for set in range(len(self.set_num_list)):
                self.data_plot_obj_list[rep_indx].ax_index_list[set] = collapsed_index_list[slice(set_bounds[set], set_bounds[set+1])]

    def make_settings_dict(self, stored_obj, stored_obj_index, rep_indx, use_twin_ax=False):
        """
        stored_obj is self.stored_x or _y _z. index 0 1 2 respectively
        """
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]

        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                if self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index] is not None:
                # if self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+9] == set:
                    self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['leg_loc'] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].leg_loc[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]

                    self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot'] = \
                    stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].important_plot[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                    self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['equal_xy'] = \
                        stored_obj[
                            self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].equal_xy[
                            self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]]
                    self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize'] = \
                        stored_obj[
                            self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].fontsize[
                            self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]]
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize'] is None:
                        self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize'] = self.default_fontsize_plots
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'line_plot':
                        pass
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'error_plot':
                        pass
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'error_example':
                        pass
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'arrow_plot':
                        pass
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_plot' or self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_at_mean_plot' or self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_at_start_plot' or self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_at_end_plot':
                        pass
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == '2d_colour_plot':
                        pass
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'scatter':
                        if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                            pass
                        else:
                            pass
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'axspan_plot':
                        self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['h_plot_axspan'] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].h_plot_axspan[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'axline_plot':
                        self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['h_plot_axline'] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].h_plot_axline[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
        # General things in the plot dict
        str_things_to_get = ['label', 'gen_label', 'units']
        xyz_str = ['x', 'y', 'z']
        xyz_index = [0, 1, 2]
        time_assignments = ['Time', 'Time', 's']
        for xyz in range(len(xyz_str)):
            xyz_storage_obj = getattr(self.data_plot_obj_list[rep_indx], 'stored_'+xyz_str[xyz])
            dict_to_assign = getattr(spec_h_use, 'dict_'+xyz_str[xyz]+'_settings_array')
            type_plot = getattr(self, 'type_'+xyz_str[xyz]+'_plot')
            for set in range(len(self.set_num_list)):
                for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                    for str_get in range(len(str_things_to_get)):
                        if type_plot[set][i] == 'time':
                            dict_to_assign[set][i][str_things_to_get[str_get] + '_' + xyz_str[xyz]] = time_assignments[str_get]
                        else:

                            if self.data_plot_obj_list[rep_indx].index_link[set][i][xyz_index[xyz]] is not None:
                                dict_to_assign[set][i][str_things_to_get[str_get]+'_'+xyz_str[xyz]] = getattr(xyz_storage_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][xyz_index[xyz]]], str_things_to_get[str_get])
                            else:
                                dict_to_assign[set][i][str_things_to_get[str_get] + '_' + xyz_str[xyz]] = None

    def reset_data_unused_ax(self, rep_indx):
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].index_link[set])):
                if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'axline_plot':
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['h_plot_axline'] is None:
                        raise ValueError("h_plot_axline must not be None")
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['h_plot_axline']:
                        self.data_plot_obj_list[rep_indx].x_data[set][i] = np.full(np.shape(self.data_plot_obj_list[rep_indx].y_data[set][i]), np.nan)
                    else:
                        self.data_plot_obj_list[rep_indx].y_data[set][i] = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), np.nan)
                elif self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'add_text':
                    if len(self.data_plot_obj_list[rep_indx].y_data[set][i]) == 0:
                        if len(self.data_plot_obj_list[rep_indx].x_data[set][i]) == 0:
                            raise ValueError("both x and y are len 0, TODO if z not 0?")
                        self.data_plot_obj_list[rep_indx].y_data[set][i] = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), np.nan)
                    elif len(self.data_plot_obj_list[rep_indx].x_data[set][i]) == 0:
                        if len(self.data_plot_obj_list[rep_indx].y_data[set][i]) == 0:
                            raise ValueError("both x and y are len 0, TODO if z not 0?")
                        self.data_plot_obj_list[rep_indx].x_data[set][i] = np.full(np.shape(self.data_plot_obj_list[rep_indx].y_data[set][i]), np.nan)
        self.tester.run_test('reset_data_unused_ax', {'object':self, 'prog_index_list_unfixed':[rep_indx]})

    def make_dict_kwargs(self, stored_obj, stored_obj_index, rep_indx, use_twin_ax=False):
        """
        stored_obj is self.stored_x or _y _z. index 0 1 2 respectively
        """
        if use_twin_ax:
            dict_kwargs_array_use = self.data_plot_obj_list[rep_indx].specific_holder[1].dict_kwargs_array
        else:
            dict_kwargs_array_use = self.data_plot_obj_list[rep_indx].specific_holder[0].dict_kwargs_array
        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'line_plot':
                        # Set automatic colours
                        dict_kwargs_array_use[set][i]["color"] = self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour']
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["marker"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                            dict_kwargs_array_use[set][i]["markeredgewidth"] =  1.
                            dict_kwargs_array_use[set][i]["markeredgecolor"] =  'k'
                            dict_kwargs_array_use[set][i]["markersize"] =  20

                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linestyle[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["linestyle"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linestyle[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linewidth[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["linewidth"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linewidth[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["alpha"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]

                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'error_plot':
                        # Set automatic colours
                        dict_kwargs_array_use[set][i]["color"] = self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour']
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["marker"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                        if self.errorbar_standard_setting == 0:
                            dict_kwargs_array_use[set][i]["markersize"] = 9.
                            dict_kwargs_array_use[set][i]["capsize"] = 3.
                            dict_kwargs_array_use[set][i]["capthick"] = 3.
                            dict_kwargs_array_use[set][i]["elinewidth"] = 3.
                            dict_kwargs_array_use[set][i]["zorder"] = 6.
                        else:
                            dict_kwargs_array_use[set][i]["markeredgewidth"] =  1.
                            dict_kwargs_array_use[set][i]["markeredgecolor"] =  'k'
                            dict_kwargs_array_use[set][i]["markersize"] =  20
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linestyle[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["linestyle"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linestyle[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linewidth[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["linewidth"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linewidth[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]

                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["alpha"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'error_example':
                        # Set automatic colours
                        dict_kwargs_array_use[set][i]["color"] = self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour']
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[
                            self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]] is not None:
                            dict_kwargs_array_use[set][i]["marker"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]]
                        if self.errorbar_standard_setting == 0:
                            dict_kwargs_array_use[set][i]["markersize"] = 9.
                            dict_kwargs_array_use[set][i]["capsize"] = 3.
                            dict_kwargs_array_use[set][i]["capthick"] = 3.
                            dict_kwargs_array_use[set][i]["elinewidth"] = 3.
                            dict_kwargs_array_use[set][i]["zorder"] = 6.
                        else:
                            dict_kwargs_array_use[set][i]["markeredgewidth"] =  1.
                            dict_kwargs_array_use[set][i]["markeredgecolor"] =  'k'
                            dict_kwargs_array_use[set][i]["markersize"] =  20

                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linestyle[
                            self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]] is not None:
                            dict_kwargs_array_use[set][i]["linestyle"] = \
                            stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linestyle[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]]
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linewidth[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["linewidth"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linewidth[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]

                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[
                            self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]] is not None:
                            dict_kwargs_array_use[set][i]["alpha"] = \
                            stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]]

                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'arrow_plot':
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].arrow_width[
                            self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]] is not None:
                            dict_kwargs_array_use[set][i]["width"] = \
                            stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].arrow_width[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]]

                        dict_kwargs_array_use[set][i]["color"] = self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour']

                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_plot' or self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_at_mean_plot' or self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_at_start_plot' or self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_at_end_plot':
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["marker"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                        else:
                            dict_kwargs_array_use[set][i]["marker"] = 'o'

                        dict_kwargs_array_use[set][i]["color"] = self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour']

                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'scatter':
                        if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                            # create colorbar
                            if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].norm[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]] is not None:
                                dict_kwargs_array_use[set][i]["norm"] = \
                                stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].norm[
                                    self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]]
                            if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].norm[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]] is not None:
                                dict_kwargs_array_use[set][i]["cmap"] = \
                                stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].cmap[
                                    self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]]
                        else:
                            # Set automatic colours
                            dict_kwargs_array_use[set][i]["color"] = self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour']
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[
                            self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]] is not None:
                            dict_kwargs_array_use[set][i]["marker"] = \
                            stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]]
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[
                            self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]] is not None:
                            dict_kwargs_array_use[set][i]["alpha"] = \
                            stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index + 3]]

                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == '2d_colour_plot':
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["marker"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].marker[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'axspan_plot':
                        # Set automatic colours
                        dict_kwargs_array_use[set][i]["facecolor"] = self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour']
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].hatch[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["hatch"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].hatch[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["alpha"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linestyle[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["linestyle"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linestyle[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linewidth[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["linewidth"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linewidth[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]

                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'axline_plot':
                        # Set automatic colours
                        dict_kwargs_array_use[set][i]["color"] = self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour']
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["alpha"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].alpha[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linestyle[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["linestyle"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linestyle[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]
                        if stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linewidth[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]] is not None:
                            dict_kwargs_array_use[set][i]["linewidth"] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].linewidth[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]

    def add_data_unordered(self, input_data_obj_list, rep_indx):
        self.data_plot_obj_list[rep_indx].input_data_obj_list = input_data_obj_list  # The parts used in this plot
        self.data_plot_obj_list[rep_indx].index_for_col_marker_list = 0
        if len(self.data_plot_obj_list[rep_indx].input_data_obj_list) == 0:
            warnings.warn("No data has been input?, removing plot"+ self.plot_key)
            self.plot_on = False

    def set_title_str(self):
        ix = 0
        iy = 0
        if self.figure_title_on:
            if not self.save_to_svg:
                if self.index_stack_direction == 'stack':
                    figure_str = ''
                    for rep_indx in range(self.max_index_repeated):
                        if rep_indx == 0:
                            figure_str = self.data_plot_obj_list[rep_indx].figure_title
                        elif rep_indx == self.max_index_repeated - 1:
                            figure_str = figure_str + ' and ' + self.data_plot_obj_list[rep_indx].figure_title
                        else:
                            figure_str = figure_str + ', ' + self.data_plot_obj_list[rep_indx].figure_title
                    self.ax[ix + self.data_plot_obj_list[rep_indx].shift_x_ax_mult][
                        iy + self.data_plot_obj_list[rep_indx].shift_y_ax_mult].set_title(figure_str, size=self.title_font_size)
                else:
                    for rep_indx in range(self.max_index_repeated):
                        self.ax[ix+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][iy+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].set_title(self.data_plot_obj_list[rep_indx].figure_title, size=self.title_font_size)

    def make_ax(self, max_repeated_ax):
        # Make the axes
        if self.plot_on:
            self.true_n_x_ax = self.n_x_ax + self.shift_x_ax_mult_max
            self.true_n_y_ax = self.n_y_ax + self.shift_y_ax_mult_max
            if self.share_ax_x:
                self.sharex = 'col'
            else:
                self.sharex = False
            if self.share_ax_y:
                self.sharey = 'row'
            else:
                self.sharey = False
            self.fig, self.ax = plt.subplots(self.true_n_x_ax, self.true_n_y_ax, sharex=self.sharex, sharey=self.sharey)
            if self.fig_size is not None:
                self.fig.set_size_inches(self.fig_size)
            self.fig.subplots_adjust(**self.fig_suplots_adjust_dict)
            # self.fig.subplots_adjust(top=0.970)
            if self.true_n_x_ax == 1:
                self.ax = [self.ax]  # 1xn
            if self.true_n_y_ax == 1:
                self.ax = [[self.ax[i]] for i in range(len(self.ax))]  # nx1
            self.fig.tight_layout()
            if not self.save_to_svg:
                self.fig.canvas.set_window_title(self.plot_key)

            self.ax_twin = np.empty((self.true_n_x_ax, self.true_n_y_ax), dtype=object)
            self.ax_twin_added = np.full((self.true_n_x_ax, self.true_n_y_ax), False)

    def twin_ax_share_y(self):
        if self.plot_on:
            if self.share_ax_y:
                print(self.ax_twin_added)
                for ix in range(np.shape(self.ax_twin)[0]):
                    iy_1st = None
                    for iy in reversed(range(np.shape(self.ax_twin)[1])):
                        print('iy_1st', iy_1st, np.shape(self.ax_twin), self.ax_twin_added[ix][iy])
                        if self.ax_twin_added[ix][iy]:
                            if iy_1st is None:
                                iy_1st = iy
                            else:
                                self.ax_twin[ix][iy_1st].get_shared_y_axes().join(self.ax_twin[ix][iy_1st], self.ax_twin[ix][iy])
                                self.ax_twin[ix][iy].set_yticklabels([])
                                print("twinned")
    def make_twin_ax(self, rep_indx):
        if self.plot_on:
            # Add twin ax
            for set in range(len(self.set_num_list)):
                for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                    print(self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i])
                    if self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]:
                        ax_ind_x = self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                            rep_indx].shift_x_ax_mult
                        ax_ind_y = self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                rep_indx].shift_y_ax_mult
                        if not self.ax_twin_added[ax_ind_x][ax_ind_y]:
                            self.ax_twin[ax_ind_x][ax_ind_y] = self.ax[ax_ind_x][ax_ind_y].twinx()
                            self.ax_twin_added[ax_ind_x][ax_ind_y] = True

    def get_ax_shifts(self, rep_indx):
        if self.plot_on:
            if self.index_stack_direction == 'horizontal':
                self.data_plot_obj_list[rep_indx].shift_x_ax_mult = rep_indx * self.n_x_ax
                self.data_plot_obj_list[rep_indx].shift_y_ax_mult = 0
                self.shift_x_ax_mult_max = (self.max_index_repeated-1) * self.n_x_ax
                self.shift_y_ax_mult_max = 0
            elif self.index_stack_direction == 'vertical':
                self.data_plot_obj_list[rep_indx].shift_x_ax_mult = 0
                self.data_plot_obj_list[rep_indx].shift_y_ax_mult = rep_indx * self.n_y_ax
                self.shift_x_ax_mult_max = 0
                self.shift_y_ax_mult_max = (self.max_index_repeated-1) * self.n_y_ax
            elif self.index_stack_direction == 'stack':
                self.data_plot_obj_list[rep_indx].shift_x_ax_mult = 0
                self.data_plot_obj_list[rep_indx].shift_y_ax_mult = 0
                self.shift_x_ax_mult_max = 0
                self.shift_y_ax_mult_max = 0

    def general_decorate_plot(self, index_xyx, dict_array_get_from, ax_n, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]

        xyz_str_list = ['x', 'y', 'z']
        xyz_str = xyz_str_list[index_xyx]
        # dict_array_get_from  = getattr(self, 'dict_' + xyz_str + '_settings_array')
        if spec_h_use.index_link_unique[ax_n] is not None:
            if self.data_plot_obj_list[rep_indx].ax_used_twice[ax_n]:
                if self.ax_labels_type == 'specific':
                    label_ax_assign = dict_array_get_from[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['label_' + xyz_str]
                elif self.ax_labels_type == 'general' or self.ax_labels_type == 'gen_if_multiple':
                    label_ax_assign = dict_array_get_from[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['gen_label_' + xyz_str]
                elif self.ax_labels_type == 'off':
                    label_ax_assign = None
                units_return = dict_array_get_from[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['units_' + xyz_str]
                if self.labels_type == 'specific' or self.labels_type == 'off_if_not_multiple':
                    legend_label = dict_array_get_from[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['label_' + xyz_str]
                elif self.labels_type == 'off':
                    legend_label = None
            else:
                if self.ax_labels_type == 'specific' or self.ax_labels_type == 'gen_if_multiple':
                    label_ax_assign = dict_array_get_from[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['label_' + xyz_str]
                elif self.ax_labels_type == 'general':
                    label_ax_assign = dict_array_get_from[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['gen_label_' + xyz_str]
                units_return = dict_array_get_from[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['units_' + xyz_str]
                if self.labels_type == 'specific':
                    legend_label = dict_array_get_from[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['label_' + xyz_str]
                elif self.labels_type == 'off' or self.labels_type == 'off_if_not_multiple':
                    legend_label = None
        else:
            label_ax_assign = None
            units_return = None
            legend_label = None
        return label_ax_assign, units_return, legend_label

    def colour_user_defined(self, rep_indx, set, i):
        colour_was_used = False
        stored_x_index = self.data_plot_obj_list[rep_indx].index_link[set][i][0]
        stored_y_index = self.data_plot_obj_list[rep_indx].index_link[set][i][1]
        stored_z_index = self.data_plot_obj_list[rep_indx].index_link[set][i][2]
        if stored_x_index is not None:
            user_colour_x = self.data_plot_obj_list[rep_indx].stored_x[stored_x_index].color[
                self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
        else:
            user_colour_x = None
        if stored_y_index is not None:
            user_colour_y = self.data_plot_obj_list[rep_indx].stored_y[stored_y_index].color[
                self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
        else:
            user_colour_y = None
        if stored_z_index is not None:
            user_colour_z = self.data_plot_obj_list[rep_indx].stored_z[stored_z_index].color[
                self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
        else:
            user_colour_z = None
        user_colour = [user_colour_x, user_colour_y, user_colour_z]
        if user_colour.count(None) == 2:  # User has defined colour
            user_colour = np.array(user_colour)
            index_user_col = np.where(user_colour is not None)#user_colour.index(None)
            if self.colour_sorting_type == 'loop_by_data_type':  # every data type has the same colour, so it is stored in stored_x, y, z
                self.data_plot_obj_list[rep_indx].stored_x[stored_x_index].color_data_type = user_colour[index_user_col]
            if self.colour_sorting_type == 'unique_every_line':  # signal has a separate colour, so colour in psetting can be updated directly
                self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour'] = user_colour[index_user_col]
            colour_was_used = True
        elif user_colour.count(None) == 3:  # No user defined colour
            pass
        else:  # User has defined too many colours
            user_colour = np.array(user_colour)
            inxc_list = []
            for inxc in user_colour:
                if inxc is not None:
                    inxc_list.append(inxc)
            for k in range(1, len(inxc_list)):
                if inxc_list[k] != inxc_list[0]:
                    raise ValueError(
                        "more than 1 colour is input, non repetition " + str(inxc_list[k]) + ' ' + str(inxc_list[0]))
            # If the colours defined were all the same then just choose the one defined
            index_user_col = np.where(user_colour is not None)#index_user_col = user_colour.index(None)
            if self.colour_sorting_type == 'loop_by_data_type':  # every data type has the same colour, so it is stored in stored_x, y, z
                if stored_x_index is not None:
                    self.data_plot_obj_list[rep_indx].stored_x_side_dict[str(stored_x_index)]['color_data_type'] = user_colour[index_user_col][0]
                    # self.data_plot_obj_list[rep_indx].stored_x[stored_x_index].color_data_type = user_colour[index_user_col][0]
                if stored_y_index is not None:
                    self.data_plot_obj_list[rep_indx].stored_y_side_dict[str(stored_y_index)]['color_data_type'] = user_colour[index_user_col][0]
                    # self.data_plot_obj_list[rep_indx].stored_y[stored_y_index].color_data_type = user_colour[index_user_col][0]
                if stored_z_index is not None:
                    self.data_plot_obj_list[rep_indx].stored_z_side_dict[str(stored_z_index)]['color_data_type'] = user_colour[index_user_col][0]
                    # self.data_plot_obj_list[rep_indx].stored_z[stored_z_index].color_data_type = user_colour[index_user_col][0]
            if self.colour_sorting_type == 'unique_every_line':  # signal has a separate colour, so colour in psetting can be updated directly
                self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour'] = \
                    user_colour[index_user_col]
            colour_was_used = True
        return colour_was_used

    def colour_take_colourset_checker(self, rep_indx, set, i):
        set_from = None
        index_take_col = None
        take_colour_xyz_index = None
        stored_x_index = self.data_plot_obj_list[rep_indx].index_link[set][i][0]
        stored_y_index = self.data_plot_obj_list[rep_indx].index_link[set][i][1]
        stored_z_index = self.data_plot_obj_list[rep_indx].index_link[set][i][2]
        take_colourset_x_bool = False
        take_colourset_y_bool = False
        take_colourset_z_bool = False

        if stored_x_index is not None:
            take_colourset_x = self.data_plot_obj_list[rep_indx].stored_x[stored_x_index].take_colour_from_set[
                self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
            if take_colourset_x is not None:
                take_colourset_x_bool = True
        else:
            take_colourset_x = None
        if stored_y_index is not None:
            take_colourset_y = self.data_plot_obj_list[rep_indx].stored_y[stored_y_index].take_colour_from_set[
                self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
            if take_colourset_y is not None:
                take_colourset_y_bool = True
        else:
            take_colourset_y = None
        if stored_z_index is not None:
            take_colourset_z = self.data_plot_obj_list[rep_indx].stored_z[stored_z_index].take_colour_from_set[
                self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
            if take_colourset_z is not None:
                take_colourset_z_bool = True
        else:
            take_colourset_z = None
        take_colourset_bool = False

        take_colourset = [take_colourset_x, take_colourset_y, take_colourset_z]
        take_colourset_bool_list = [take_colourset_x_bool, take_colourset_y_bool, take_colourset_z_bool]
        index_take_col_list= []
        if take_colourset_bool_list.count(True) == 1:  # User has defined colour
            index_take_col = take_colourset_bool_list.index(True)
            set_from = take_colourset[index_take_col]
            take_colourset_bool = True
            index_take_col_list.append(index_take_col)
        elif take_colourset_bool_list.count(True) == 0:  # Colour set defined to take from
            pass
        else:  # User has defined too many colours
            inxc_list = []
            for inxc in range(len(take_colourset)):
                if take_colourset_bool_list[inxc]:
                    inxc_list.append(take_colourset[inxc])
            for k in range(1, len(inxc_list)):
                if inxc_list[k] != inxc_list[0]:
                    raise ValueError(
                        "more than 1 colour is input, non repetition " + str(inxc_list[k]) + ' ' + str(inxc_list[0]))
            for k in range(len(inxc_list)):
                index_take_col_list.append(k)
            # If the colours defined were all the same then just choose the one defined
            index_take_col = take_colourset_bool_list.index(True)
            set_from = take_colourset[index_take_col]
            take_colourset_bool = True
        return take_colourset_bool, set_from, index_take_col_list

    def colour_auto_defined(self, rep_indx, set, i):
        stored_x_index = self.data_plot_obj_list[rep_indx].index_link[set][i][0]
        stored_y_index = self.data_plot_obj_list[rep_indx].index_link[set][i][1]
        stored_z_index = self.data_plot_obj_list[rep_indx].index_link[set][i][2]
        colour_was_used = False

        if self.colour_sorting_type == 'loop_by_data_type':  # every data type has the same colour, so it is stored in stored_x, y, z
            if stored_x_index is not None:
                if self.data_plot_obj_list[rep_indx].stored_x_side_dict[str(stored_x_index)]['color_data_type'] is None:
                    self.data_plot_obj_list[rep_indx].stored_x_side_dict[str(stored_x_index)]['color_data_type'] = \
                        self.default_col_list[self.data_plot_obj_list[rep_indx].index_for_col_marker_list]
                    colour_was_used = True

            if stored_y_index is not None:
                if self.data_plot_obj_list[rep_indx].stored_y_side_dict[str(stored_y_index)]['color_data_type'] is None:
                    self.data_plot_obj_list[rep_indx].stored_y_side_dict[str(stored_y_index)]['color_data_type'] = \
                        self.default_col_list[self.data_plot_obj_list[rep_indx].index_for_col_marker_list]
                    colour_was_used = True

            if stored_z_index is not None:
                if self.data_plot_obj_list[rep_indx].stored_z_side_dict[str(stored_z_index)]['color_data_type'] is None:
                    self.data_plot_obj_list[rep_indx].stored_z_side_dict[str(stored_z_index)]['color_data_type'] = \
                        self.default_col_list[self.data_plot_obj_list[rep_indx].index_for_col_marker_list]
                    colour_was_used = True

        if self.colour_sorting_type == 'unique_every_line':  # signal has a separate colour, so colour in psetting can be updated directly
            self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour'] = self.default_col_list[self.data_plot_obj_list[rep_indx].index_for_col_marker_list]
            colour_was_used = True
        if colour_was_used:
            self.data_plot_obj_list[rep_indx].index_for_col_marker_list = self.data_plot_obj_list[rep_indx].index_for_col_marker_list + 1

    def ax_specific_setting_updater(self, use_twin_ax=False):
        """Legend location ...  Things that are specific to an axis"""
        for rep_indx in range(self.max_index_repeated):
            if use_twin_ax:
                spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
            else:
                spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]
            spec_h_use.ax_settings_dict_array = np.empty((np.shape(self.ax_index_list_unique_max)[0], ), dtype=object)

            for ax_n in range(np.shape(self.ax_index_list_unique_max)[0]):
                spec_h_use.ax_settings_dict_array[ax_n] = {}
                spec_h_use.ax_settings_dict_array[ax_n]['leg_loc'] = None
            for set in range(len(self.set_num_list)):
                for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                    if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (
                            not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                        index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                        if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['leg_loc'] is not None:
                            print(index)
                            print("1leg_loc", spec_h_use.ax_settings_dict_array[index[0][0]]['leg_loc'], self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['leg_loc'])
                            print("2", self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['leg_loc'])
                            spec_h_use.ax_settings_dict_array[index[0][0]]['leg_loc'] = self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['leg_loc']
            # Update legend if the
            for ax_n in range(np.shape(self.ax_index_list_unique_max)[0]):
                if spec_h_use.ax_settings_dict_array[ax_n]['leg_loc'] is None:
                    spec_h_use.ax_settings_dict_array[ax_n]['leg_loc'] = self.default_leg_loc
                print("1leg_loc", spec_h_use.ax_settings_dict_array[ax_n]['leg_loc'])

    def colour_updater(self):
        loop_by_ax = False  # otherwise by set
        for rep_indx in range(self.max_index_repeated):
            self.data_plot_obj_list[rep_indx].already_set_i = []
            self.data_plot_obj_list[rep_indx].already_set_set = []
            self.data_plot_obj_list[rep_indx].set_reduced_by = np.zeros(len(self.set_num_list))
            self.data_plot_obj_list[rep_indx].colour_defined_set = []
            self.data_plot_obj_list[rep_indx].colour_defined_i = []

        if loop_by_ax:
            for rep_indx in range(self.max_index_repeated):
                for ax_n in range(np.shape(self.ax_index_list_unique_max)[0]):
                    for j in range(int(self.max_set_index_link_list[ax_n])):
                        if j < len(self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list[ax_n][0]):  # The array is larger for some pulses
                            set = self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list[ax_n][1][j]
                            i = self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list[ax_n][2][j]
                            colour_was_used = self.colour_user_defined(rep_indx, set, i)
                            take_colour_will_be_used, take_colour_set_from, take_colour_xyz_index_list = self.colour_take_colourset_checker(rep_indx, set, i)
                            self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['take_colour_bool'] = take_colour_will_be_used
                            self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['take_colour_set_from'] = take_colour_set_from
                            self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['take_colour_xyz_index_list'] = take_colour_xyz_index_list
                            if colour_was_used:
                                self.data_plot_obj_list[rep_indx].colour_defined_set.append(set)
                                self.data_plot_obj_list[rep_indx].colour_defined_i.append(i)
                            if colour_was_used or take_colour_will_be_used:
                                self.data_plot_obj_list[rep_indx].already_set_set.append(set)
                                self.data_plot_obj_list[rep_indx].already_set_i.append(i)
                                self.data_plot_obj_list[rep_indx].set_reduced_by[set] = self.data_plot_obj_list[rep_indx].set_reduced_by[set] + 1
        else:
            for rep_indx in range(self.max_index_repeated):
                for set in range(len(self.set_num_list)):
                    for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                        colour_was_used = self.colour_user_defined(rep_indx, set, i)
                        take_colour_will_be_used, take_colour_set_from, take_colour_xyz_index_list = self.colour_take_colourset_checker(rep_indx, set, i)
                        self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['take_colour_bool'] = take_colour_will_be_used
                        self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['take_colour_set_from'] = take_colour_set_from
                        self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['take_colour_xyz_index_list'] = take_colour_xyz_index_list
                        if colour_was_used:
                            self.data_plot_obj_list[rep_indx].colour_defined_set.append(set)
                            self.data_plot_obj_list[rep_indx].colour_defined_i.append(i)
                        if colour_was_used or take_colour_will_be_used:
                            self.data_plot_obj_list[rep_indx].already_set_set.append(set)
                            self.data_plot_obj_list[rep_indx].already_set_i.append(i)
                            self.data_plot_obj_list[rep_indx].set_reduced_by[set] = self.data_plot_obj_list[rep_indx].set_reduced_by[set] + 1

        # Set automatic colours that are not in above list and not in type
        for rep_indx in range(self.max_index_repeated):
            self.data_plot_obj_list[rep_indx].already_set_set = np.array(self.data_plot_obj_list[rep_indx].already_set_set)
            self.data_plot_obj_list[rep_indx].already_set_i = np.array(self.data_plot_obj_list[rep_indx].already_set_i)
        if loop_by_ax:
            for rep_indx in range(self.max_index_repeated):
                for ax_n in range(np.shape(self.ax_index_list_unique_max)[0]):
                    for j in range(int(self.max_set_index_link_list[ax_n])):
                        if j < len(self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list[ax_n][0]):  # The array is larger for some pulses
                            set = self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list[ax_n][1][j]
                            i = self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list[ax_n][2][j]
                            bool_set_and_i = np.logical_and(self.data_plot_obj_list[rep_indx].already_set_set==set, self.data_plot_obj_list[rep_indx].already_set_i==i)
                            num_times_true = np.sum(bool_set_and_i)
                            if num_times_true == 0:  # The set has already been used
                                self.colour_auto_defined(rep_indx, set, i)
                            elif num_times_true == 1:
                                pass
                            else:
                                raise ValueError("unexpecteed result summing boolean")
                self.data_plot_obj_list[rep_indx].index_for_col_marker_list = 0  # Change the indentation for reseting colours over pulses
        else:
            for rep_indx in range(self.max_index_repeated):
                for set in range(len(self.set_num_list)):
                    for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                        bool_set_and_i = np.logical_and(self.data_plot_obj_list[rep_indx].already_set_set == set, self.data_plot_obj_list[rep_indx].already_set_i == i)
                        # Above has 1 true if set has been used or all false if not used
                        num_times_true = np.sum(bool_set_and_i)
                        if num_times_true == 0:
                            self.colour_auto_defined(rep_indx, set, i)
                        elif num_times_true == 1:  # The set has already been used
                            pass
                        else:
                            raise ValueError("unexpecteed result summing boolean")
            self.data_plot_obj_list[rep_indx].index_for_col_marker_list = 0  # Change the indentation for reseting colours over pulses
        for rep_indx in range(self.max_index_repeated):
            for set in range(len(self.set_num_list)):
                for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                    stored_y_index = self.data_plot_obj_list[rep_indx].index_link[set][i][1]

        # Overwrite colours using take_colour_from_set
        for rep_indx in range(self.max_index_repeated):
            for set in range(len(self.set_num_list)):
                index_from = 0  # To loop back if it runs out of index
                for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['take_colour_bool']:
                        set_from = self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['take_colour_set_from']
                        if len(self.data_plot_obj_list[rep_indx].index_link[set_from]) == index_from:  # Loop back index to 0 if it goes over the number of cols in set
                            index_from = 0
                        stored_x_index = self.data_plot_obj_list[rep_indx].index_link[set][i][0]
                        stored_y_index = self.data_plot_obj_list[rep_indx].index_link[set][i][1]
                        stored_z_index = self.data_plot_obj_list[rep_indx].index_link[set][i][2]
                        stored_x_index_from = self.data_plot_obj_list[rep_indx].index_link[set_from][index_from][0]
                        stored_y_index_from = self.data_plot_obj_list[rep_indx].index_link[set_from][index_from][1]
                        stored_z_index_from = self.data_plot_obj_list[rep_indx].index_link[set_from][index_from][2]
                        if self.colour_sorting_type == 'loop_by_data_type':  # every data type has the same colour, so it is stored in stored_x, y, z
                            for k in self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['take_colour_xyz_index_list']:
                                if k == 0:
                                    if stored_x_index_from is not None and stored_x_index is not None:
                                        self.data_plot_obj_list[rep_indx].stored_x_side_dict[str(stored_x_index)]['color_data_type'] = self.data_plot_obj_list[rep_indx].stored_x_side_dict[str(stored_x_index_from)]['color_data_type']
                                if k == 1:
                                    if stored_y_index_from is not None and stored_y_index is not None:
                                        self.data_plot_obj_list[rep_indx].stored_y_side_dict[str(stored_y_index)]['color_data_type'] = self.data_plot_obj_list[rep_indx].stored_y_side_dict[str(stored_y_index_from)]['color_data_type']
                                if k == 2:
                                    if stored_z_index_from is not None and stored_z_index is not None:
                                        self.data_plot_obj_list[rep_indx].stored_z_side_dict[str(stored_z_index)]['color_data_type'] = self.data_plot_obj_list[rep_indx].stored_z_side_dict[str(stored_z_index_from)]['color_data_type']
                        if self.colour_sorting_type == 'unique_every_line':  # signal has a separate colour, so colour in psetting can be updated directly
                            self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour'] = self.data_plot_obj_list[rep_indx].dict_psettings_array[set_from][i]['colour']
                    index_from = index_from + 1
        # Populate self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour'] with correct colours if loop_by_data_type
        if self.colour_sorting_type == 'loop_by_data_type':
            for rep_indx in range(self.max_index_repeated):
                for set in range(len(self.set_num_list)):
                    for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                        stored_x_index = self.data_plot_obj_list[rep_indx].index_link[set][i][0]
                        stored_y_index = self.data_plot_obj_list[rep_indx].index_link[set][i][1]
                        stored_z_index = self.data_plot_obj_list[rep_indx].index_link[set][i][2]
                        bool_set_and_i_col = np.logical_and(self.data_plot_obj_list[rep_indx].colour_defined_set == set,
                                                        self.data_plot_obj_list[rep_indx].colour_defined_i == i)
                        self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['colour'] = \
                        self.data_plot_obj_list[rep_indx].stored_y_side_dict[str(stored_y_index)]['color_data_type']
    def set_lims(self, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            ax_to_use = self.ax_twin
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
            set_caption = False
            # dict_kwargs_array_use = self.data_plot_obj_list[rep_indx].specific_holder[1].dict_kwargs_array
        else:
            set_caption = True
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]
            ax_to_use = self.ax
            # dict_kwargs_array_use = self.data_plot_obj_list[rep_indx].specific_holder[0].dict_kwargs_array

        # Set the axis limits if there is an entry that should be ignored for the axis limits
        for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
            set_list = self.data_plot_obj_list[rep_indx].set_i_to_ax[ax_n]['set']
            i_list = self.data_plot_obj_list[rep_indx].set_i_to_ax[ax_n]['i']
            ax_exists = False
            for indx in range(len(set_list)):
                set_use = set_list[indx]
                i_use = i_list[indx]
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set_use][i_use]) or (
                        not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set_use][i_use]):
                    ax_exists = True
            if ax_exists:
                if self.data_plot_obj_list[rep_indx].ax_resize_x[ax_n] or self.make_room_for_legend:
                    spec_h_use.xlims[ax_n] = xlims
                    ax_to_use[int(
                        self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0] + self.data_plot_obj_list[
                            rep_indx].shift_x_ax_mult)][int(
                        self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1] + self.data_plot_obj_list[
                            rep_indx].shift_y_ax_mult)].set_xlim(xlims)
                if self.data_plot_obj_list[rep_indx].ax_resize_y[ax_n]:
                    spec_h_use.ylims[ax_n] = ylims
                    ax_to_use[int(
                        self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0] + self.data_plot_obj_list[
                            rep_indx].shift_x_ax_mult)][int(
                        self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1] + self.data_plot_obj_list[
                            rep_indx].shift_y_ax_mult)].set_ylim(ylims)
                if self.data_plot_obj_list[rep_indx].ax_resize_z[ax_n]:
                    spec_h_use.zlims[ax_n] = zlims
                    ax_to_use[int(
                        self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0] + self.data_plot_obj_list[
                            rep_indx].shift_x_ax_mult)][int(
                        self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1] + self.data_plot_obj_list[
                            rep_indx].shift_y_ax_mult)].set_zlim(zlims)

    def decorate_plot(self, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            ax_to_use = self.ax_twin
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
            set_caption = False
            # dict_kwargs_array_use = self.data_plot_obj_list[rep_indx].specific_holder[1].dict_kwargs_array
        else:
            set_caption = True
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]
            ax_to_use = self.ax
            # dict_kwargs_array_use = self.data_plot_obj_list[rep_indx].specific_holder[0].dict_kwargs_array
        spec_h_use.xlims = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], 2), np.nan)
        spec_h_use.ylims = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], 2), np.nan)
        spec_h_use.zlims = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], 2), np.nan)

        self.data_plot_obj_list[rep_indx].set_i_to_ax = [{} for _ in range(np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0])]

        for lg in range(len(self.data_plot_obj_list[rep_indx].set_i_to_ax)):
            self.data_plot_obj_list[rep_indx].set_i_to_ax[lg]['label'] = []
            self.data_plot_obj_list[rep_indx].set_i_to_ax[lg]['set'] = []
            self.data_plot_obj_list[rep_indx].set_i_to_ax[lg]['i'] = []


        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                index = index[0][0]  # For list only on
                self.data_plot_obj_list[rep_indx].set_i_to_ax[index]['set'].append(set)
                self.data_plot_obj_list[rep_indx].set_i_to_ax[index]['i'].append(i)
                self.tester.run_test('decorate_plot', {'object': self, 'prog_index_list_unfixed': [rep_indx, set, i]})

        # Set the axis limits if there is an entry that should be ignored for the axis limits
        for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
            set_list = self.data_plot_obj_list[rep_indx].set_i_to_ax[ax_n]['set']
            i_list = self.data_plot_obj_list[rep_indx].set_i_to_ax[ax_n]['i']
            ax_exists = False
            for indx in range(len(set_list)):
                set_use = set_list[indx]
                i_use = i_list[indx]
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set_use][i_use]) or (
                        not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set_use][i_use]):
                    ax_exists = True
            if ax_exists:
                if self.data_plot_obj_list[rep_indx].ax_resize_x[ax_n] or self.make_room_for_legend:
                    xlims = spec_h_use.ax_max_min_x_unique_normalised[ax_n]
                    if self.make_room_for_legend:
                        extra_percent_add_right = self.percent_to_add_around_ax_lim_change + 0.15
                    else:
                        extra_percent_add_right = 0.0
                    extra_percent_add_left = self.percent_to_add_around_ax_lim_change  # TODO add if the legend is on the left
                    xlims[0] = xlims[0] - (spec_h_use.ax_max_min_x_unique_normalised[ax_n][0] - spec_h_use.ax_max_min_x_unique_normalised[ax_n][0])*extra_percent_add_left
                    xlims[1] = xlims[1] + (spec_h_use.ax_max_min_x_unique_normalised[ax_n][1] - spec_h_use.ax_max_min_x_unique_normalised[ax_n][1])*extra_percent_add_right
                    spec_h_use.xlims[ax_n] = [np.nanmin([xlims[0], spec_h_use.xlims[ax_n][0]]), np.nanmax([xlims[1], spec_h_use.xlims[ax_n][1]])]
                    # ax_to_use[int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult)][int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult)].set_xlim(xlims)
                if self.data_plot_obj_list[rep_indx].ax_resize_y[ax_n]:
                    ylims = spec_h_use.ax_max_min_y_unique_normalised[ax_n]
                    ylims[0] = ylims[0] - (spec_h_use.ax_max_min_y_unique_normalised[ax_n][0] - spec_h_use.ax_max_min_y_unique_normalised[ax_n][0])*self.percent_to_add_around_ax_lim_change
                    ylims[1] = ylims[1] + (spec_h_use.ax_max_min_y_unique_normalised[ax_n][1] - spec_h_use.ax_max_min_y_unique_normalised[ax_n][1])*self.percent_to_add_around_ax_lim_change
                    spec_h_use.ylims[ax_n] = [np.nanmin([ylims[0], spec_h_use.ylims[ax_n][0]]), np.nanmax([ylims[1], spec_h_use.ylims[ax_n][1]])]

                    # ax_to_use[int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult)][int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult)].set_ylim(ylims)
                if self.data_plot_obj_list[rep_indx].ax_resize_z[ax_n]:
                    zlims = spec_h_use.ax_max_min_z_unique_normalised[ax_n]
                    zlims[0] = zlims[0] - (spec_h_use.ax_max_min_z_unique_normalised[ax_n][0] - spec_h_use.ax_max_min_z_unique_normalised[ax_n][0])*self.percent_to_add_around_ax_lim_change
                    zlims[1] = zlims[1] + (spec_h_use.ax_max_min_z_unique_normalised[ax_n][1] - spec_h_use.ax_max_min_z_unique_normalised[ax_n][1])*self.percent_to_add_around_ax_lim_change
                    spec_h_use.zlims[ax_n] = [np.nanmin([zlims[0], spec_h_use.zlims[ax_n][0]]), np.nanmax([zlims[1], spec_h_use.zlims[ax_n][1]])]
                    # ax_to_use[int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult)][int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult)].set_zlim(zlims)
            print(ax_n, ax_exists)

        self.ax_equal_xy = np.full(np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], False)
        for ax_n in range(len(self.data_plot_obj_list[rep_indx].set_i_to_ax)):
            set_list = self.data_plot_obj_list[rep_indx].set_i_to_ax[ax_n]['set']
            i_list = self.data_plot_obj_list[rep_indx].set_i_to_ax[ax_n]['i']
            equal_xy_bool = False
            ax_exists = False
            for all_in_ax in range(len(set_list)):
                if self.data_plot_obj_list[rep_indx].dict_psettings_array[set_list[all_in_ax]][i_list[all_in_ax]]['equal_xy']:
                    if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set_list[all_in_ax]][i_list[all_in_ax]]) or (
                            not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set_list[all_in_ax]][i_list[all_in_ax]]):
                        ax_exists = True
                    equal_xy_bool = True
            self.ax_equal_xy[ax_n] = equal_xy_bool
            if self.ax_equal_xy[ax_n]:
                if ax_exists:
                    ax_to_use[int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0] + self.data_plot_obj_list[
                        rep_indx].shift_x_ax_mult)][int(
                        self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1] + self.data_plot_obj_list[
                            rep_indx].shift_y_ax_mult)].set_aspect('equal', 'box')

        # Select the labels to be the general label or
        for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
            set_list = self.data_plot_obj_list[rep_indx].set_i_to_ax[ax_n]['set']
            i_list = self.data_plot_obj_list[rep_indx].set_i_to_ax[ax_n]['i']
            works_this_loop = False
            for all_in_ax in range(len(set_list)):
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set_list[all_in_ax]][i_list[all_in_ax]]) or (not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set_list[all_in_ax]][i_list[all_in_ax]]):
                    works_this_loop = True
            if works_this_loop:
                label_x, units_x, _ = self.general_decorate_plot(
                    index_xyx=0, dict_array_get_from=spec_h_use.dict_x_settings_array, ax_n=ax_n, rep_indx=rep_indx, use_twin_ax=use_twin_ax
                )
                label_y, units_y, _ = self.general_decorate_plot(
                    index_xyx=1, dict_array_get_from=spec_h_use.dict_y_settings_array, ax_n=ax_n, rep_indx=rep_indx, use_twin_ax=use_twin_ax
                )
                label_z, units_z, _ = self.general_decorate_plot(
                    index_xyx=2, dict_array_get_from=spec_h_use.dict_z_settings_array, ax_n=ax_n, rep_indx=rep_indx, use_twin_ax=use_twin_ax
                )
                if units_x is not None:
                    units_x = ' (' + units_x + ') '
                if units_y is not None:
                    units_y = ' (' + units_y + ') '
                if units_z is not None:
                    units_z = ' (' + units_z + ') '

                if units_x== ' () ':
                    units_x = ''
                if units_y== ' () ':
                    units_y = ''
                if units_z== ' () ':
                    units_z = ''
                print('units_y', units_y, label_y)


                # if units_x is not None and legend_label_x is not None:
                #     spec_h_use.dict_x_settings_array[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['legend_label_x'] = legend_label_x + units_x
                # else:
                #     spec_h_use.dict_x_settings_array[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['legend_label_x'] = None
                # if units_y is not None and legend_label_y is not None:
                #     spec_h_use.dict_y_settings_array[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['legend_label_y'] = legend_label_y + units_y
                # else:
                #     spec_h_use.dict_y_settings_array[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['legend_label_y'] = None
                # if units_z is not None and legend_label_z is not None:
                #     spec_h_use.dict_z_settings_array[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['legend_label_z'] = legend_label_z + units_z
                # else:
                #     spec_h_use.dict_z_settings_array[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['legend_label_z'] = None
                add_plot_label_size_share_x = 0.
                add_plot_label_size_share_y = 0.
                if self.share_ax_x:
                    if int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0] + self.data_plot_obj_list[
                        rep_indx].shift_x_ax_mult) != self.true_n_x_ax - 1:
                        label_x = ''
                        units_x = ''
                        add_plot_label_size_share_x = self.add_plot_size_shared / (float(self.true_n_y_ax))
                if self.share_ax_y:
                    if use_twin_ax:
                        if int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult) != self.true_n_y_ax - 1:
                            label_y = ''
                            units_y = ''
                            add_plot_label_size_share_y = self.add_plot_size_shared / (float(self.true_n_x_ax))
                    else:
                        if int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1] + self.data_plot_obj_list[rep_indx].shift_y_ax_mult) > 0:
                            label_y = ''
                            units_y = ''
                            add_plot_label_size_share_y = self.add_plot_size_shared / (float(self.true_n_x_ax))
                if units_x!= ' () ':
                    label_x_add = label_x + units_x
                else:
                    label_x_add = label_x
                if units_y!= ' () ':
                    label_y_add = label_y + units_y
                else:
                    label_y_add = label_y

                if self.data_plot_obj_list[rep_indx].legend_font_size is not None and self.data_plot_obj_list[rep_indx].dict_psettings_array[spec_h_use.set_link_unique[ax_n]][spec_h_use.index_link_unique[ax_n]]['legend_on']:
                    legend_font_size = self.data_plot_obj_list[rep_indx].legend_font_size[ax_n]  #
                    set_legend_bool = True
                else:
                    legend_font_size = None
                    set_legend_bool = False
                if not spec_h_use.all_legends_off[ax_n]:
                    set_legend_bool = False

                print('label_x', label_x_add )
                colour_twin_ax = None
                if use_twin_ax:
                    for all_in_ax in range(len(set_list)):
                        if use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set_list[all_in_ax]][i_list[all_in_ax]]:
                            colour_twin_ax = self.data_plot_obj_list[rep_indx].dict_psettings_array[set_list[all_in_ax]][i_list[all_in_ax]]['colour']

                UsefulPlottingMethods.refine_ax(ax_to_use[int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult)][int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult)],
                                                label_x_add, label_y_add, poster_bool=True, leg_loc=self.data_plot_obj_list[rep_indx].dict_psettings_array[set_list[all_in_ax]][i_list[all_in_ax]]['leg_loc'],
                                                ax_x_label_size=self.size_of_plot_labels / (float(self.true_n_y_ax))+add_plot_label_size_share_x,
                                                ax_y_label_size=self.size_of_plot_labels / (float(self.true_n_x_ax))+add_plot_label_size_share_y, scale_legend=self.size_of_plot_labels / (10.5*float(self.n_x_ax)),
                                                border_line_width=0.2, make_bf_latex=self.make_bf_latex, legend_size_precal=legend_font_size, set_legend_bool=set_legend_bool, is_twin_ax=use_twin_ax, colour_twin_ax=colour_twin_ax, fix_leg_label_size=self.fix_leg_label_size, fix_x_label_size=self.fix_x_label_size, fix_y_label_size=self.fix_y_label_size, labelpad_twin_ax=self.labelpad_twin_ax
                                                )
                if set_legend_bool:
                    if self.legend_below:
                        handles, labels = ax_to_use[int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult)][int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult)].get_legend_handles_labels()
                        lgd = ax_to_use[int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult)][int(self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n][1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult)].legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, -0.1))
        if self.set_caption_abc and not self.save_to_svg and set_caption:
            if self.set_caption_independent:
                index = 0
            else:
                index = self.set_caption_abc_index
            for ix in range(self.n_x_ax):
                for iy in range(self.n_y_ax):
                    if ax_to_use[ix+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][iy+self.data_plot_obj_list[rep_indx].shift_y_ax_mult] is not None:
                        caption_xy = '(' + chr(97 + index) + ')'
                        if self.make_bf_latex:
                            caption_xy = UsefulStringMethods.surround_2_string(caption_xy, start=r'\textbf{', end='}')
                        ax_to_use[ix+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][iy+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].text(0.04, 0.9, caption_xy,
                             horizontalalignment='center',
                             verticalalignment='center',
                             transform=ax_to_use[ix+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][iy+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].transAxes, size=self.abc_font_size)
                        index = index + 1
                        self.set_caption_abc_index = self.set_caption_abc_index + 1

        # If sharing axes, remove the white space between them
        if self.share_ax_x:
            self.fig.subplots_adjust(hspace=0)
        if self.share_ax_y:
            self.fig.subplots_adjust(wspace=0)
        if self.bottom is not None:
            self.fig.subplots_adjust(bottom=self.bottom)
        if self.left is not None:
            self.fig.subplots_adjust(left=self.left)
        if self.right is not None:
            self.fig.subplots_adjust(right=self.right)

    def add_legend_label(self, rep_indx, x_or_y_or_z='y', use_twin_ax=False):
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]
        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (
                        not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                    print(set, i, x_or_y_or_z, self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot'])
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot']:
                        self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on'] = False
                        if self.labels_type == 'specific':
                            if x_or_y_or_z == 'x':
                                legend_label = spec_h_use.dict_x_settings_array[set][i]['label_x']
                                units = spec_h_use.dict_x_settings_array[set][i]['units_x']
                            if x_or_y_or_z == 'y':
                                legend_label = spec_h_use.dict_y_settings_array[set][i]['label_y']
                                units = spec_h_use.dict_y_settings_array[set][i]['units_y']
                            if x_or_y_or_z == 'z':
                                legend_label = spec_h_use.dict_z_settings_array[set][i]['label_z']
                                units = spec_h_use.dict_z_settings_array[set][i]['units_z']
                        elif self.labels_type == 'off' or self.labels_type == 'off_if_not_multiple':
                            if x_or_y_or_z == 'x':
                                legend_label = None
                                units = None
                            if x_or_y_or_z == 'y':
                                legend_label = None
                                units = None
                            if x_or_y_or_z == 'z':
                                legend_label = None
                                units = None
                        if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'line_plot':
                            if legend_label is not None:
                                if len(units) == 0:
                                    total_legend = legend_label
                                else:
                                    total_legend = legend_label + ' (' + units + ') '
                                if spec_h_use.overwrite_legend_label[set][i] is not None:
                                    total_legend = spec_h_use.overwrite_legend_label[set][i]
                                spec_h_use.dict_kwargs_array[set][i]['label'] = total_legend
                                self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on'] = True
                            else:
                                self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on'] = False
                        if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'error_plot':
                            if legend_label is not None:
                                if len(units) == 0:
                                    total_legend = legend_label
                                else:
                                    total_legend = legend_label + ' (' + units + ') '

                                if spec_h_use.overwrite_legend_label[set][i] is not None:
                                    total_legend = spec_h_use.overwrite_legend_label[set][i]
                                spec_h_use.dict_kwargs_array[set][i]['label'] = total_legend
                                self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on'] = True
                            else:
                                self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on'] = False
                        if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'error_example':
                            pass
                        if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'arrow_plot':
                            pass
                        if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_plot':
                            pass
                        if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'scatter':
                            if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                                pass
                            else:
                                if legend_label is not None:
                                    if len(units) == 0:
                                        total_legend = legend_label
                                    else:
                                        total_legend = legend_label + ' (' + units + ') '

                                    if spec_h_use.overwrite_legend_label[set][i] is not None:
                                        total_legend = spec_h_use.overwrite_legend_label[set][i]
                                    spec_h_use.dict_kwargs_array[set][i]['label'] = total_legend
                                    self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on'] = True
                                else:
                                    self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on'] = False
                        print('legend_on', self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on'])

        spec_h_use.all_legends_off = np.full(np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], False)
        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (
                        not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot']:
                        if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on']:
                            index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                            spec_h_use.all_legends_off[index] = True
        print('legend_off', spec_h_use.all_legends_off)
        spec_h_use.list_array_ax_label = [{} for _ in range(np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0])]

        for lg in range(len(spec_h_use.list_array_ax_label)):
            spec_h_use.list_array_ax_label[lg]['label'] = []
            spec_h_use.list_array_ax_label[lg]['set'] = []
            spec_h_use.list_array_ax_label[lg]['i'] = []
        self.data_plot_obj_list[rep_indx].at_least_1_label = False
        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (
                        not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot']:
                        if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'line_plot' or self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'error_plot' or (self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'scatter' and self.data_plot_obj_list[rep_indx].z_data[set][i] is None):
                            index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                            index = index[0][0]  # For list only on
                            if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on']:
                                spec_h_use.list_array_ax_label[index]['label'].append(spec_h_use.dict_kwargs_array[set][i]['label'])
                            spec_h_use.list_array_ax_label[index]['set'].append(set)
                            spec_h_use.list_array_ax_label[index]['i'].append(i)
                            if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['legend_on']:
                                self.data_plot_obj_list[rep_indx].at_least_1_label = True

    def find_max_num_newlines_and_len(self):
        # Find the labels, len and num newlines
        tot_num_newlines = 0
        max_tot_num_newlines = 0
        max_len = 0
        rep_indx_max_len = 0
        ax_index_max_len = 0
        lst_index_max_len = 0
        for rep_indx in range(self.max_index_repeated):
            self.data_plot_obj_list[rep_indx].legend_font_size = None
            for lg in range(len(spec_h_use.list_array_ax_label)):
                spec_h_use.list_array_ax_label[lg]['len'] = np.zeros(
                    (len(spec_h_use.list_array_ax_label[lg]['label']),))
                spec_h_use.list_array_ax_label[lg]['num_newlines'] = np.zeros(
                    (len(spec_h_use.list_array_ax_label[lg]['label']),))
                for lst in range(len(spec_h_use.list_array_ax_label[lg]['len'])):
                    spec_h_use.list_array_ax_label[lg]['len'][
                        lst] = UsefulStringMethods.len_string_newline_frac(
                        spec_h_use.list_array_ax_label[lg]['label'][lst])
                    spec_h_use.list_array_ax_label[lg]['num_newlines'][
                        lst] = UsefulStringMethods._count_newlines(
                        spec_h_use.list_array_ax_label[lg]['label'][lst])

        if self.index_stack_direction == 'stack':
            tot_num_newlines = np.zeros(len(spec_h_use.list_array_ax_label))
            for rep_indx in range(self.max_index_repeated):
                self.data_plot_obj_list[rep_indx].legend_font_size = None
                for lg in range(len(spec_h_use.list_array_ax_label)):
                    for lst in range(len(spec_h_use.list_array_ax_label[lg]['len'])):
                        tot_num_newlines[lg] = tot_num_newlines[lg] + spec_h_use.list_array_ax_label[lg]['num_newlines'][lst]
                        max_choose = [max_len,  spec_h_use.list_array_ax_label[lg]['len'][lst]]
                        arg_max_choose = np.nanargmax(max_choose)
                        max_len = max_choose[arg_max_choose]
                        if arg_max_choose == 1:  # if the new len is larger above
                            rep_indx_max_len = rep_indx
                            ax_index_max_len = lg
                            lst_index_max_len = lst
                        max_len_last = max_len
            max_tot_num_newlines = np.nanmax(tot_num_newlines)
        if self.index_stack_direction == 'vertical':
            tot_num_newlines = np.zeros(len(spec_h_use.list_array_ax_label))
            for rep_indx in range(self.max_index_repeated):
                tot_num_newlines_ax = np.zeros(len(spec_h_use.list_array_ax_label))
                self.data_plot_obj_list[rep_indx].legend_font_size = None
                for lg in range(len(spec_h_use.list_array_ax_label)):
                    for lst in range(len(spec_h_use.list_array_ax_label[lg]['len'])):
                        tot_num_newlines_ax[lg] = tot_num_newlines_ax[lg] + spec_h_use.list_array_ax_label[lg]['num_newlines'][lst]
                        max_choose = [max_len, spec_h_use.list_array_ax_label[lg]['len'][lst]]
                        arg_max_choose = np.nanargmax(max_choose)
                        max_len = max_choose[arg_max_choose]
                        if arg_max_choose == 1:  # if the new len is larger above
                            rep_indx_max_len = rep_indx
                            ax_index_max_len = lg
                            lst_index_max_len = lst
                        max_len_last = max_len
                    tot_num_newlines[lg] = np.nanmax([tot_num_newlines[lg], tot_num_newlines_ax[lg]])
            max_tot_num_newlines = np.nanmax(tot_num_newlines)
        self.extend_ax_by = max_len
        return max_tot_num_newlines, max_len, rep_indx_max_len, ax_index_max_len, lst_index_max_len

    def legend_sizing2(self):
        resize_legend = True
        resize_legend_to_box_ratio = True  # resizes legend to fit the
        max_starting_leg_size_per_num_lines_per_subplots = 280.  # max size with no overlapping of weird legend behavior vertically
        proportion_legend_vert = self.proportion_legend_vert  # 1.
        proportion_legend_horiz = self.proportion_legend_horiz  # 2.
        if self.use_afm:
            afm_rough = self.afm.string_width_height('W')
            roughconversion_height_towidth = float(afm_rough[0]) / float(afm_rough[1])
        else:
            roughconversion_height_towidth = 1.4259818731117824

        max_height_legend_box = max_starting_leg_size_per_num_lines_per_subplots * proportion_legend_vert
        ratio_hw = roughconversion_height_towidth * proportion_legend_horiz / proportion_legend_vert
        at_least_1_label = False
        for rep_indx in range(self.max_index_repeated):
            if self.data_plot_obj_list[rep_indx].at_least_1_label:
                at_least_1_label = True
        if at_least_1_label:
            if resize_legend:
                tot_num_newlines, max_len, rep_indx_max_len, ax_index_max_len, lst_index_max_len = self.find_max_num_newlines_and_len()
        if at_least_1_label:
            if resize_legend:
                if resize_legend_to_box_ratio:
                    while max_len > tot_num_newlines * ratio_hw:
                        used_while = True
                        # change label split newline on the max length one
                        self.data_plot_obj_list[rep_indx_max_len].list_array_ax_label[ax_index_max_len]['label'], \
                        exitcond = UsefulStringMethods.split_newline_best(None, list_of_strings_in_instead=
                        self.data_plot_obj_list[rep_indx_max_len].list_array_ax_label[ax_index_max_len]['label'])
                        tot_num_newlines, max_len, rep_indx_max_len, ax_index_max_len, lst_index_max_len = self.find_max_num_newlines_and_len()
                        if exitcond:
                            break

                # Resize the legend font size to fit the
                for rep_indx in range(self.max_index_repeated):
                    self.data_plot_obj_list[rep_indx].legend_font_size = np.full(
                        (np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)), max_height_legend_box)
                    # self.legend_font_size = 25.#/((float(self.n_x_ax))*)

                    for lg in range(len(spec_h_use.list_array_ax_label)):
                        max_unbroken_newline = np.nanmax(
                            spec_h_use.list_array_ax_label[lg]['len'])
                        n_newlines = np.sum(spec_h_use.list_array_ax_label[lg][
                                                'num_newlines'])  # number of newlines and legends added
                        scaling_font = np.max([np.max(max_unbroken_newline) * float(self.true_n_y_ax),
                                               n_newlines * float(self.true_n_x_ax)])
                        self.data_plot_obj_list[rep_indx].legend_font_size[lg] = \
                            self.data_plot_obj_list[rep_indx].legend_font_size[lg] / scaling_font

            else:
                for rep_indx in range(self.max_index_repeated):
                    self.data_plot_obj_list[rep_indx].legend_font_size = np.full(
                        (np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)), None)
                    for set in range(len(self.set_num_list)):
                        for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                            if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot']:
                                index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique ==
                                                 self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                                index = index[0][0]  # For list only on
                                spec_h_use.list_array_ax_label[index]['label'].append(
                                    spec_h_use.dict_kwargs_array[set][i]['label'])
        else:
            for rep_indx in range(self.max_index_repeated):
                self.data_plot_obj_list[rep_indx].legend_font_size = np.full(
                    (np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)), None)

        # set the absolute maximum legend size
        for rep_indx in range(self.max_index_repeated):
            if self.data_plot_obj_list[rep_indx].at_least_1_label:
                for lg in range(len(spec_h_use.list_array_ax_label)):
                    if self.data_plot_obj_list[rep_indx].legend_font_size[lg] > self.max_legend_font_size:
                        self.data_plot_obj_list[rep_indx].legend_font_size[lg] = self.max_legend_font_size
                for lg in range(len(spec_h_use.list_array_ax_label)):
                    for gen_index in range(len(spec_h_use.list_array_ax_label[lg]['label'])):
                        set = spec_h_use.list_array_ax_label[lg]['set'][gen_index]
                        i = spec_h_use.list_array_ax_label[lg]['i'][gen_index]
                        legend_label = spec_h_use.list_array_ax_label[lg]['label'][gen_index]
                        if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot']:
                            spec_h_use.dict_kwargs_array[set][i][
                                'label'] = UsefulStringMethods.surround_2_string(legend_label, start=r'\textbf{',
                                                                                 end='}')

    def legend_sizing(self):
        resize_legend = True
        resize_legend_to_box_ratio = True # resizes legend to fit the
        max_starting_leg_size_per_num_lines_per_subplots = 280.  # max size with no overlapping of weird legend behavior vertically
        proportion_legend_vert = self.proportion_legend_vert#1.
        proportion_legend_horiz = self.proportion_legend_horiz#2.
        if self.use_afm:
            afm_rough = self.afm.string_width_height('W')
            roughconversion_height_towidth = float(afm_rough[0]) / float(afm_rough[1])
        else:
            roughconversion_height_towidth = 1.4259818731117824

        max_height_legend_box = max_starting_leg_size_per_num_lines_per_subplots * proportion_legend_vert
        ratio_hw = roughconversion_height_towidth * proportion_legend_horiz / proportion_legend_vert
        at_least_1_label = False
        for rep_indx in range(self.max_index_repeated):
            if self.data_plot_obj_list[rep_indx].at_least_1_label:
                at_least_1_label = True
        if at_least_1_label:
            if resize_legend:
                tot_num_newlines, max_len, rep_indx_max_len, ax_index_max_len, lst_index_max_len = self.find_max_num_newlines_and_len()

        if at_least_1_label:
            if resize_legend:
                if resize_legend_to_box_ratio:
                    while max_len > tot_num_newlines * ratio_hw:
                        used_while = True
                        # change label split newline on the max length one
                        self.data_plot_obj_list[rep_indx_max_len].list_array_ax_label[ax_index_max_len]['label'],\
                        exitcond = UsefulStringMethods.split_newline_best(None, list_of_strings_in_instead=
                        self.data_plot_obj_list[rep_indx_max_len].list_array_ax_label[ax_index_max_len]['label'])
                        tot_num_newlines, max_len, rep_indx_max_len, ax_index_max_len, lst_index_max_len = self.find_max_num_newlines_and_len()
                        if exitcond:
                            break

                # Resize the legend font size to fit the
                for rep_indx in range(self.max_index_repeated):
                    self.data_plot_obj_list[rep_indx].legend_font_size = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)), max_height_legend_box)

                    for lg in range(len(spec_h_use.list_array_ax_label)):
                        set = spec_h_use.list_array_ax_label[lg]['set']
                        i = spec_h_use.list_array_ax_label[lg]['i']
                        n_newlines = np.sum(spec_h_use.list_array_ax_label[lg]['num_newlines'])  # number of newlines and legends added
                        max_unbroken_newline = np.nanmax(spec_h_use.list_array_ax_label[lg]['len'])

                        new_scaling_font_horizontal = proportion_legend_horiz/((1./1000.)*(4.+ np.max(max_unbroken_newline))*float(self.true_n_y_ax))  # 4 extra char for the legend edge and icon, font 200 with 4+1 fills the legend so contant of 1/1000
                        new_scaling_font_vertical = proportion_legend_vert/((1./400.)*(1.+ n_newlines)*float(self.true_n_x_ax))  # 4 extra char for the legend edge and icon, font 200 with 4+1 fills the legend so contant of 1/1000

                        new_font = np.min([new_scaling_font_horizontal, new_scaling_font_vertical])
                        scaling_font = np.max([np.max(max_unbroken_newline) * float(self.true_n_y_ax), n_newlines * float(self.true_n_x_ax)])
                        self.data_plot_obj_list[rep_indx].legend_font_size[lg] = new_font

            else:
                for rep_indx in range(self.max_index_repeated):
                    self.data_plot_obj_list[rep_indx].legend_font_size = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)), None)
                    for set in range(len(self.set_num_list)):
                        for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):
                            if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot']:
                                index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique ==
                                                 self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                                index = index[0][0]  # For list only on
                                spec_h_use.list_array_ax_label[index]['label'].append(
                                    spec_h_use.dict_kwargs_array[set][i]['label'])
        else:
            for rep_indx in range(self.max_index_repeated):
                self.data_plot_obj_list[rep_indx].legend_font_size = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)), None)

        # set the absolute maximum legend size
        for rep_indx in range(self.max_index_repeated):
            if self.data_plot_obj_list[rep_indx].at_least_1_label:
                for lg in range(len(spec_h_use.list_array_ax_label)):
                    if self.data_plot_obj_list[rep_indx].legend_font_size[lg] > self.max_legend_font_size:
                        self.data_plot_obj_list[rep_indx].legend_font_size[lg] = self.max_legend_font_size
                for lg in range(len(spec_h_use.list_array_ax_label)):
                    for gen_index in range(len(spec_h_use.list_array_ax_label[lg]['label'])):
                        set = spec_h_use.list_array_ax_label[lg]['set'][gen_index]
                        i = spec_h_use.list_array_ax_label[lg]['i'][gen_index]
                        legend_label = spec_h_use.list_array_ax_label[lg]['label'][gen_index]
                        if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot']:
                            spec_h_use.dict_kwargs_array[set][i]['label'] = UsefulStringMethods.surround_2_string(legend_label, start=r'\textbf{', end='}')
    
    def colorbar_generation(self, rep_indx):
        pass

    def index_data_gen(self):
        ###################
        # Making link from ax to sets and i
        self.max_set_index_link_list = np.full(np.shape(self.ax_index_list_unique_max), np.nan)
        for rep_indx in range(self.max_index_repeated):
            for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
                self.max_set_index_link_list[ax_n] = np.nanmax([self.max_set_index_link_list[ax_n], len(self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list[ax_n][0])])
        ###################
        # find the max index in each set
        self.max_set_length = np.full(np.shape(self.set_num_list), np.nan)
        for rep_indx in range(self.max_index_repeated):
            for set in range(len(self.set_num_list)):
                i_max = len(self.data_plot_obj_list[rep_indx].ax_index_list[set])
                self.max_set_length[set] = np.nanmax([self.max_set_length[set], i_max])
            ###################

        for rep_indx in range(self.max_index_repeated):
            self.data_plot_obj_list[rep_indx].set_has_data_in = np.full(len(self.set_num_list), False)
            for set in range(len(self.set_num_list)):
                x_true = False
                y_true = False
                for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot']:
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None:
                            if len(self.data_plot_obj_list[rep_indx].x_data[set][i]) > 0:
                                x_true = True
                        if self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                            if len(self.data_plot_obj_list[rep_indx].y_data[set][i]) > 0:
                                y_true = True
                        if x_true and y_true:
                            self.data_plot_obj_list[rep_indx].set_has_data_in[set] = True

        if self.index_stack_direction == 'stack':

            ax_used_twice_overw = np.full(np.shape(self.ax_index_list_unique_max), np.nan)
            for rep_indx in range(self.max_index_repeated):
                for set in range(len(self.set_num_list)):
                    for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):

                        index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique ==
                                         self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                        if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot']:
                            current_ax_used_twice = self.data_plot_obj_list[rep_indx].ax_used_twice[index]
                            if np.isnan(ax_used_twice_overw):
                                if np.isnan(current_ax_used_twice):
                                    pass
                                elif not current_ax_used_twice:
                                    ax_used_twice_overw[index] = False
                                else:
                                    ax_used_twice_overw[index] = True
                            elif not ax_used_twice_overw:
                                if np.isnan(current_ax_used_twice):
                                    pass
                                elif not current_ax_used_twice:
                                    ax_used_twice_overw[index] = True
                                else:
                                    ax_used_twice_overw[index] = True
                            else:
                                ax_used_twice_overw[index] = True

            for ax in range(len(ax_used_twice_overw)):
                for rep_indx in range(self.max_index_repeated):
                    self.data_plot_obj_list[rep_indx].ax_used_twice[ax] = ax_used_twice_overw[ax]
        self.tester.run_test('index_data_gen', {'object':self, 'prog_index_list_unfixed':[rep_indx]})
    def index_data(self, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]

        self.data_plot_obj_list[rep_indx].index_list = np.empty((len(self.set_num_list)), dtype=list)
        for set in range(len(self.set_num_list)):
            self.data_plot_obj_list[rep_indx].index_list[set] = np.zeros((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list[set])[0], 2))
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                index = self.data_plot_obj_list[rep_indx].ax_index_list[set][i]
                index_ax_sublist = self.get_axes_index(index, self.n_y_ax, fixed_num_cols=self.n_x_ax)
                self.data_plot_obj_list[rep_indx].index_list[set][i, :] = index_ax_sublist
            self.data_plot_obj_list[rep_indx].index_list[set] = np.array(self.data_plot_obj_list[rep_indx].index_list[set], dtype=int)

        ##################
        self.data_plot_obj_list[rep_indx].ax_used_twice = np.full(np.shape(self.ax_index_list_unique_max), np.nan)
        spec_h_use.set_link_unique = np.full(np.shape(self.ax_index_list_unique_max), None)
        spec_h_use.index_link_unique = np.full(np.shape(self.ax_index_list_unique_max), None)

        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (
                        not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                    index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot']:
                        if np.isnan(self.data_plot_obj_list[rep_indx].ax_used_twice[index]):
                            self.data_plot_obj_list[rep_indx].ax_used_twice[index] = False
                            spec_h_use.set_link_unique[index] = set
                            spec_h_use.index_link_unique[index] = i

                        else:
                            self.data_plot_obj_list[rep_indx].ax_used_twice[index] = True
                            spec_h_use.set_link_unique[index] = set
                            spec_h_use.index_link_unique[index] = i
        self.data_plot_obj_list[rep_indx].ax_used_twice = np.array(self.data_plot_obj_list[rep_indx].ax_used_twice, dtype=bool)

#############################

        # Making link from ax to sets and i
        self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list = [[[], [], []] for _ in range(np.shape(self.ax_index_list_unique_max)[0])]

        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                for j in index[0]:
                    self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list[j][0].append(self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                    self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list[j][1].append(set)
                    self.data_plot_obj_list[rep_indx].ax_to_set_index_link_list[j][2].append(i)

        ############################################################
        self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy = np.zeros((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], 2))
        for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
            self.data_plot_obj_list[rep_indx].ax_index_list_unique_xy[ax_n, :] = self.get_axes_index(self.data_plot_obj_list[rep_indx].ax_index_list_unique[ax_n], self.n_y_ax, fixed_num_cols=self.n_x_ax)

    def reformat_data(self, rep_indx):
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == '2d_colour_plot':
                    x, y = np.meshgrid(self.data_plot_obj_list[rep_indx].x_data[set][i],
                                       self.data_plot_obj_list[rep_indx].y_data[set][
                                           i])  # makes dimension (y, x) so switch z below

                    if self.type_x_plot[set][i] == 'time':
                        self.x_data_set_type[set][i][0] = 'time_type'
                        if self.data_plot_obj_list[rep_indx].x_data[set][i].ndim > 1:
                            self.x_data_set_type[set][i][1] = 'time_type'
                        else:
                            self.x_data_set_type[set][i][1] = None
                    if self.type_y_plot[set][i] == 'time':
                        self.y_data_set_type[set][i][0] = 'time_type'
                        if self.data_plot_obj_list[rep_indx].y_data[set][i].ndim > 1:
                            self.y_data_set_type[set][i][1] = 'time_type'
                        else:
                            self.y_data_set_type[set][i][1] = None
                    if self.type_z_plot[set][i] == 'time':
                        self.z_data_set_type[set][i][0] = 'time_type'
                        if self.data_plot_obj_list[rep_indx].z_data[set][i].ndim > 1:
                            self.z_data_set_type[set][i][1] = 'time_type'
                        else:
                            self.z_data_set_type[set][i][1] = None
                    self.data_plot_obj_list[rep_indx].x_data[set][i] = x
                    self.data_plot_obj_list[rep_indx].y_data[set][i] = y
                    if self.z_data_set_type[set][i][0] == self.x_data_set_type[set][i][0] and \
                            self.z_data_set_type[set][i][1] == self.y_data_set_type[set][i][0]:
                        z = self.data_plot_obj_list[rep_indx].z_data[set][i].T
                    elif self.z_data_set_type[set][i][1] == self.x_data_set_type[set][i][0] and \
                            self.z_data_set_type[set][i][0] == self.y_data_set_type[set][i][0]:
                        z = self.data_plot_obj_list[rep_indx].z_data[set][i]
                    else:
                        raise ValueError("cant match the set type")
                    self.data_plot_obj_list[rep_indx].z_data[set][i] = z

    def calculate_where_ranges(self, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]

        # Calculate axis limits if ignore_for_ax_range_setting is True in the axis
        self.data_plot_obj_list[rep_indx].ax_resize_x = np.full(np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique), False)
        self.data_plot_obj_list[rep_indx].ax_resize_y = np.full(np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique), False)
        self.data_plot_obj_list[rep_indx].ax_resize_z = np.full(np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique), False)
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                self.tester.run_test('calculate_where_ranges', {'object': self, 'prog_index_list_unfixed': [rep_indx, set, i]})
                index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                if self.data_plot_obj_list[rep_indx].index_link[set][i][0] is not None:
                    spec_h_use.dict_x_settings_array[set][i]['not_for_min_max'] = self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]].ignore_for_ax_range_setting[self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
                    if spec_h_use.dict_x_settings_array[set][i]['not_for_min_max']:
                        self.data_plot_obj_list[rep_indx].ax_resize_x[index] = True
                else:
                    spec_h_use.dict_x_settings_array[set][i]['not_for_min_max'] = False
                if self.data_plot_obj_list[rep_indx].index_link[set][i][1] is not None:
                    spec_h_use.dict_y_settings_array[set][i]['not_for_min_max'] = self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]].ignore_for_ax_range_setting[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                    if spec_h_use.dict_y_settings_array[set][i]['not_for_min_max']:
                        self.data_plot_obj_list[rep_indx].ax_resize_y[index] = True
                else:
                    spec_h_use.dict_y_settings_array[set][i]['not_for_min_max'] = False
                if self.data_plot_obj_list[rep_indx].index_link[set][i][2] is not None:
                    spec_h_use.dict_z_settings_array[set][i]['not_for_min_max'] = self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]].ignore_for_ax_range_setting[self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
                    if spec_h_use.dict_z_settings_array[set][i]['not_for_min_max']:
                        self.data_plot_obj_list[rep_indx].ax_resize_z[index] = True
                else:
                    spec_h_use.dict_z_settings_array[set][i]['not_for_min_max'] = False

        # read in individually set ranges
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                if self.data_plot_obj_list[rep_indx].index_link[set][i][0] is not None:
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds'] = self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]].min_max_value_list[self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_type'] = self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]].min_max_bool_type_list[self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_data_type'] = self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]].min_max_data_type_list[self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
                else:
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds'] = [[None, None]]
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_type'] = [None]
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_data_type'] = [None]
                if self.data_plot_obj_list[rep_indx].index_link[set][i][1] is not None:
                    spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds'] = self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]].min_max_value_list[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                    spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_type'] = self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]].min_max_bool_type_list[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                    spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_data_type'] = self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]].min_max_data_type_list[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                else:
                    spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds'] = [[None, None]]
                    spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_type'] = [None]
                    spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_data_type'] = [None]
                if self.data_plot_obj_list[rep_indx].index_link[set][i][2] is not None:
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds'] = self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]].min_max_value_list[self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_type'] = self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]].min_max_bool_type_list[self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_data_type'] = self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]].min_max_data_type_list[self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
                else:
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds'] = [[None, None]]
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_type'] = [None]
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_data_type'] = [None]

        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                spec_h_use.dict_x_settings_array[set][i]['boolean_where_logic'] = []
                spec_h_use.dict_y_settings_array[set][i]['boolean_where_logic'] = []
                spec_h_use.dict_z_settings_array[set][i]['boolean_where_logic'] = []
                if spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds'] is not None:
                    if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None:
                        use_before = False
                        use_aft = False
                        for k in range(len(spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds'])):  # loop over all the booleans considered
                            if spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds'][k][0] is not None:
                                if spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_data_type'][k] is not None:
                                    if self.type_x_plot[set][i] == spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_data_type'][k]:
                                        data_to_comp_x = self.data_plot_obj_list[rep_indx].x_data[set][i]
                                    else:
                                        data_to_comp_x = getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_data_type'][k])
                                        # data_to_comp_x = data_to_comp_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
                                else:
                                    data_to_comp_x = self.data_plot_obj_list[rep_indx].x_data[set][i]
                                print(np.shape(data_to_comp_x),np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]),
                                     spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_data_type'])
                                bool_before = spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds'][k][0]<=data_to_comp_x
                                use_before = True
                            if spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds'][k][1] is not None:
                                if spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_data_type'][k] is not None:
                                    if self.type_x_plot[set][i] == spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_data_type'][k]:
                                        data_to_comp_x = self.data_plot_obj_list[rep_indx].x_data[set][i]
                                    else:
                                        data_to_comp_x = getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_data_type'][k])
                                        # data_to_comp_x = data_to_comp_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
                                else:
                                    data_to_comp_x = self.data_plot_obj_list[rep_indx].x_data[set][i]
                                print(np.shape(data_to_comp_x),np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]),
                                     spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_data_type'])
                                bool_after = spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds'][k][1]>=data_to_comp_x
                                use_aft = True
                            if use_before and use_aft:
                                bool_tot = np.logical_and(bool_before, bool_after)
                            elif use_before:
                                bool_tot = bool_before
                            elif use_aft:
                                bool_tot = bool_after
                            else:
                                bool_tot = None
                            spec_h_use.dict_x_settings_array[set][i]['boolean_where_logic'].append(bool_tot)
                    else:
                        spec_h_use.dict_x_settings_array[set][i]['boolean_where_logic'].append(None)
                else:
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_logic'].append(None)
                if spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds'] is not None:
                    if self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                        use_before = False
                        use_aft = False
                        for k in range(len(spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds'])):  # loop over all the booleans considered
                            if spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds'][k][0] is not None:
                                if spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_data_type'][k] is not None:
                                    if self.type_y_plot[set][i] == spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_data_type'][k]:
                                        data_to_comp_y = self.data_plot_obj_list[rep_indx].y_data[set][i]
                                    else:
                                        data_to_comp_y = getattr(self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]], spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_data_type'][k])
                                        # data_to_comp_y = data_to_comp_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                                else:
                                    data_to_comp_y = self.data_plot_obj_list[rep_indx].y_data[set][i]
                                print(np.shape(data_to_comp_y),np.shape(self.data_plot_obj_list[rep_indx].y_data[set][i]),
                                     spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_data_type'])
                                bool_before = spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds'][k][0] <= data_to_comp_y
                                use_before = True
                            if spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds'][k][1] is not None:
                                if spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_data_type'][k] is not None:
                                    if self.type_y_plot[set][i] == spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_data_type'][k]:
                                        data_to_comp_y = self.data_plot_obj_list[rep_indx].y_data[set][i]
                                    else:
                                        data_to_comp_y = getattr(self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]], spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_data_type'][k])
                                        # data_to_comp_y = data_to_comp_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                                else:
                                    data_to_comp_y = self.data_plot_obj_list[rep_indx].y_data[set][i]
                                print(np.shape(data_to_comp_y),np.shape(self.data_plot_obj_list[rep_indx].y_data[set][i]),
                                     spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_data_type'])
                                bool_after = spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds'][k][1] >= data_to_comp_y
                                use_aft = True
                            if use_before and use_aft:
                                bool_tot = np.logical_and(bool_before, bool_after)
                            elif use_before:
                                bool_tot = bool_before
                            elif use_aft:
                                bool_tot = bool_after
                            else:
                                bool_tot = None
                            spec_h_use.dict_y_settings_array[set][i]['boolean_where_logic'].append(bool_tot)
                    else:
                        spec_h_use.dict_y_settings_array[set][i]['boolean_where_logic'].append(None)
                else:
                    spec_h_use.dict_y_settings_array[set][i]['boolean_where_logic'].append(None)
                if spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds'] is not None:
                    if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                        use_before = False
                        use_aft = False
                        for k in range(len(spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds'])):  # loop over all the booleans considered
                            if spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds'][k][0] is not None:
                                if spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_data_type'][k] is not None:
                                    if self.type_z_plot[set][i] == spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_data_type'][k]:
                                        data_to_comp_z = self.data_plot_obj_list[rep_indx].z_data[set][i]
                                    else:
                                        data_to_comp_z = getattr(self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]], spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_data_type'][k])
                                        # data_to_comp_z = data_to_comp_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
                                else:
                                    data_to_comp_z = self.data_plot_obj_list[rep_indx].z_data[set][i]
                                bool_before = spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds'][k][0] <= data_to_comp_z
                                use_before = True
                            if spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds'][k][1] is not None:
                                if spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_data_type'][k] is not None:
                                    if self.type_z_plot[set][i] == spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_data_type'][k]:
                                        data_to_comp_z = self.data_plot_obj_list[rep_indx].z_data[set][i]
                                    else:
                                        data_to_comp_z = getattr(self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]], spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_data_type'][k])
                                        # data_to_comp_z = data_to_comp_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
                                else:
                                    data_to_comp_z = self.data_plot_obj_list[rep_indx].z_data[set][i]
                                bool_after = spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds'][k][1] >= data_to_comp_z
                                use_aft = True
                            if use_before and use_aft:
                                bool_tot = np.logical_and(bool_before, bool_after)
                            elif use_before:
                                bool_tot = bool_before
                            elif use_aft:
                                bool_tot = bool_after
                            else:
                                bool_tot = None
                            spec_h_use.dict_z_settings_array[set][i]['boolean_where_logic'].append(bool_tot)
                    else:
                        spec_h_use.dict_z_settings_array[set][i]['boolean_where_logic'].append(None)
                else:
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_logic'].append(None)

        # Initialise
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_or'] = None
                spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_and'] = None
                spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_or'] = None
                spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_and'] = None
                spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_or'] = None
                spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_and'] = None

        # Make boolean_where_bounds all the same
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None:

                    total_bool_and = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), True)
                    total_bool_or = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), False)
                    or_used = False
                    for k in range(len(spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds'])):
                        if spec_h_use.dict_x_settings_array[set][i]['boolean_where_logic'][k] is not None:
                            if spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_type'][k] == 'and':
                                total_bool_and = np.logical_and(total_bool_and, spec_h_use.dict_x_settings_array[set][i]['boolean_where_logic'][k])
                            if spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_type'][k] == 'or':
                                total_bool_or = np.logical_or(total_bool_or, spec_h_use.dict_x_settings_array[set][i]['boolean_where_logic'][k])
                                or_used = True
                    if or_used:
                        spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_or'] = total_bool_or
                    else:
                        total_bool_or = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i], ), True)  # [0] is the first dimension
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_or'] = total_bool_or
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_and'] = total_bool_and

                if self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                    total_bool_and = np.full(np.shape(self.data_plot_obj_list[rep_indx].y_data[set][i]), True)
                    total_bool_or = np.full(np.shape(self.data_plot_obj_list[rep_indx].y_data[set][i]), False)
                    or_used = False
                    for k in range(len(spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds'])):
                        if spec_h_use.dict_y_settings_array[set][i]['boolean_where_logic'][k] is not None:
                            if spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_type'][k] == 'and':
                                total_bool_and = np.logical_and(total_bool_and, spec_h_use.dict_y_settings_array[set][i]['boolean_where_logic'][k])
                            if spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_type'][k] == 'or':
                                total_bool_or = np.logical_or(total_bool_or, spec_h_use.dict_y_settings_array[set][i]['boolean_where_logic'][k])
                                or_used = True
                    if or_used:
                        spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_or'] = total_bool_or
                    else:
                        total_bool_or = np.full(np.shape(self.data_plot_obj_list[rep_indx].y_data[set][i]), True)
                    spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_and'] = total_bool_and

                if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                    total_bool_and = np.full(np.shape(self.data_plot_obj_list[rep_indx].z_data[set][i]), True)
                    total_bool_or = np.full(np.shape(self.data_plot_obj_list[rep_indx].z_data[set][i]), False)
                    or_used = False
                    for k in range(len(spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds'])):
                        if spec_h_use.dict_z_settings_array[set][i]['boolean_where_logic'][k] is not None:
                            if spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_type'][k] == 'and':
                                total_bool_and = np.logical_and(total_bool_and, spec_h_use.dict_z_settings_array[set][i]['boolean_where_logic'][k])
                            if spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_type'][k] == 'or':
                                total_bool_or = np.logical_or(total_bool_or, spec_h_use.dict_z_settings_array[set][i]['boolean_where_logic'][k])
                                or_used = True
                    if or_used:
                        spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_or'] = total_bool_or
                    else:
                        total_bool_or = np.full(np.shape(self.data_plot_obj_list[rep_indx].z_data[set][i]), True)
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_or'] = total_bool_or
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_and'] = total_bool_and

        # Combine the booleans to one
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):

                bool_xyz_or = [
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_or'],
                    spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_or'],
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_or']
                ]
                bool_xyz_not_none_or = [i for i in range(len(bool_xyz_or)) if bool_xyz_or[i] is not None]
                total_bool_or = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), True)

                second_dim = None
                total_bool_or_max_dim = None
                for bool_index_or in bool_xyz_not_none_or:
                    total_bool_or = np.logical_and(total_bool_or, bool_xyz_or[bool_index_or])
                    if total_bool_or_max_dim is not None:
                        if bool_xyz_or[bool_index_or].ndim > 1:
                            total_bool_or_max_dim = np.logical_and(total_bool_or, bool_xyz_or[bool_index_or])
                        else:
                            total_bool_or_max_dim = np.logical_and(total_bool_or[:, np.newaxis], bool_xyz_or[bool_index_or])
                    elif bool_xyz_or[bool_index_or].ndim > 1:
                        total_bool_or_max_dim = np.logical_and(total_bool_or[:, np.newaxis], bool_xyz_or[bool_index_or])

                    # total_bool_or = np.logical_and(total_bool_or, bool_xyz_or[bool_index_or])

                bool_xyz_and = [
                    spec_h_use.dict_x_settings_array[set][i]['boolean_where_bounds_and'],
                    spec_h_use.dict_y_settings_array[set][i]['boolean_where_bounds_and'],
                    spec_h_use.dict_z_settings_array[set][i]['boolean_where_bounds_and']
                ]
                bool_xyz_not_none_and = [i for i in range(len(bool_xyz_and)) if bool_xyz_and[i] is not None]
                total_bool_and = np.full(np.shape(self.data_plot_obj_list[rep_indx].x_data[set][i]), True)
                total_bool_and_max_dim = None
                if not self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'axspan_plot':
                    for bool_index_and in bool_xyz_not_none_and:
                        # if bool_xyz_and[bool_index_and].ndim > 1:
                        #     total_bool_and = np.logical_and(total_bool_and, np.all(bool_xyz_and[bool_index_and], axis=1))
                        # else:
                        total_bool_and = np.logical_and(total_bool_and, bool_xyz_and[bool_index_and])
                        if total_bool_and_max_dim is not None:
                            if bool_xyz_and[bool_index_and].ndim > 1:
                                total_bool_and_max_dim = np.logical_and(total_bool_and, bool_xyz_and[bool_index_and])
                            else:
                                total_bool_and_max_dim = np.logical_and(total_bool_and[:, np.newaxis], bool_xyz_and[bool_index_and])
                        elif bool_xyz_and[bool_index_and].ndim > 1:
                            total_bool_and_max_dim = np.logical_and(total_bool_and[:, np.newaxis], bool_xyz_and[bool_index_and])

                        # total_bool_and = np.logical_and(total_bool_and, bool_xyz_and[bool_index_and])

                    total_bool = np.logical_and(total_bool_and, total_bool_or)
                    if total_bool_and_max_dim is not None and total_bool_or_max_dim is not None:
                        total_bool_max_dim = np.logical_and(total_bool_and_max_dim, total_bool_or_max_dim)
                    elif total_bool_and_max_dim is not None:
                        total_bool_max_dim = np.logical_and(total_bool_and_max_dim, total_bool_or[:, np.newaxis])
                    elif total_bool_or_max_dim is not None:
                        total_bool_max_dim = np.logical_and(total_bool_and[:, np.newaxis], total_bool_or_max_dim)
                    else:
                        total_bool_max_dim = None
                    spec_h_use.bool_full[set][i] = total_bool  # np.logical_and(spec_h_use.bool_full[set][i], bool_xyz[bool_index])
                    spec_h_use.bool_full_max_dim[set][i] = total_bool_max_dim  # np.logical_and(spec_h_use.bool_full[set][i], bool_xyz[bool_index])

    def set_where_ranges(self, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]

        # Act on xyzdata
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):

                    if spec_h_use.bool_full_max_dim[set][i] is not None:
                        where_this_loop_max_dim = np.where(spec_h_use.bool_full_max_dim[set][i])
                    else:
                        where_this_loop_max_dim = None
                    where_this_loop = np.where(spec_h_use.bool_full[set][i])
                    if not self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'axspan_plot':
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and not isinstance(self.data_plot_obj_list[rep_indx].x_data[set][i], str):
                            if where_this_loop_max_dim is not None:
                                if self.data_plot_obj_list[rep_indx].x_data[set][i].ndim > 1:
                                    x = self.data_plot_obj_list[rep_indx].x_data[set][i]
                                    x_new = np.full(np.shape(x), np.nan)
                                    x_new[where_this_loop_max_dim] = self.data_plot_obj_list[rep_indx].x_data[set][i][where_this_loop_max_dim]
                                    rows_not_all_nan = np.where(np.logical_not(np.all(np.isnan(x_new), axis=1)))[0]
                                    cols_not_all_nan = np.where(np.logical_not(np.all(np.isnan(x_new), axis=0)))[0]

                                    x_new = x_new[rows_not_all_nan, :]
                                    x_new = x_new[:, cols_not_all_nan]
                                    self.data_plot_obj_list[rep_indx].x_data[set][i] = x_new
                                else:
                                    self.data_plot_obj_list[rep_indx].x_data[set][i] = self.data_plot_obj_list[rep_indx].x_data[set][i][where_this_loop]
                                    if self.data_plot_obj_list[rep_indx].x_err[set][i] is not None:
                                        self.data_plot_obj_list[rep_indx].x_err[set][i] = \
                                            self.data_plot_obj_list[rep_indx].x_err[set][i][where_this_loop]
                            else:
                                if where_this_loop is not None:
                                    print(self.data_plot_obj_list[rep_indx].x_data[set][i], set, i, type(self.data_plot_obj_list[rep_indx].x_data[set][i]) == tuple)
                                    print(where_this_loop, self.data_plot_obj_list[rep_indx].type_plot_ax[set][i])
                                    print("fdsdfsdf", rep_indx, set, i, use_twin_ax)
                                    self.data_plot_obj_list[rep_indx].x_data[set][i] = self.data_plot_obj_list[rep_indx].x_data[set][i][where_this_loop]
                                    if self.data_plot_obj_list[rep_indx].x_err[set][i] is not None:
                                        self.data_plot_obj_list[rep_indx].x_err[set][i] = self.data_plot_obj_list[rep_indx].x_err[set][i][where_this_loop]
                        if self.data_plot_obj_list[rep_indx].y_data[set][i] is not None and not isinstance(self.data_plot_obj_list[rep_indx].y_data[set][i], str):
                            print(self.plot_key, where_this_loop_max_dim, self.data_plot_obj_list[rep_indx].y_data[set][i], set, i, np.isnan(self.data_plot_obj_list[rep_indx].y_data[set][i]))
                            if where_this_loop_max_dim is not None:
                                if self.data_plot_obj_list[rep_indx].y_data[set][i].ndim > 1:
                                    y = self.data_plot_obj_list[rep_indx].y_data[set][i]
                                    y_new = np.full(np.shape(y), np.nan)
                                    y_new[where_this_loop_max_dim] = self.data_plot_obj_list[rep_indx].y_data[set][i][where_this_loop_max_dim]
                                    rows_not_all_nan = np.where(np.logical_not(np.all(np.isnan(y_new), axis=1)))[0]
                                    cols_not_all_nan = np.where(np.logical_not(np.all(np.isnan(y_new), axis=0)))[0]

                                    y_new = y_new[rows_not_all_nan, :]
                                    y_new = y_new[:, cols_not_all_nan]
                                    self.data_plot_obj_list[rep_indx].y_data[set][i] = y_new
                                else:
                                    if where_this_loop is not None:
                                        self.data_plot_obj_list[rep_indx].y_data[set][i] = \
                                        self.data_plot_obj_list[rep_indx].y_data[set][i][where_this_loop]
                                        if self.data_plot_obj_list[rep_indx].y_err[set][i] is not None:
                                            self.data_plot_obj_list[rep_indx].y_err[set][i] = \
                                            self.data_plot_obj_list[rep_indx].y_err[set][i][where_this_loop]
                            else:
                                if where_this_loop is not None:
                                    self.data_plot_obj_list[rep_indx].y_data[set][i] = self.data_plot_obj_list[rep_indx].y_data[set][i][where_this_loop]
                                    if self.data_plot_obj_list[rep_indx].y_err[set][i] is not None:
                                        self.data_plot_obj_list[rep_indx].y_err[set][i] = self.data_plot_obj_list[rep_indx].y_err[set][i][where_this_loop]

                        if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None and not isinstance(self.data_plot_obj_list[rep_indx].z_data[set][i], str):
                            if where_this_loop_max_dim is not None:
                                if self.data_plot_obj_list[rep_indx].z_data[set][i].ndim > 1:
                                    z = self.data_plot_obj_list[rep_indx].z_data[set][i]
                                    z_new = np.full(np.shape(z), np.nan)
                                    z_new[where_this_loop_max_dim] = self.data_plot_obj_list[rep_indx].z_data[set][i][where_this_loop_max_dim]
                                    rows_not_all_nan = np.where(np.logical_not(np.all(np.isnan(z_new), axis=1)))[0]
                                    cols_not_all_nan = np.where(np.logical_not(np.all(np.isnan(z_new), axis=0)))[0]

                                    z_new = z_new[rows_not_all_nan, :]
                                    z_new = z_new[:, cols_not_all_nan]
                                    self.data_plot_obj_list[rep_indx].z_data[set][i] = z_new

                                else:
                                    self.data_plot_obj_list[rep_indx].z_data[set][i] = self.data_plot_obj_list[rep_indx].z_data[set][i][where_this_loop]
                                    if self.data_plot_obj_list[rep_indx].z_err[set][i] is not None:
                                        self.data_plot_obj_list[rep_indx].z_err[set][i] = self.data_plot_obj_list[rep_indx].z_err[set][i][where_this_loop]
                            else:
                                self.data_plot_obj_list[rep_indx].z_data[set][i] = self.data_plot_obj_list[rep_indx].z_data[set][i][where_this_loop]
                                if self.data_plot_obj_list[rep_indx].z_err[set][i] is not None:
                                    self.data_plot_obj_list[rep_indx].z_err[set][i] = self.data_plot_obj_list[rep_indx].z_err[set][i][where_this_loop]

    def find_min_max(self, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]

        spec_h_use.ax_max_min_x_unique = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], 2), np.nan)
        spec_h_use.ax_max_min_y_unique = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], 2), np.nan)
        spec_h_use.ax_max_min_z_unique = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], 2), np.nan)
        self.data_plot_obj_list[rep_indx].x_ax_unique_usable = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0]), False)# Not all empty or string
        self.data_plot_obj_list[rep_indx].y_ax_unique_usable = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0]), False)# Not all empty or string
        self.data_plot_obj_list[rep_indx].z_ax_unique_usable = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0]), False)# Not all empty or string
        # store the min and max per axis
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                if self.data_plot_obj_list[rep_indx].set_has_data_in[set]:
                    nanmin_new_x = np.nanmin(self.data_plot_obj_list[rep_indx].x_data[set][i])
                    nanmax_new_x = np.nanmax(self.data_plot_obj_list[rep_indx].x_data[set][i])

                if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and not isinstance(self.data_plot_obj_list[rep_indx].x_data[set][i], str) and self.data_plot_obj_list[rep_indx].set_has_data_in[set]:
                    self.data_plot_obj_list[rep_indx].x_ax_unique_usable[index] = True
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot'] and not spec_h_use.dict_x_settings_array[set][i]['not_for_min_max']:

                        if not np.isnan(spec_h_use.ax_max_min_x_unique[index, 0]):
                            if spec_h_use.ax_max_min_x_unique[index, 0] > nanmin_new_x:
                                spec_h_use.ax_max_min_x_unique[index, 0] = nanmin_new_x
                        else:
                            spec_h_use.ax_max_min_x_unique[index, 0] = nanmin_new_x
                        if not np.isnan(spec_h_use.ax_max_min_x_unique[index, 1]):
                            if spec_h_use.ax_max_min_x_unique[index, 1] < nanmax_new_x:
                                spec_h_use.ax_max_min_x_unique[index, 1] = nanmax_new_x
                        else:
                            spec_h_use.ax_max_min_x_unique[index, 1] = nanmax_new_x
                if self.data_plot_obj_list[rep_indx].set_has_data_in[set]:
                    nanmin_new_y = np.nanmin(self.data_plot_obj_list[rep_indx].y_data[set][i])
                    nanmax_new_y = np.nanmax(self.data_plot_obj_list[rep_indx].y_data[set][i])
                if self.data_plot_obj_list[rep_indx].y_data[set][i] is not None and not isinstance(self.data_plot_obj_list[rep_indx].y_data[set][i], str):
                    self.data_plot_obj_list[rep_indx].y_ax_unique_usable[index] = True
                    self.tester.run_test('find_min_max', {'object': self, 'prog_index_list_unfixed': [rep_indx, set, i]})

                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot'] and not spec_h_use.dict_y_settings_array[set][i]['not_for_min_max']:
                        if not np.isnan(spec_h_use.ax_max_min_y_unique[index, 0]):
                            if spec_h_use.ax_max_min_y_unique[index, 0] > nanmin_new_y:
                                spec_h_use.ax_max_min_y_unique[index, 0] = nanmin_new_y
                        else:
                            spec_h_use.ax_max_min_y_unique[index, 0] = nanmin_new_y
                        if not np.isnan(spec_h_use.ax_max_min_y_unique[index, 1]):
                            if spec_h_use.ax_max_min_y_unique[index, 1] < nanmax_new_y:
                                spec_h_use.ax_max_min_y_unique[index, 1] = nanmax_new_y
                        else:
                            spec_h_use.ax_max_min_y_unique[index, 1] = nanmax_new_y
                if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None and not isinstance(self.data_plot_obj_list[rep_indx].z_data[set][i], str):
                    self.data_plot_obj_list[rep_indx].z_ax_unique_usable[index] = True
                    nanmin_new_z = np.nanmin(self.data_plot_obj_list[rep_indx].z_data[set][i])
                    nanmax_new_z = np.nanmax(self.data_plot_obj_list[rep_indx].z_data[set][i])
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['important_plot'] and not spec_h_use.dict_z_settings_array[set][i]['not_for_min_max']:
                        if not np.isnan(spec_h_use.ax_max_min_z_unique[index, 0]):
                            if spec_h_use.ax_max_min_z_unique[index, 0] > nanmin_new_z:
                                spec_h_use.ax_max_min_z_unique[index, 0] = nanmin_new_z
                        else:
                            spec_h_use.ax_max_min_z_unique[index, 0] = nanmin_new_z
                        if not np.isnan(spec_h_use.ax_max_min_z_unique[index, 1]):
                            if spec_h_use.ax_max_min_z_unique[index, 1] < nanmax_new_z:
                                spec_h_use.ax_max_min_z_unique[index, 1] = nanmax_new_z
                        else:
                            spec_h_use.ax_max_min_z_unique[index, 1] = nanmax_new_z
        spec_h_use.ax_max_min_x_unique_normalised = np.copy(spec_h_use.ax_max_min_x_unique)
        spec_h_use.ax_max_min_y_unique_normalised = np.copy(spec_h_use.ax_max_min_y_unique)
        spec_h_use.ax_max_min_z_unique_normalised = np.copy(spec_h_use.ax_max_min_z_unique)
        self.data_plot_obj_list[rep_indx].order_mag_x_data_unique = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], ), np.nan)
        self.data_plot_obj_list[rep_indx].order_mag_y_data_unique = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], ), np.nan)
        self.data_plot_obj_list[rep_indx].order_mag_z_data_unique = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0], ), np.nan)
        for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
            self.data_plot_obj_list[rep_indx].order_mag_x_data_unique[ax_n] = np.floor(np.log10(np.nanmax(np.abs(spec_h_use.ax_max_min_x_unique[ax_n,:]))))
            self.data_plot_obj_list[rep_indx].order_mag_y_data_unique[ax_n] = np.floor(np.log10(np.nanmax(np.abs(spec_h_use.ax_max_min_y_unique[ax_n,:]))))
            self.data_plot_obj_list[rep_indx].order_mag_z_data_unique[ax_n] = np.floor(np.log10(np.nanmax(np.abs(spec_h_use.ax_max_min_z_unique[ax_n,:]))))

        # Calculate potential si units
        self.data_plot_obj_list[rep_indx].si_string_prefix_x_unique = np.empty((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],), dtype=list)
        self.data_plot_obj_list[rep_indx].si_symbol_prefix_x_unique = np.empty((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],), dtype=list)
        self.data_plot_obj_list[rep_indx].si_base_x_unique = np.zeros((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],))
        self.data_plot_obj_list[rep_indx].si_string_prefix_y_unique = np.empty((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],), dtype=list)
        self.data_plot_obj_list[rep_indx].si_symbol_prefix_y_unique = np.empty((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],), dtype=list)
        self.data_plot_obj_list[rep_indx].si_base_y_unique = np.zeros((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],))
        self.data_plot_obj_list[rep_indx].si_string_prefix_z_unique = np.empty((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],), dtype=list)
        self.data_plot_obj_list[rep_indx].si_symbol_prefix_z_unique = np.empty((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],), dtype=list)
        self.data_plot_obj_list[rep_indx].si_base_z_unique = np.zeros((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],))
        for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
            if not np.isnan(self.data_plot_obj_list[rep_indx].order_mag_x_data_unique[ax_n]):
                si_string_prefix_x_unique, \
                si_symbol_prefix_x_unique, \
                si_base_x_unique = self.unit_converter.get_prefix_for_power_lower(
                    power=self.data_plot_obj_list[rep_indx].order_mag_x_data_unique[ax_n])
            else:
                si_string_prefix_x_unique = None
                si_symbol_prefix_x_unique = None
                si_base_x_unique = None
            if not np.isnan(self.data_plot_obj_list[rep_indx].order_mag_y_data_unique[ax_n]):
                si_string_prefix_y_unique, \
                si_symbol_prefix_y_unique, \
                si_base_y_unique = self.unit_converter.get_prefix_for_power_lower(
                    power=self.data_plot_obj_list[rep_indx].order_mag_y_data_unique[ax_n])
            else:
                si_string_prefix_y_unique = None
                si_symbol_prefix_y_unique = None
                si_base_y_unique = None
            if not np.isnan(self.data_plot_obj_list[rep_indx].order_mag_z_data_unique[ax_n]):
                si_string_prefix_z_unique, \
                si_symbol_prefix_z_unique, \
                si_base_z_unique = self.unit_converter.get_prefix_for_power_lower(
                    power=self.data_plot_obj_list[rep_indx].order_mag_z_data_unique[ax_n]
                )
            else:
                si_string_prefix_z_unique = None
                si_symbol_prefix_z_unique = None
                si_base_z_unique = None

            self.data_plot_obj_list[rep_indx].si_string_prefix_x_unique[ax_n] = [si_string_prefix_x_unique]
            self.data_plot_obj_list[rep_indx].si_symbol_prefix_x_unique[ax_n] = [si_symbol_prefix_x_unique]
            self.data_plot_obj_list[rep_indx].si_base_x_unique[ax_n] = si_base_x_unique
            self.data_plot_obj_list[rep_indx].si_string_prefix_y_unique[ax_n] = [si_string_prefix_y_unique]
            self.data_plot_obj_list[rep_indx].si_symbol_prefix_y_unique[ax_n] = [si_symbol_prefix_y_unique]
            self.data_plot_obj_list[rep_indx].si_base_y_unique[ax_n] = si_base_y_unique
            self.data_plot_obj_list[rep_indx].si_string_prefix_z_unique[ax_n] = [si_string_prefix_z_unique]
            self.data_plot_obj_list[rep_indx].si_symbol_prefix_z_unique[ax_n] = [si_symbol_prefix_z_unique]
            self.data_plot_obj_list[rep_indx].si_base_z_unique[ax_n] = si_base_z_unique
        # check if each axis will be changed
        if self.label_order_of_mag == 'all_with_si_units':
            for set in range(len(self.set_num_list)):
                for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                    spec_h_use.dict_x_settings_array[set][i]['use_si'] = True
                    spec_h_use.dict_y_settings_array[set][i]['use_si'] = True
                    spec_h_use.dict_z_settings_array[set][i]['use_si'] = True
        else:
            for set in range(len(self.set_num_list)):
                for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                    if self.data_plot_obj_list[rep_indx].index_link[set][i][0] is not None:
                        spec_h_use.dict_x_settings_array[set][i]['use_si'] = \
                            self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]].overwrite_use_si_units[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][0 + 3]]
                    else:
                        spec_h_use.dict_x_settings_array[set][i]['use_si'] = False
                    if self.data_plot_obj_list[rep_indx].index_link[set][i][1] is not None:
                        spec_h_use.dict_y_settings_array[set][i]['use_si'] = \
                            self.data_plot_obj_list[rep_indx].stored_y[self.data_plot_obj_list[rep_indx].index_link[set][i][1]].overwrite_use_si_units[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][1 + 3]]
                    else:
                        spec_h_use.dict_y_settings_array[set][i]['use_si'] = False
                    if self.data_plot_obj_list[rep_indx].index_link[set][i][2] is not None:
                        spec_h_use.dict_z_settings_array[set][i]['use_si'] = \
                            self.data_plot_obj_list[rep_indx].stored_z[self.data_plot_obj_list[rep_indx].index_link[set][i][2]].overwrite_use_si_units[
                                self.data_plot_obj_list[rep_indx].index_link[set][i][2 + 3]]
                    else:
                        spec_h_use.dict_z_settings_array[set][i]['use_si'] = False
        # covert to unique ax
        self.data_plot_obj_list[rep_indx].use_si_x_unique = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],), False)
        self.data_plot_obj_list[rep_indx].use_si_y_unique = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],), False)
        self.data_plot_obj_list[rep_indx].use_si_z_unique = np.full((np.shape(self.data_plot_obj_list[rep_indx].ax_index_list_unique)[0],), False)

        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                if not self.data_plot_obj_list[rep_indx].use_si_x_unique[index]:
                    if spec_h_use.dict_x_settings_array[set][i]['use_si']:
                        self.data_plot_obj_list[rep_indx].use_si_x_unique[index] = True
                if not self.data_plot_obj_list[rep_indx].use_si_y_unique[index]:
                    if spec_h_use.dict_y_settings_array[set][i]['use_si']:
                        self.data_plot_obj_list[rep_indx].use_si_y_unique[index] = True
                if not self.data_plot_obj_list[rep_indx].use_si_z_unique[index]:
                    if spec_h_use.dict_z_settings_array[set][i]['use_si']:
                        self.data_plot_obj_list[rep_indx].use_si_z_unique[index] = True

            # update the order_mag_data_unique parts if it is changed
            for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
                if self.data_plot_obj_list[rep_indx].use_si_x_unique[ax_n]:
                    self.data_plot_obj_list[rep_indx].order_mag_x_data_unique[ax_n] = \
                    self.data_plot_obj_list[rep_indx].si_base_x_unique[ax_n]
                if self.data_plot_obj_list[rep_indx].use_si_y_unique[ax_n]:
                    self.data_plot_obj_list[rep_indx].order_mag_y_data_unique[ax_n] = \
                    self.data_plot_obj_list[rep_indx].si_base_y_unique[ax_n]
                if self.data_plot_obj_list[rep_indx].use_si_z_unique[ax_n]:
                    self.data_plot_obj_list[rep_indx].order_mag_z_data_unique[ax_n] = \
                    self.data_plot_obj_list[rep_indx].si_base_z_unique[ax_n]

    def update_min_max_gen_sizing(self):
        # If the normalisation needs to be updated between pulses
        if self.index_stack_direction == 'stack' or self.share_ax_y:
            order_mag_x_data_unique = np.full(np.shape(self.ax_index_list_unique_max), np.nan)
            order_mag_y_data_unique = np.full(np.shape(self.ax_index_list_unique_max), np.nan)
            order_mag_z_data_unique = np.full(np.shape(self.ax_index_list_unique_max), np.nan)

            for rep_indx in range(self.max_index_repeated):
                # update the order_mag_data_unique parts if it is changed
                for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
                    update_order_mag_x_data_unique = [order_mag_x_data_unique[ax_n], self.data_plot_obj_list[rep_indx].order_mag_x_data_unique[ax_n]]
                    order_mag_x_data_unique[ax_n] = np.nanmax(update_order_mag_x_data_unique)
                    update_order_mag_y_data_unique = [order_mag_y_data_unique[ax_n], self.data_plot_obj_list[rep_indx].order_mag_y_data_unique[ax_n]]
                    order_mag_y_data_unique[ax_n] = np.nanmax(update_order_mag_y_data_unique)
                    update_order_mag_z_data_unique = [order_mag_z_data_unique[ax_n], self.data_plot_obj_list[rep_indx].order_mag_z_data_unique[ax_n]]
                    order_mag_z_data_unique[ax_n] = np.nanmax(update_order_mag_z_data_unique)
            for rep_indx in range(self.max_index_repeated):
                # update the order_mag_data_unique parts if it is changed
                for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
                    self.data_plot_obj_list[rep_indx].order_mag_x_data_unique[ax_n] = order_mag_x_data_unique[ax_n]
                    self.data_plot_obj_list[rep_indx].order_mag_y_data_unique[ax_n] = order_mag_y_data_unique[ax_n]
                    self.data_plot_obj_list[rep_indx].order_mag_z_data_unique[ax_n] = order_mag_z_data_unique[ax_n]

            ax_si_base_x_unique = np.full(np.shape(self.ax_index_list_unique_max), np.nan)
            ax_si_base_y_unique = np.full(np.shape(self.ax_index_list_unique_max), np.nan)
            ax_si_base_z_unique = np.full(np.shape(self.ax_index_list_unique_max), np.nan)
            ax_si_string_prefix_x_unique = np.empty(np.shape(self.ax_index_list_unique_max), dtype=list)
            ax_si_symbol_prefix_x_unique = np.empty(np.shape(self.ax_index_list_unique_max), dtype=list)
            ax_si_string_prefix_y_unique = np.empty(np.shape(self.ax_index_list_unique_max), dtype=list)
            ax_si_symbol_prefix_y_unique = np.empty(np.shape(self.ax_index_list_unique_max), dtype=list)
            ax_si_string_prefix_z_unique = np.empty(np.shape(self.ax_index_list_unique_max), dtype=list)
            ax_si_symbol_prefix_z_unique = np.empty(np.shape(self.ax_index_list_unique_max), dtype=list)
            for rep_indx in range(self.max_index_repeated):
                # update the order_mag_data_unique parts if it is changed
                for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
                    if self.data_plot_obj_list[rep_indx].use_si_x_unique[ax_n]:
                        if self.data_plot_obj_list[rep_indx].x_ax_unique_usable[ax_n]:
                            update_si_base_x = [ax_si_base_x_unique[ax_n], self.data_plot_obj_list[rep_indx].si_base_x_unique[ax_n]]
                            arg_updated_x = np.nanargmax(update_si_base_x)
                            ax_si_base_x_unique[ax_n] = update_si_base_x[arg_updated_x]
                            if arg_updated_x == 1:
                                ax_si_string_prefix_x_unique[ax_n] = self.data_plot_obj_list[rep_indx].si_string_prefix_x_unique[ax_n]
                                ax_si_symbol_prefix_x_unique[ax_n] = self.data_plot_obj_list[rep_indx].si_symbol_prefix_x_unique[ax_n]

                    if self.data_plot_obj_list[rep_indx].use_si_y_unique[ax_n]:
                        if self.data_plot_obj_list[rep_indx].y_ax_unique_usable[ax_n]:
                            update_si_base_y = [ax_si_base_y_unique[ax_n],
                                                self.data_plot_obj_list[rep_indx].si_base_y_unique[ax_n]]
                            arg_updated_y = np.nanargmax(update_si_base_y)
                            ax_si_base_y_unique[ax_n] = update_si_base_y[arg_updated_y]
                            if arg_updated_y == 1:
                                ax_si_string_prefix_y_unique[ax_n] = self.data_plot_obj_list[rep_indx].si_string_prefix_y_unique[ax_n]
                                ax_si_symbol_prefix_y_unique[ax_n] = self.data_plot_obj_list[rep_indx].si_symbol_prefix_y_unique[ax_n]
                    if self.data_plot_obj_list[rep_indx].use_si_z_unique[ax_n]:
                        if self.data_plot_obj_list[rep_indx].z_ax_unique_usable[ax_n]:
                            update_si_base_z = [ax_si_base_z_unique[ax_n], self.data_plot_obj_list[rep_indx].si_base_z_unique[ax_n]]
                            arg_updated_z = np.nanargmax(update_si_base_z)
                            ax_si_base_x_unique[ax_n] = update_si_base_x[arg_updated_z]
                            if arg_updated_z == 1:
                                ax_si_string_prefix_z_unique[ax_n] = self.data_plot_obj_list[rep_indx].si_string_prefix_z_unique[ax_n]
                                ax_si_symbol_prefix_z_unique[ax_n] = self.data_plot_obj_list[rep_indx].si_symbol_prefix_z_unique[ax_n]

            for rep_indx in range(self.max_index_repeated):
                # update the order_mag_data_unique parts if it is changed
                for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
                    if not np.isnan(ax_si_base_x_unique[ax_n]):
                        self.data_plot_obj_list[rep_indx].order_mag_x_data_unique[ax_n] = ax_si_base_x_unique[ax_n]
                        self.data_plot_obj_list[rep_indx].si_base_x_unique[ax_n] = ax_si_base_x_unique[ax_n]
                        self.data_plot_obj_list[rep_indx].si_string_prefix_x_unique[ax_n] = [ax_si_string_prefix_x_unique[ax_n][0]]#np.array([ax_si_string_prefix_x_unique[ax_n][0]])#np.array([list([ax_si_string_prefix_x_unique[ax_n][0]])])
                        self.data_plot_obj_list[rep_indx].si_symbol_prefix_x_unique[ax_n] = [ax_si_symbol_prefix_x_unique[ax_n][0]]#np.array([list([ax_si_symbol_prefix_x_unique[ax_n][0]])])
                    if not np.isnan(ax_si_base_y_unique[ax_n]):
                        self.data_plot_obj_list[rep_indx].order_mag_y_data_unique[ax_n] = ax_si_base_y_unique[ax_n]
                        self.data_plot_obj_list[rep_indx].si_base_y_unique[ax_n] = ax_si_base_y_unique[ax_n]
                        self.data_plot_obj_list[rep_indx].si_string_prefix_y_unique[ax_n] = [ax_si_string_prefix_y_unique[ax_n][0]]#np.array([list([ax_si_string_prefix_y_unique[ax_n][0]])])
                        self.data_plot_obj_list[rep_indx].si_symbol_prefix_y_unique[ax_n] = [ax_si_symbol_prefix_y_unique[ax_n][0]]
                    if not np.isnan(ax_si_base_z_unique[ax_n]):
                        self.data_plot_obj_list[rep_indx].order_mag_z_data_unique[ax_n] = ax_si_base_z_unique[ax_n]
                        self.data_plot_obj_list[rep_indx].si_base_z_unique[ax_n] = ax_si_base_z_unique[ax_n]
                        self.data_plot_obj_list[rep_indx].si_string_prefix_z_unique[ax_n] = [ax_si_string_prefix_z_unique[ax_n][0]]#np.array([ax_si_string_prefix_z_unique[ax_n][0]])#np.array([list([ax_si_string_prefix_z_unique[ax_n][0][0]])])
                        self.data_plot_obj_list[rep_indx].si_symbol_prefix_z_unique[ax_n] = [ax_si_symbol_prefix_z_unique[ax_n][0]]#np.array([list([ax_si_symbol_prefix_z_unique[ax_n][0][0]])])

    ##########################################################################
    def update_find_min_max(self, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]
        # Move the stored the min and max per axis to per set
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                self.data_plot_obj_list[rep_indx].x_ax_min_max_sets[set][i] = spec_h_use.ax_max_min_x_unique[index, :]
                self.data_plot_obj_list[rep_indx].y_ax_min_max_sets[set][i] = spec_h_use.ax_max_min_y_unique[index, :]
                self.data_plot_obj_list[rep_indx].z_ax_min_max_sets[set][i] = spec_h_use.ax_max_min_z_unique[index, :]
                self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i] = self.data_plot_obj_list[rep_indx].order_mag_x_data_unique[index]
                self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i] = self.data_plot_obj_list[rep_indx].order_mag_y_data_unique[index]
                self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i] = self.data_plot_obj_list[rep_indx].order_mag_z_data_unique[index]

        # Bounds normalised for each ax is calcuated
        for ax_n in range(len(self.data_plot_obj_list[rep_indx].ax_index_list_unique)):
            if np.abs(self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]) > 2:
                spec_h_use.ax_max_min_x_unique_normalised[ax_n] = spec_h_use.ax_max_min_x_unique_normalised[ax_n]/ 10**self.data_plot_obj_list[rep_indx].order_mag_x_data_unique[ax_n]
            if np.abs(self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]) > 2:
                spec_h_use.ax_max_min_y_unique_normalised[ax_n] = spec_h_use.ax_max_min_y_unique_normalised[ax_n]/ 10**self.data_plot_obj_list[rep_indx].order_mag_y_data_unique[ax_n]
            if np.abs(self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i]) > 2:
                spec_h_use.ax_max_min_z_unique_normalised[ax_n] = spec_h_use.ax_max_min_z_unique_normalised[ax_n]/ 10**self.data_plot_obj_list[rep_indx].order_mag_z_data_unique[ax_n]

    def label_change(self, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                    index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                    spec_h_use.dict_x_settings_array[set][i]['si_string_prefix'] = self.data_plot_obj_list[rep_indx].si_string_prefix_x_unique[index][0][0]
                    spec_h_use.dict_x_settings_array[set][i]['si_symbol_prefix'] = self.data_plot_obj_list[rep_indx].si_symbol_prefix_x_unique[index][0][0]
                    spec_h_use.dict_x_settings_array[set][i]['si_base'] = self.data_plot_obj_list[rep_indx].si_base_x_unique[index]
                    spec_h_use.dict_y_settings_array[set][i]['si_string_prefix'] = self.data_plot_obj_list[rep_indx].si_string_prefix_y_unique[index][0][0]
                    spec_h_use.dict_y_settings_array[set][i]['si_symbol_prefix'] = self.data_plot_obj_list[rep_indx].si_symbol_prefix_y_unique[index][0][0]
                    spec_h_use.dict_y_settings_array[set][i]['si_base'] = self.data_plot_obj_list[rep_indx].si_base_y_unique[index]
                    spec_h_use.dict_z_settings_array[set][i]['si_string_prefix'] = self.data_plot_obj_list[rep_indx].si_string_prefix_z_unique[index][0][0]
                    spec_h_use.dict_z_settings_array[set][i]['si_symbol_prefix'] = self.data_plot_obj_list[rep_indx].si_symbol_prefix_z_unique[index][0][0]
                    spec_h_use.dict_z_settings_array[set][i]['si_base'] = self.data_plot_obj_list[rep_indx].si_base_z_unique[index]
        # Update the dictionary
        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                    index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique == self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
                    spec_h_use.dict_x_settings_array[set][i]['use_si'] = self.data_plot_obj_list[rep_indx].use_si_x_unique[index]
                    spec_h_use.dict_y_settings_array[set][i]['use_si'] = self.data_plot_obj_list[rep_indx].use_si_y_unique[index]
                    spec_h_use.dict_z_settings_array[set][i]['use_si'] = self.data_plot_obj_list[rep_indx].use_si_z_unique[index]

        for set in range(len(self.set_num_list)):
            for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                    if self.data_plot_obj_list[rep_indx].set_has_data_in[set]:
                        if spec_h_use.dict_x_settings_array[set][i]['use_si']:
                            if np.abs(spec_h_use.dict_x_settings_array[set][i]['si_base']) > 2:
                                self.data_plot_obj_list[rep_indx].x_data[set][i] = self.data_plot_obj_list[rep_indx].x_data[set][i] / 10 ** spec_h_use.dict_x_settings_array[set][i]['si_base']
                                if self.data_plot_obj_list[rep_indx].x_err[set][i] is not None:
                                    self.data_plot_obj_list[rep_indx].x_err[set][i] = self.data_plot_obj_list[rep_indx].x_err[set][i] / 10 ** spec_h_use.dict_x_settings_array[set][i]['si_base']
                                spec_h_use.dict_x_settings_array[set][i]['units_x'] = str(spec_h_use.dict_x_settings_array[set][i]['si_symbol_prefix']) + str(spec_h_use.dict_x_settings_array[set][i]['units_x'])
                                self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i] = 1.  # It has already been set here to avoid it being used again set = 1
                        if spec_h_use.dict_y_settings_array[set][i]['use_si']:
                            if np.abs(spec_h_use.dict_y_settings_array[set][i]['si_base']) > 2:
                                self.data_plot_obj_list[rep_indx].y_data[set][i] = self.data_plot_obj_list[rep_indx].y_data[set][i] / 10 ** spec_h_use.dict_y_settings_array[set][i]['si_base']
                                if self.data_plot_obj_list[rep_indx].y_err[set][i] is not None:
                                    self.data_plot_obj_list[rep_indx].y_err[set][i] = self.data_plot_obj_list[rep_indx].y_err[set][i] / 10 ** spec_h_use.dict_y_settings_array[set][i]['si_base']
                                spec_h_use.dict_y_settings_array[set][i]['units_y'] = str(spec_h_use.dict_y_settings_array[set][i]['si_symbol_prefix']) + str(spec_h_use.dict_y_settings_array[set][i]['units_y'])
                                self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i] = 1.  # It has already been set here to avoid it being used again set = 1
                        if spec_h_use.dict_z_settings_array[set][i]['use_si']:
                            if np.abs(spec_h_use.dict_z_settings_array[set][i]['si_base']) > 2:
                                self.data_plot_obj_list[rep_indx].z_data[set][i] = self.data_plot_obj_list[rep_indx].z_data[set][i] / 10 ** spec_h_use.dict_z_settings_array[set][i]['si_base']
                                if self.data_plot_obj_list[rep_indx].z_err[set][i] is not None:
                                    self.data_plot_obj_list[rep_indx].z_err[set][i] = self.data_plot_obj_list[rep_indx].z_err[set][i] / 10 ** spec_h_use.dict_z_settings_array[set][i]['si_base']
                                spec_h_use.dict_z_settings_array[set][i]['units_z'] = str(spec_h_use.dict_z_settings_array[set][i]['si_symbol_prefix']) + str(spec_h_use.dict_z_settings_array[set][i]['units_z'])
                                self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i] = 1.  # It has already been set here to avoid it being used again set = 1

        if self.label_order_of_mag == 'divide_by_power_add_to_label':
            for set in range(len(self.set_num_list)):
                for i in range(len(self.data_plot_obj_list[rep_indx].ax_index_list[set])):
                    if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (
                            not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                        if not np.isnan(self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]):
                            if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None:
                                if np.abs(self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]) > 2:
                                    self.data_plot_obj_list[rep_indx].x_data[set][i] = self.data_plot_obj_list[rep_indx].x_data[set][i] / 10**self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]
                                    if self.data_plot_obj_list[rep_indx].x_err[set][i] is not None:
                                        self.data_plot_obj_list[rep_indx].x_err[set][i] = self.data_plot_obj_list[rep_indx].x_err[set][i] / 10**self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]
                                    spec_h_use.dict_x_settings_array[set][i]['units_x'] = '$10^{' + str(int(self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i])) +'}$' + spec_h_use.dict_x_settings_array[set][i]['units_x']
                            elif self.data_plot_obj_list[rep_indx].x_err[set][i] is not None:
                                if np.abs(self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]) > 2:
                                    self.data_plot_obj_list[rep_indx].x_err[set][i] = self.data_plot_obj_list[rep_indx].x_err[set][i] / 10 ** self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]
                                    spec_h_use.dict_x_settings_array[set][i]['units_x'] = '$10^{' + str(int(self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i])) +'}$' + spec_h_use.dict_x_settings_array[set][i]['units_x']
                        if not np.isnan(self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]):
                            if self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                                if np.abs(self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]) > 2:
                                    self.data_plot_obj_list[rep_indx].y_data[set][i] = self.data_plot_obj_list[rep_indx].y_data[set][i] / 10**self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]
                                    if self.data_plot_obj_list[rep_indx].y_err[set][i] is not None:
                                        self.data_plot_obj_list[rep_indx].y_err[set][i] = self.data_plot_obj_list[rep_indx].y_err[set][i] / 10**self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]
                                    spec_h_use.dict_y_settings_array[set][i]['units_y'] = '$10^{' + str(int(self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i])) +'}$' + spec_h_use.dict_y_settings_array[set][i]['units_y']
                            elif self.data_plot_obj_list[rep_indx].y_err[set][i] is not None:
                                if np.abs(self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]) > 2:
                                    self.data_plot_obj_list[rep_indx].y_err[set][i] = self.data_plot_obj_list[rep_indx].y_err[set][i] / 10**self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]
                                    spec_h_use.dict_y_settings_array[set][i]['units_y'] = '$10^{' + str(int(self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i])) +'}$' + spec_h_use.dict_y_settings_array[set][i]['units_y']
                        if not np.isnan(self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i]):
                            if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                                if np.abs(self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i]) > 2:
                                    self.data_plot_obj_list[rep_indx].z_data[set][i] = self.data_plot_obj_list[rep_indx].z_data[set][i] / 10**self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i]
                                    if self.data_plot_obj_list[rep_indx].z_err[set][i] is not None:
                                        self.data_plot_obj_list[rep_indx].z_err[set][i] = self.data_plot_obj_list[rep_indx].z_err[set][i] / 10**self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i]
                                    spec_h_use.dict_z_settings_array[set][i]['units_z'] = '$10^{' + str(int(self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i])) +'}$' + spec_h_use.dict_z_settings_array[set][i]['units_z']
                            elif self.data_plot_obj_list[rep_indx].z_err[set][i] is not None:
                                if np.abs(self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i]) > 2:
                                    self.data_plot_obj_list[rep_indx].z_err[set][i] = self.data_plot_obj_list[rep_indx].z_err[set][i] / 10 ** self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i]
                                    spec_h_use.dict_z_settings_array[set][i]['units_z'] = '$10^{' + str(int(self.data_plot_obj_list[rep_indx].order_mag_z_data_sets[set][i])) + '}$' + spec_h_use.dict_z_settings_array[set][i]['units_z']

    def get_position_text(self, rep_indx, set, i, loc_string_x, loc_string_y, percent_edge=0.2, fixed_x=None, fixed_y=None, x_min_shift=None, y_min_shift=None, use_twin_ax=False):
        if use_twin_ax:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]
        index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique ==
                         self.data_plot_obj_list[rep_indx].ax_index_list[set][i])
        x_width = spec_h_use.ax_max_min_x_unique_normalised[index, 1] - \
                  spec_h_use.ax_max_min_x_unique_normalised[index, 0]
        y_width = spec_h_use.ax_max_min_y_unique_normalised[index, 1] - \
                  spec_h_use.ax_max_min_y_unique_normalised[index, 0]

        if x_min_shift is not None:
            x_shift = np.nanmax([percent_edge * x_width, x_min_shift])
        else:
            x_shift = percent_edge * x_width
        if y_min_shift is not None:
            y_shift = np.nanmax([percent_edge * y_width, y_min_shift])
        else:
            y_shift = percent_edge * y_width

        if loc_string_x == 'right':
            x_shift_text = x_shift * 0.5
            if fixed_x is not None:
                x_pos = fixed_x
                x_pos_text = fixed_x - x_shift_text
            else:
                x_pos = spec_h_use.ax_max_min_x_unique_normalised[index, 1] - x_shift
                x_pos_text = spec_h_use.ax_max_min_x_unique_normalised[index, 1] - x_shift_text
            print('x_pos_text', spec_h_use.ax_max_min_x_unique_normalised[index, 1],  x_shift_text, x_shift)
        elif loc_string_x == 'left':
            x_shift_text = x_shift * 0.7
            if fixed_x is not None:
                x_pos = fixed_x
                x_pos_text = fixed_x + x_shift_text
            else:
                x_pos = spec_h_use.ax_max_min_x_unique_normalised[index, 0] + x_shift
                x_pos_text = spec_h_use.ax_max_min_x_unique_normalised[index, 0] + x_shift_text
        elif loc_string_x == 'centre':
            x_shift_text = 0.0
            if fixed_x is not None:
                x_pos = fixed_x
                x_pos_text = fixed_x
            else:
                x_pos = (spec_h_use.ax_max_min_x_unique_normalised[index, 1] + spec_h_use.ax_max_min_x_unique_normalised[index, 0])/2
                x_pos_text = (spec_h_use.ax_max_min_x_unique_normalised[index, 1] + spec_h_use.ax_max_min_x_unique_normalised[index, 0])/2
        else:
            raise ValueError("define xposition")


        if loc_string_y == 'upper':
            y_shift_text = y_shift * 0.7
            if fixed_y is not None:
                y_pos = fixed_y
                y_pos_text = fixed_y - y_shift_text
            else:
                y_pos = spec_h_use.ax_max_min_y_unique_normalised[index, 1] - y_shift
                y_pos_text = spec_h_use.ax_max_min_y_unique_normalised[index, 1] - y_shift_text
        elif loc_string_y == 'lower':
            y_shift_text = y_shift * 0.7
            if fixed_y is not None:
                y_pos = fixed_y
                y_pos_text = fixed_y - y_shift_text
            else:
                y_pos = spec_h_use.ax_max_min_y_unique_normalised[index, 0] + y_shift
                y_pos_text = spec_h_use.ax_max_min_y_unique_normalised[index, 0] + y_shift_text
        elif loc_string_y == 'centre':
            y_shift_text = 0.0
            if fixed_y is not None:
                y_pos = fixed_y
                y_pos_text = fixed_y - y_shift_text
            else:
                y_pos = (spec_h_use.ax_max_min_y_unique_normalised[index, 1] + spec_h_use.ax_max_min_y_unique_normalised[index, 0])/2
                y_pos_text = (spec_h_use.ax_max_min_y_unique_normalised[index, 1] + spec_h_use.ax_max_min_y_unique_normalised[index, 0])/2
        else:
            raise ValueError("define yposition")
        return x_pos, y_pos, x_shift, y_shift, x_shift_text, y_shift_text, x_pos_text, y_pos_text

    def plot_data(self, rep_indx, use_twin_ax=False):
        if use_twin_ax:
            ax_to_use = self.ax_twin
            dict_kwargs_array_use = self.data_plot_obj_list[rep_indx].specific_holder[1].dict_kwargs_array
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[1]
        else:
            ax_to_use = self.ax
            dict_kwargs_array_use = self.data_plot_obj_list[rep_indx].specific_holder[0].dict_kwargs_array
            spec_h_use = self.data_plot_obj_list[rep_indx].specific_holder[0]

        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                if (use_twin_ax and self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]) or (not use_twin_ax and not self.data_plot_obj_list[rep_indx].add_onto_twin_ax[set][i]):
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'line_plot':
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                            ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].plot(self.data_plot_obj_list[rep_indx].x_data[set][i], self.data_plot_obj_list[rep_indx].y_data[set][i], **dict_kwargs_array_use[set][i])
                        else:
                            warnings.warn("x or y empty for line plot set, i " + str(set)+' '+ str(i) + ' ' )
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'error_plot':
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                            if self.data_plot_obj_list[rep_indx].x_err[set][i] is not None and self.data_plot_obj_list[rep_indx].x_err_on[set][i]:
                                dict_kwargs_array_use[set][i]['xerr'] = self.data_plot_obj_list[rep_indx].x_err[set][i]
                                # Only plot every nth error bar
                                if self.data_plot_obj_list[rep_indx].thin_error_bar_num_x[set][i] != 1:
                                    index_keep = np.arange(0, len(dict_kwargs_array_use[set][i]['xerr']), int(self.data_plot_obj_list[rep_indx].thin_error_bar_num_x[set][i]))
                                    set_to_nan_bool = np.full((len(dict_kwargs_array_use[set][i]['xerr']), ), True)
                                    set_to_nan_bool[index_keep] = False
                                    dict_kwargs_array_use[set][i]['xerr'][set_to_nan_bool] = np.nan
                            if self.data_plot_obj_list[rep_indx].y_err[set][i] is not None and self.data_plot_obj_list[rep_indx].y_err_on[set][i]:
                                dict_kwargs_array_use[set][i]['yerr'] = self.data_plot_obj_list[rep_indx].y_err[set][i]
                                # Only plot every nth error bar
                                if self.data_plot_obj_list[rep_indx].thin_error_bar_num_y[set][i] != 1:
                                    index_keep = np.arange(0, len(dict_kwargs_array_use[set][i]['yerr']), int(self.data_plot_obj_list[rep_indx].thin_error_bar_num_y[set][i]))
                                    set_to_nan_bool = np.full((len(dict_kwargs_array_use[set][i]['yerr']), ), True)
                                    set_to_nan_bool[index_keep] = False
                                    dict_kwargs_array_use[set][i]['yerr'][set_to_nan_bool] = np.nan
                            ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].errorbar(self.data_plot_obj_list[rep_indx].x_data[set][i], self.data_plot_obj_list[rep_indx].y_data[set][i], **dict_kwargs_array_use[set][i])

                            if self.errorbar_standard_setting == 0:
                                if self.errorbar_background:
                                    exclude_keys = ['label', 'color']
                                    background_dict_kwargs = {}
                                    for key, value in dict_kwargs_array_use[set][i].items():
                                        if key not in exclude_keys:
                                            background_dict_kwargs[key] = value
                                    background_dict_kwargs["color"] = 'k'
                                    background_dict_kwargs["markersize"] = background_dict_kwargs["markersize"] +1.
                                    background_dict_kwargs["capsize"] = background_dict_kwargs["capsize"] + 1.
                                    background_dict_kwargs["capthick"] = background_dict_kwargs["capthick"] + 1.
                                    background_dict_kwargs["elinewidth"] = background_dict_kwargs["elinewidth"] +1.
                                    background_dict_kwargs["zorder"] = background_dict_kwargs["zorder"] -1.
                            if self.errorbar_background:
                                ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[rep_indx].shift_y_ax_mult].errorbar(self.data_plot_obj_list[rep_indx].x_data[set][i], self.data_plot_obj_list[rep_indx].y_data[set][i], **background_dict_kwargs)
                        else:
                            warnings.warn("x or y empty for line plot set, i " + str(set)+' '+ str(i) + ' ' )
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'add_text':
                        for j in range(len(self.data_plot_obj_list[rep_indx].x_data[set][i])):
                            if np.all(np.isnan(self.data_plot_obj_list[rep_indx].x_data[set][i])):
                                x_input_fix = None
                                y_input_fix = self.data_plot_obj_list[rep_indx].y_data[set][i][j]
                            else:
                                x_input_fix = self.data_plot_obj_list[rep_indx].x_data[set][i][j]
                                y_input_fix = None
                            x_pos, y_pos, x_shift, y_shift, x_shift_text, y_shift_text, x_pos_text, y_pos_text = self.get_position_text(
                                rep_indx=rep_indx, set=set, i=i, loc_string_x='right', loc_string_y='upper', x_min_shift=0.0,
                                y_min_shift=0.0, fixed_x=x_input_fix, fixed_y=y_input_fix, use_twin_ax=use_twin_ax)

                            if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                                ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                                    rep_indx].shift_x_ax_mult][
                                    self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                        rep_indx].shift_y_ax_mult].annotate(
                                    self.data_plot_obj_list[rep_indx].z_data[set][i], xy=(x_pos_text, y_pos_text),
                                    fontsize=self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize'],
                                    ha="center", va="center")
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'error_example':
                        index = np.where(self.data_plot_obj_list[rep_indx].ax_index_list_unique ==
                                         self.data_plot_obj_list[rep_indx].ax_index_list[set][i])

                        if self.data_plot_obj_list[rep_indx].x_err[set][i] is not None:
                            x_err = np.nanmean(self.data_plot_obj_list[rep_indx].x_err[set][i])
                        else:
                            x_err = None
                        if self.data_plot_obj_list[rep_indx].y_err[set][i] is not None:
                            y_err = np.nanmean(self.data_plot_obj_list[rep_indx].y_err[set][i])
                        else:
                            y_err = None

                        x_pos, y_pos, x_shift, y_shift, x_shift_text, y_shift_text, x_pos_text, y_pos_text = self.get_position_text(rep_indx=rep_indx, set=set, i=i, loc_string_x='right', loc_string_y='upper', x_min_shift=x_err, y_min_shift=y_err, use_twin_ax=use_twin_ax)
                        # loc = 'upperright'
                        # x_width = spec_h_use.ax_max_min_x_unique_normalised[index, 1] - \
                        #           spec_h_use.ax_max_min_x_unique_normalised[index, 0]
                        # y_width = spec_h_use.ax_max_min_y_unique_normalised[index, 1] - \
                        #           spec_h_use.ax_max_min_y_unique_normalised[index, 0]
                        # percent_edge = 0.1
                        # if self.data_plot_obj_list[rep_indx].x_err[set][i] is not None:
                        #     x_err = np.nanmean(self.data_plot_obj_list[rep_indx].x_err[set][i])
                        #     x_shift = np.nanmax([percent_edge * x_width, x_err])
                        # else:
                        #     x_shift = percent_edge * x_width
                        # if self.data_plot_obj_list[rep_indx].y_err[set][i] is not None:
                        #     y_err = np.nanmean(self.data_plot_obj_list[rep_indx].y_err[set][i])
                        #     y_shift = np.nanmax([percent_edge * y_width, y_err])
                        # else:
                        #     y_shift = percent_edge * y_width
                        #
                        # if loc == 'upperright':
                        #     y_shift_text = y_shift * 0.7
                        #     x_pos = spec_h_use.ax_max_min_x_unique_normalised[index, 1] - x_shift
                        #     y_pos = spec_h_use.ax_max_min_y_unique_normalised[index, 1] - y_shift
                        #
                        #     y_pos_text = spec_h_use.ax_max_min_y_unique_normalised[index, 1] - y_shift_text

                        if self.data_plot_obj_list[rep_indx].y_err[set][i] is not None and self.data_plot_obj_list[rep_indx].y_err_on[set][i]:
                            x_pos_text = spec_h_use.ax_max_min_x_unique_normalised[index, 1] - 0.3*x_shift # Manually changed from +0.5* for now
                            print('x_pos_text', x_pos_text, x_shift_text, spec_h_use.ax_max_min_x_unique_normalised[index, 1] ,  0.5*x_shift)
                            text_str = 'example \nerror bar'
                        else:
                            x_pos_text = x_pos
                            text_str = 'example error bar'

                        if self.data_plot_obj_list[rep_indx].x_err[set][i] is not None and self.data_plot_obj_list[rep_indx].x_err_on[set][i]:
                            dict_kwargs_array_use[set][i]['xerr'] = x_err

                        if self.data_plot_obj_list[rep_indx].y_err[set][i] is not None and self.data_plot_obj_list[rep_indx].y_err_on[set][i]:
                            dict_kwargs_array_use[set][i]['yerr'] = y_err

                        ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].errorbar(x_pos, y_pos, **dict_kwargs_array_use[set][i])
                        if self.errorbar_standard_setting == 0:
                            if self.errorbar_background:
                                dict_kwargs_array_use[set][i]["color"] = 'k'
                                dict_kwargs_array_use[set][i]["markersize"] = \
                                dict_kwargs_array_use[set][i]["markersize"] + 1.
                                dict_kwargs_array_use[set][i]["capsize"] = \
                                dict_kwargs_array_use[set][i]["capsize"] + 1.
                                dict_kwargs_array_use[set][i]["capthick"] = \
                                dict_kwargs_array_use[set][i]["capthick"] + 1.
                                dict_kwargs_array_use[set][i]["elinewidth"] = \
                                dict_kwargs_array_use[set][i]["elinewidth"] + 1.
                                dict_kwargs_array_use[set][i]["zorder"] = \
                                dict_kwargs_array_use[set][i]["zorder"] - 1.
                        if self.errorbar_background:
                            ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                                rep_indx].shift_x_ax_mult][
                                self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                    rep_indx].shift_y_ax_mult].errorbar(x_pos, y_pos, **dict_kwargs_array_use[set][i])
                        ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                            rep_indx].shift_x_ax_mult][
                            self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                rep_indx].shift_y_ax_mult].annotate(text_str, xy=(x_pos_text, y_pos_text),
                            fontsize=self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize'],
                            ha="center", va="center")

                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'arrow_plot':
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                            x = self.data_plot_obj_list[rep_indx].x_data[set][i][0]
                            x2 = self.data_plot_obj_list[rep_indx].x_data[set][i][-1]
                            dx = x2 - x
                            y = self.data_plot_obj_list[rep_indx].y_data[set][i][0]
                            y2 = self.data_plot_obj_list[rep_indx].y_data[set][i][-1]
                            dy = y2 - y
                            ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].arrow(x, y, dx, dy, **dict_kwargs_array_use[set][i])
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_plot':
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                            for j in range(len(self.data_plot_obj_list[rep_indx].x_data[set][i])):
                                ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                                    rep_indx].shift_x_ax_mult][
                                    self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                        rep_indx].shift_y_ax_mult].scatter(
                                    self.data_plot_obj_list[rep_indx].x_data[set][i][j], self.data_plot_obj_list[rep_indx].y_data[set][i][j],  marker='o', s=30, **dict_kwargs_array_use[set][i]
                                )
                                # circle = Circle((self.data_plot_obj_list[rep_indx].x_data[set][i][j], self.data_plot_obj_list[rep_indx].y_data[set][i][j]))
                                # ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].add_artist(circle)
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_at_mean_plot':
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:

                            x_mean = np.mean(self.data_plot_obj_list[rep_indx].x_data[set][i])
                            y_mean = np.mean(self.data_plot_obj_list[rep_indx].y_data[set][i])
                            ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                                rep_indx].shift_x_ax_mult][
                                self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                    rep_indx].shift_y_ax_mult].scatter(
                                x_mean, y_mean, s=(1.1*self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize'])**2, **dict_kwargs_array_use[set][i]
                            )
                            if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                                ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                                    rep_indx].shift_x_ax_mult][
                                    self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                        rep_indx].shift_y_ax_mult].annotate(
                                    self.data_plot_obj_list[rep_indx].z_data[set][i], xy=(x_mean, y_mean), fontsize=self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize'], ha="center", va="center")
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_at_start_plot':
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:

                            if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                                diff_x = self.data_plot_obj_list[rep_indx].x_ax_min_max_sets[set][i][0][0][1] - self.data_plot_obj_list[rep_indx].x_ax_min_max_sets[set][i][0][0][0]
                                diff_y = self.data_plot_obj_list[rep_indx].y_ax_min_max_sets[set][i][0][0][1] - self.data_plot_obj_list[rep_indx].y_ax_min_max_sets[set][i][0][0][0]
                                shift_val_x = diff_x * self.shift_marker_labels_frac[0]
                                shift_val_y = diff_y * self.shift_marker_labels_frac[1]
                                if spec_h_use.dict_x_settings_array[set][i]['use_si']:
                                    if np.abs(spec_h_use.dict_x_settings_array[set][i]['si_base']) > 2:
                                        shift_val_x = shift_val_x / 10 ** spec_h_use.dict_x_settings_array[set][i]['si_base']
                                if spec_h_use.dict_y_settings_array[set][i]['use_si']:
                                    if np.abs(spec_h_use.dict_y_settings_array[set][i]['si_base']) > 2:
                                        shift_val_y = shift_val_y / 10 ** spec_h_use.dict_y_settings_array[set][i]['si_base']
                                if self.label_order_of_mag == 'divide_by_power_add_to_label':
                                    if not np.isnan(self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]):
                                        if np.abs(self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]) > 2:
                                            shift_val_x = shift_val_x / 10 ** self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]
                                    if not np.isnan(self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]):
                                        if np.abs(self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]) > 2:
                                            shift_val_y = shift_val_y / 10 ** self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]

                                x_s = self.data_plot_obj_list[rep_indx].x_data[set][i][0]
                                y_s = self.data_plot_obj_list[rep_indx].y_data[set][i][0]
                                x_s = x_s + shift_val_x
                                y_s = y_s + shift_val_y
                                dict_kwargs_array_use[set][i]['facecolors'] = 'none'
                                dict_kwargs_array_use[set][i]['edgecolors'] = 'k'

                                ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                                    rep_indx].shift_x_ax_mult][
                                    self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                        rep_indx].shift_y_ax_mult].scatter(
                                    x_s, y_s,
                                    s=(1.1 * self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize']) ** 2,
                                    **dict_kwargs_array_use[set][i]
                                )
                                if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                                    ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                                        rep_indx].shift_x_ax_mult][
                                        self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                            rep_indx].shift_y_ax_mult].annotate(
                                        self.data_plot_obj_list[rep_indx].z_data[set][i], xy=(x_s, y_s),
                                        fontsize=self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize'],
                                        ha="center", va="center")
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'circle_at_end_plot':
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:

                            if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                                diff_x = self.data_plot_obj_list[rep_indx].x_ax_min_max_sets[set][i][0][0][1] - \
                                         self.data_plot_obj_list[rep_indx].x_ax_min_max_sets[set][i][0][0][0]
                                diff_y = self.data_plot_obj_list[rep_indx].y_ax_min_max_sets[set][i][0][0][1] - \
                                         self.data_plot_obj_list[rep_indx].y_ax_min_max_sets[set][i][0][0][0]
                                shift_val_x = diff_x * self.shift_marker_labels_frac[0]
                                shift_val_y = diff_y * self.shift_marker_labels_frac[1]
                                if spec_h_use.dict_x_settings_array[set][i]['use_si']:
                                    if np.abs(spec_h_use.dict_x_settings_array[set][i]['si_base']) > 2:
                                        shift_val_x = shift_val_x / 10 ** \
                                                      spec_h_use.dict_x_settings_array[set][i]['si_base']
                                if spec_h_use.dict_y_settings_array[set][i]['use_si']:
                                    if np.abs(spec_h_use.dict_y_settings_array[set][i]['si_base']) > 2:
                                        shift_val_y = shift_val_y / 10 ** \
                                                      spec_h_use.dict_y_settings_array[set][i]['si_base']
                                if self.label_order_of_mag == 'divide_by_power_add_to_label':
                                    if not np.isnan(self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]):
                                        if np.abs(self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]) > 2:
                                            shift_val_x = shift_val_x / 10 ** \
                                                          self.data_plot_obj_list[rep_indx].order_mag_x_data_sets[set][i]
                                    if not np.isnan(self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]):
                                        if np.abs(self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]) > 2:
                                            shift_val_y = shift_val_y / 10 ** \
                                                          self.data_plot_obj_list[rep_indx].order_mag_y_data_sets[set][i]
                                x_e = self.data_plot_obj_list[rep_indx].x_data[set][i][-1]
                                y_e = self.data_plot_obj_list[rep_indx].y_data[set][i][-1]
                                x_e = x_e + shift_val_x
                                y_e = y_e + shift_val_y
                                dict_kwargs_array_use[set][i]['facecolors'] = 'none'
                                dict_kwargs_array_use[set][i]['edgecolors'] = 'k'
                                ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                                    rep_indx].shift_x_ax_mult][
                                    self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                        rep_indx].shift_y_ax_mult].scatter(
                                    x_e, y_e,
                                    s=(1.1 * self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize']) ** 2,
                                    **dict_kwargs_array_use[set][i]
                                )
                                if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                                    ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[
                                        rep_indx].shift_x_ax_mult][
                                        self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[
                                            rep_indx].shift_y_ax_mult].annotate(
                                        self.data_plot_obj_list[rep_indx].z_data[set][i], xy=(x_e, y_e),
                                        fontsize=self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['fontsize'],
                                        ha="center", va="center")

                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'scatter':
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                            if self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                                dict_kwargs_array_use[set][i]['c'] = self.data_plot_obj_list[rep_indx].z_data[set][i]
                                self.data_plot_obj_list[rep_indx].post_plot_set[set][i] = \
                                ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[rep_indx].shift_x_ax_mult][
                                    self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[rep_indx].shift_y_ax_mult].scatter(
                                    self.data_plot_obj_list[rep_indx].x_data[set][i],
                                    self.data_plot_obj_list[rep_indx].y_data[set][i], **dict_kwargs_array_use[set][i])
                            else:
                                ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0] + self.data_plot_obj_list[rep_indx].shift_x_ax_mult][
                                    self.data_plot_obj_list[rep_indx].index_list[set][i, 1] + self.data_plot_obj_list[rep_indx].shift_y_ax_mult].scatter(
                                    self.data_plot_obj_list[rep_indx].x_data[set][i],
                                    self.data_plot_obj_list[rep_indx].y_data[set][i], **dict_kwargs_array_use[set][i])

                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == '2d_colour_plot':
                        if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None and self.data_plot_obj_list[rep_indx].y_data[set][i] is not None and self.data_plot_obj_list[rep_indx].z_data[set][i] is not None:
                            self.data_plot_obj_list[rep_indx].post_plot_set[set][i] = ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].pcolormesh(
                                self.data_plot_obj_list[rep_indx].x_data[set][i], self.data_plot_obj_list[rep_indx].y_data[set][i], self.data_plot_obj_list[rep_indx].z_data[set][i]
                            )
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'axspan_plot':
                        if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['h_plot_axspan'] is None:
                            raise ValueError("h_plot_axspan must not be None")

                        if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['h_plot_axspan']:
                            if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None:
                                ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].axvspan(self.data_plot_obj_list[rep_indx].x_data[set][i][0], self.data_plot_obj_list[rep_indx].x_data[set][i][1], **dict_kwargs_array_use[set][i])
                        else:
                            if self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                                ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].axhspan(self.data_plot_obj_list[rep_indx].y_data[set][i][0], self.data_plot_obj_list[rep_indx].y_data[set][i][1], **dict_kwargs_array_use[set][i])
                    if self.data_plot_obj_list[rep_indx].type_plot_ax[set][i] == 'axline_plot':
                        if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['h_plot_axline'] is None:
                            raise ValueError("h_plot_axline must not be None")
                        if not self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['h_plot_axline']:
                            if self.data_plot_obj_list[rep_indx].x_data[set][i] is not None:
                                for line_i in self.data_plot_obj_list[rep_indx].x_data[set][i]:
                                    ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].axvline(
                                        line_i, **dict_kwargs_array_use[set][i]
                                    )
                        else:
                            if self.data_plot_obj_list[rep_indx].y_data[set][i] is not None:
                                for line_i in self.data_plot_obj_list[rep_indx].y_data[set][i]:
                                    ax_to_use[self.data_plot_obj_list[rep_indx].index_list[set][i, 0]+self.data_plot_obj_list[rep_indx].shift_x_ax_mult][self.data_plot_obj_list[rep_indx].index_list[set][i, 1]+self.data_plot_obj_list[rep_indx].shift_y_ax_mult].axhline(
                                        line_i, ** dict_kwargs_array_use[set][i]
                                    )
    def save_check_bool(self, stored_obj, stored_obj_index, rep_indx):
        """
        stored_obj is self.stored_x or _y _z. index 0 1 2 respectively
        """
        # Overwrite
        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                if self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+9] == set:
                    if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['save_signal']:
                        pass
                    else:
                       self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['save_signal'] = stored_obj[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index]].save_this[self.data_plot_obj_list[rep_indx].index_link[set][i][stored_obj_index+3]]

    def save_signal(self, rep_indx):
        # Intitialise
        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['save_signal'] = None
        self.save_check_bool(self.data_plot_obj_list[rep_indx].stored_x, 0, rep_indx=rep_indx)
        self.save_check_bool(self.data_plot_obj_list[rep_indx].stored_y, 1, rep_indx=rep_indx)
        self.save_check_bool(self.data_plot_obj_list[rep_indx].stored_z, 2, rep_indx=rep_indx)

        for set in range(len(self.set_num_list)):
            for i in range(np.shape(self.data_plot_obj_list[rep_indx].index_link[set])[0]):  # Every signal
                if self.data_plot_obj_list[rep_indx].dict_psettings_array[set][i]['save_signal']:
                    raise ValueError("not implemented.  I would have to recover some signals if they get normalised. "
                                     "Would be easier to save elsewher otherwise")
                    self.save_npz(set, i)

        # Separately save figure as svg prep (saving done in save_fig)
        if self.save_to_svg:
            for ax_i in range(len(self.ax)):
                for ax_j in range(len(self.ax[ax_i])):
                    self.ax[ax_i][ax_j].axis('off')
                    self.ax[ax_i][ax_j].set_position([0, 0, 1, 1])

    def save_fig(self, direc):
        if self.save_to_svg:
            parts = [self.plot_key + '.svg']
            filename_gen = Os_path(direc).joinpath(*parts)
            filename_gen = str(filename_gen)

            self.fig.savefig(filename_gen)
    def save_npz(self, set, i):
        raise ValueError("not implemented.  I would have to recover some signals if they get normalised. "
                         "Would be easier to save elsewher otherwise")
        xdata = getattr(self.data_plot_obj_list[rep_indx].stored_x[self.data_plot_obj_list[rep_indx].index_link[set][i][0]], self.type_x_plot[set][i])

        np.savez_compressed(xdata)

    @staticmethod
    def get_axes_index(current_index, fixed_num_rows, fixed_num_cols=None):
        ax_inx = current_index // int(fixed_num_rows)  # index is the floor and remainder
        ax_iny = current_index % int(fixed_num_rows)
        if fixed_num_cols is not None:
            # If a fixed number of columns, then start from the begining again.
            # Also I probably got rows and columns labeled wrong
            if ax_inx >= fixed_num_cols:
                n = int(np.ceil(ax_inx/fixed_num_cols))
                ax_inx = int(ax_inx - fixed_num_cols * n)
        axes_list = [ax_inx, ax_iny]
        return axes_list

    def get_plot_index(self):
        ax_index_list_unique_last = []
        for rep_indx in range(self.max_index_repeated):
            ax_index_list_unique = np.array([y for x in self.data_plot_obj_list[rep_indx].ax_index_list for y in x])
            ax_index_list_unique = np.unique(ax_index_list_unique)
            self.data_plot_obj_list[rep_indx].ax_index_list_unique = ax_index_list_unique
            self.ax_index_list_unique_max = np.unique(np.hstack((ax_index_list_unique_last, ax_index_list_unique)))
            ax_index_list_unique_last = ax_index_list_unique

        total_len = int(np.max(self.ax_index_list_unique_max)) + 1
        if self.n_rows_cols_ax[0] is None:
            if self.n_rows_cols_ax[1] is None:
                # Do a square plot
                self.n_x_ax = int(np.sqrt(total_len))
                self.n_y_ax = int(np.sqrt(total_len))
            else:
                if total_len < self.n_rows_cols_ax[1]:
                    self.n_x_ax = 1
                    self.n_y_ax = total_len
                else:
                    self.n_x_ax = int(np.ceil(total_len/self.n_rows_cols_ax[1]))
                    self.n_y_ax = self.n_rows_cols_ax[1]
        else:
            if self.n_rows_cols_ax[1] is None:
                if total_len < self.n_rows_cols_ax[0]:
                    self.n_x_ax = total_len
                    self.n_y_ax = 1
                else:
                    self.n_x_ax = self.n_rows_cols_ax[0]
                    self.n_y_ax = int(np.ceil(total_len / self.n_rows_cols_ax[0]))
            else:
                self.n_x_ax = self.n_rows_cols_ax[0]
                self.n_y_ax = self.n_rows_cols_ax[1]
        if self.n_x_ax == 0:
            raise ValueError(
                "No axes will be made, "
                "Try adding the plot to the input data dictionary of the data you want to plot"
            )
        if self.n_y_ax == 0:
            raise ValueError(
                "No axes will be made, "
                "Try adding the plot to the input data dictionary of the data you want to plot"
            )
