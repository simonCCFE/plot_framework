import numpy as np
from useful_methods import Usefulmethods, UsefulSmoothing, UsefulSetPaths, UsefulFiltering
from pathlib import Path as Os_path
from itertools import combinations
from data_settings import SaveSettings
import warnings
from copy import deepcopy
from scipy.interpolate import interp1d


import pickle

# def memoize2(file_name):
#     print('argument', file_name)
#     filename_data_pl = Os_path(file_name)
#     if filename_data_pl.is_file():
#         print('reading cache file')
#         cache = pickle.load(open( str(filename_data_pl), "rb" ) )
#     else:
#         cache = {}
#
#     def decorator(function):
#         print('function', function)
#         def wrapper(*args, **kwargs):
#             """ args and kwargs must be dict"""
#             # find were args is not an instance, so this works on methods too
#             if len(args) > 2:
#                 raise ValueError('only account for 1 input atm')
#             for args_iter in args:
#                 print('args_iter', args_iter)
#                 if callable(args_iter):
#                     pass
#                 else:
#                     arguments_sep = args_iter
#                     print(arguments_sep)
#             args_in = args
#             args_used = arguments_sep
#
#             # Check if the args match the args in the cache
#             redo_cache = False
#             for key, val in args_used.items():
#                 if key in cache:
#                     if np.all(args_used[key] == cache[key]):
#                         pass
#                     else:
#                         redo_cache = True
#                 else:
#                     redo_cache = True
#
#             if redo_cache:
#                 print('Running func')
#                 result = deepcopy(function(*args_in, **kwargs))
#                 # update the cache file
#                 pickle.dump(result, open(file_name, "wb"))
#             else:
#                 print('result in cache')
#                 result = cache
#             return result
#         return wrapper
#     return decorator
#
# class memoize(object):
#     def __init__(self, file_name):
#         filename_data_pl = Os_path(file_name)
#         if filename_data_pl.is_file():
#             print('reading cache file')
#             with open(file_name) as f:
#                 cache = pickle.load(f)
#         else:
#             cache = {}
#         self.cache = cache
#         self.file_name = file_name
#
#     def __call__(self, original_func):
#         print(original_func)
#         cache = deepcopy(original_func(param))
#         raise ValueError('self.cache'+self.file_name)
#         quit()
#         print(param, type(param))
#         print(cache, type(cache))
#
#         if param != cache:
#             print('Running func')
#
#             cache = deepcopy(original_func(param))
#             # update the cache file
#             with open(file_name, 'wb') as f:
#                 pickle.dump(cache, f)
#         else:
#             print('result in cache')
#         return self.cache

    # @staticmethod
    # def persist_to_file(file_name):
    #     def decorator(original_func):
    #         filename_data_pl = Os_path(file_name)
    #         if filename_data_pl.is_file():
    #             print('reading cache file')
    #             with open(file_name) as f:
    #                 cache = pickle.load(f)
    #         else:
    #             cache = {}
    #         # @wraps(original_func)
    #         def new_func(param):
    #             print(param, type(param))
    #             print(cache, type(cache))
    #
    #             if param != cache:
    #                 print('Running func')
    #
    #                 cache = deepcopy(original_func(param))
    #                 # update the cache file
    #                 with open(file_name, 'wb') as f:
    #                     pickle.dump(cache, f)
    #             else:
    #                 print('result in cache')
    #             return cache[param]
    #         return new_func
    #     return decorator
# @memoize2('cache.p')
# def get_nchannels_to_dict(dict_return):
#     if "time" in dict_return.keys():
#         if dict_return["data"] is not None:
#             if isinstance(dict_return["data"], (list)):#,pd.core.series.Series,np.ndarray)):
#                 dict_return["data"] = np.array(dict_return["data"])
#             if dict_return["data"].ndim > 1:
#                 dict_return['channels'] = np.shape(dict_return["data"])[1]
#             else:
#                 dict_return['channels'] = 1
#     return dict_return



class LoadSettings(object):
    """

    """

    def __init__(self):
        self.x_set = None
        self.y_set = None

    def add_load_info(self, key, label, units):
        self.is_used = True
        self.key = key
        self.label = label
        self.units = units

    def add_to_set(self, x_set, y_set=None):
        self.x_set = x_set
        self.y_set = y_set  # 2d data type will need a ytype set to match axis for some graphs

    # @staticmethod
    # def get_nchannels_to_dict(dict_return):
    #     if "time" in dict_return.keys():
    #         if dict_return["data"] is not None:
    #             if isinstance(dict_return["data"], (list)):#,pd.core.series.Series,np.ndarray)):
    #                 dict_return["data"] = np.array(dict_return["data"])
    #             if dict_return["data"].ndim > 1:
    #                 dict_return['channels'] = np.shape(dict_return["data"])[1]
    #             else:
    #                 dict_return['channels'] = 1
    # @memoize2('cache.p')
    def get_nchannels_to_dict(self, dict_return):
        if "time" in dict_return.keys():
            if dict_return["data"] is not None:
                if isinstance(dict_return["data"], (list)):#,pd.core.series.Series,np.ndarray)):
                    dict_return["data"] = np.array(dict_return["data"])
                if dict_return["data"].ndim > 1:
                    dict_return['channels'] = np.shape(dict_return["data"])[1]
                else:
                    dict_return['channels'] = 1
            else:
                dict_return['channels'] = None
        return dict_return

class LoadSettingsFile(LoadSettings):
    """

    """

    def __init__(self):
        LoadSettings.__init__(self)

    def add_load_info(self, filename_data, direc, filename_time=None):
        self.filename_data = filename_data
        self.filename_time = filename_time
        self.direc = direc

    def load_vals_dict(self, object_in=None, current_uid=None):
        """
        Unpacks the chain of where in the object that a value is in
        :param object_in:
        :return:
        """
        dict_return = {}
        parts = [self.filename_data]
        filename_data = Os_path(self.direc).joinpath(*parts)
        filename_data = str(filename_data)
        parts = [self.filename_time]
        filename_time = Os_path(self.direc).joinpath(*parts)
        filename_time = str(filename_time)
        dict_return['data'] = np.load(filename_data)
        dict_return['time'] = np.load(filename_time)

        return dict_return

class SetSimilarData():
    """
    Make a set of similar data types
    """

    def __init__(self):
        pass

class LoadSettingsInterpDict(LoadSettings):
    """

    """

    def __init__(self):
        LoadSettings.__init__(self)

    def add_load_info(self, filename_data, direc, pulse):
        'filtered_or_smoothed_data'
        parts = ['interped_data']
        direc = Os_path(direc).joinpath(*parts)
        Usefulmethods.path_check_and_make(direc)
        parts = [str(pulse) + '_' + filename_data]  # DOnt include npy here because it gets added later
        filename_data = Os_path(direc).joinpath(*parts)
        filename_data = str(filename_data)
        self.filename_data = filename_data

    def load_vals_dict(self, object_in, current_uid=None):
        """
        Unpacks the chain of where in the object that a value is in
        :param object_in:
        :return:
        """
        dict_return = {}
        return dict_return


class LoadSettingsFilteredSmoothDict(LoadSettings):
    """

    """

    def __init__(self):
        LoadSettings.__init__(self)

    def add_load_info(self, filename_data, direc, pulse, filt_type_str=None, smooth_type_str=None):
        if filt_type_str is None:
            filt_type_str = ''
        if smooth_type_str is None:
            smooth_type_str = ''
        parts = ['filtered_or_smoothed_data']
        direc = Os_path(direc).joinpath(*parts)
        Usefulmethods.path_check_and_make(direc)
        parts = [str(pulse) + '_' + filename_data + filt_type_str + smooth_type_str + '_saved.npy']
        filename_data = Os_path(direc).joinpath(*parts)
        filename_data = str(filename_data)
        self.filename_data = filename_data

    def load_vals_dict(self, object_in, current_uid=None):
        """
        Unpacks the chain of where in the object that a value is in
        :param object_in:
        :return:
        """
        dict_return = {}
        return dict_return

class LoadSettingsChannelDict(LoadSettings):
    """

    """

    def __init__(self):
        LoadSettings.__init__(self)

    def add_load_info(self, filename_data, direc, pulse):
        'filtered_or_smoothed_data'
        parts = ['channel_data']
        direc = Os_path(direc).joinpath(*parts)
        Usefulmethods.path_check_and_make(direc)
        parts = [str(pulse) + '_' + filename_data + '_saved.npy']
        filename_data = Os_path(direc).joinpath(*parts)
        filename_data = str(filename_data)
        self.filename_data = filename_data

    def load_vals_dict(self, object_in, current_uid=None):
        """
        Unpacks the chain of where in the object that a value is in
        :param object_in:
        :return:
        """

        dict_return = {}
        return dict_return

class LoadSettingsSmoothedDict(LoadSettings):
    """

    """

    def __init__(self):
        LoadSettings.__init__(self)

    def add_load_info(self):
        pass

    def load_vals_dict(self, object_in, current_uid=None):
        """
        Unpacks the chain of where in the object that a value is in
        :param object_in:
        :return:
        """
        dict_return = {}
        return dict_return

class LoadSettingsTestDict(LoadSettings):
    """

    """

    def __init__(self):
        LoadSettings.__init__(self)

    def add_load_info(self, point_to_obj):
        self.point_to_obj = point_to_obj

    def load_vals_dict(self, object_in, current_uid=None):
        """
        Unpacks the chain of where in the object that a value is in
        :param object_in:
        :return:
        """
        dict_return = {}
        for keys, values in self.point_to_obj.dict_type_to_attribute_list.items():
            data_obj = object_in
            for i in range(len(values['type_list']) - 1):
                if values['type_list'][i] == "obj":
                    try:
                        data_obj = getattr(data_obj, values['attribute_or_index_list'][i])
                    except:
                        raise ValueError(
                            "can't find " + str(values['attribute_or_index_list'][i]) + " in the current object")
                elif values['type_list'][i] == "list":
                    raise ValueError("not implemented")
            dict_return[values['type_list'][-1]] = getattr(data_obj, values['attribute_or_index_list'][-1])
        return dict_return


class LoadSettingsTest(LoadSettings):
    """

    """


    def __init__(self):
        pass

    def add_load_info(self):
        pass


class PointToObject(object):
    """
    """

    def __init__(self, type_list, attribute_or_index_list):
        i = 0
        self.dict_type_to_attribute_list = {}
        index_list = []
        self.dict_type_to_attribute_list[str(0)] = {}
        while i < np.shape(type_list)[0]:  # goes through type_list
            if np.shape(type_list[i])[0] > len(index_list):
                for j in range(len(index_list), len(type_list[i]), 1):
                    if j > 0:
                        self.dict_type_to_attribute_list[str(j)] = deepcopy(self.dict_type_to_attribute_list[str(j-1)])
                    else:
                        self.dict_type_to_attribute_list[str(j)] = {}
                        self.dict_type_to_attribute_list[str(j)]["type_list"] = []
                        self.dict_type_to_attribute_list[str(j)]["attribute_or_index_list"] = []
                index_list = [i for i in range(len(type_list[i]))]

            for j in range(len(index_list)):
                self.dict_type_to_attribute_list[str(j)]["type_list"].append(type_list[i][j])
                self.dict_type_to_attribute_list[str(j)]["attribute_or_index_list"].append(attribute_or_index_list[i][j])
            i = i + 1


class DataTransform(object):
    """
    """

    def __init__(self):
        pass


class PlotReferences(object):
    """
    """

    def __init__(self, plot_str, is_x_list, is_y_list, is_z_list, overplot_with_num=None, type_plot_use=None):
        self.overwrite_dict = {}
        self.overwrite_dict["type_plot_use"] = {}
        self.overwrite_dict["type_plot_use"][plot_str] = type_plot_use
        self.plot_str = plot_str


class DataArray:
    """
    Create an empty data array
    """
    def __init__(self):
        self.key = None
        self.key_unfiltered = None
        self.key_unsmoothed = None
        # self.required_class_to_load = None
        self.time = None
        self.data = None
        self.error = None
        self.time_error = None
        self.label = None
        self.units = None
        self.is_used = False
        self.load_obj = None  # uid, dda, data_type_str, elm_filter_bool, smooth_bool, percent_to_smooth, is_sav_bool, x_is_data, elm_cycle_filter_bool, time_to_smooth, gaussian_smooth_fwhm, gaussian_smooth
        self.load_obj_next_index = 0
        self.list_of_plots_used = []
        self.time_dependent = True

        # Change these to overwriting list
        self.marker_use = None
        self.colour_use = None,
        self.linestyle_use = None
        self.overwrite_obj = None
        self.error_on = True  # no_error = False
        self.cmap_use = None
        self.norm_cmap_use = None
        self.can_be_filtered_bool = False
        self.can_be_interped_bool = False

        # make a new data signal for these
        self.do_filtering = False
        self.do_interp = False

    def get_data_settings(self, key, dict_of_add_load_info, dict_of_add_basic_info):
        load_basic_info = dict_of_add_basic_info[key]
        for keys, value in load_basic_info.items():
            setattr(self, keys, value)  # label and units by default, others if called again
        load_loading_info = dict_of_add_load_info[key]
        for keys, value in load_loading_info.items():
            setattr(self, keys, value)  # loads the pointer obj or data_path

    def load_vals(self, object_in=None, current_uid_list=None, secondary=False):
        if secondary:
            dict_of_data = self.load_obj_next_list[self.load_obj_next_index].load_vals_dict(object_in, current_uid_list)  #
            self.load_obj_next_index = self.load_obj_next_index + 1
        else:
            dict_of_data = self.load_obj.load_vals_dict(object_in, current_uid_list)  #
        # get_nchannels_to_dict(dict_of_data)
        self.load_obj.get_nchannels_to_dict(dict_of_data)
        # quit()
        # for keys, values in dict_of_data.items():
        #     setattr(self, keys, values)
        return dict_of_data


    def add_channel_split_vals(self, input_data_obj_list, input_data_settings):

        dict_to_join_add_basic_info = {}
        dict_to_join_type_to_index = {}
        dict_of_add_load_info = {}
        if self.split_into_channels:
            store_for_these_channels = input_data_settings.dict_of_add_basic_info[self.key]['dict_channels']['store_for_these_channels']
            filt_smooth_dict = input_data_settings.dict_of_add_basic_info[self.key]['dict_channels_filter_smooth_settings']
            filtered_list = []
            smoothed_list = []
            filtered_list.append(False)  # Make unfiltered unsmoothed
            smoothed_list.append(False)
            if filt_smooth_dict['filter_bool']:
                filtered_list.append(True)
                smoothed_list.append(False)
                if filt_smooth_dict['smooth_bool']:
                    filtered_list.append(True)
                    smoothed_list.append(True)
            else:
                if filt_smooth_dict['smooth_bool']:
                    filtered_list.append(False)
                    smoothed_list.append(True)
            input_data_obj = DataArray()

            # Add to the dict
            for filt_sm_index in range(len(filtered_list)):
                filt_bool = filtered_list[filt_sm_index]
                smooth_bool = smoothed_list[filt_sm_index]
                if not filt_bool and not smooth_bool:
                    print(self.key)
                    for ch_i in range(self.channels):
                        if ch_i in store_for_these_channels:
                            key_i = self.key +'_channel_' + str(ch_i)
                            dict_to_join_add_basic_info, dict_to_join_type_to_index = input_data_settings.fill_dict_of_add_basic_info(key_i, self.label + " channel " + str(ch_i), self.units, gen_label=self.gen_label, dict_of_add_basic_info=dict_to_join_add_basic_info, data_type_to_index_dict=dict_to_join_type_to_index)
                            new_load_setting = LoadSettingsChannelDict()
                            new_load_setting.add_load_info(filename_data=key_i, direc=input_data_settings.gen_path, pulse=input_data_settings.pulse)
                            dict_of_add_load_info = input_data_settings.fill_dict_of_add_load_info(key_i, new_load_setting, dict_of_add_load_info=dict_of_add_load_info)

                if filt_bool and not smooth_bool:
                    for ch_i in range(self.channels):
                        if ch_i in store_for_these_channels:
                            key_i = self.key + '_channel_' + str(ch_i)
                            key_i_filt = self.key + '_channel_' + str(ch_i) +'_filtered'
                            new_load_setting_filtered = LoadSettingsFilteredSmoothDict()
                            new_load_setting_filtered.add_load_info(filename_data=key_i_filt,
                                                                    direc=input_data_settings.gen_path,
                                                                    pulse=input_data_settings.pulse)
                            input_data_settings.add_filtered_entry(key_i, type_filter=filt_smooth_dict['type_filter'],
                                                                   dict_filter_kwargs=filt_smooth_dict[
                                                                       'dict_filter_kwargs'],
                                                                   new_load_setting_filt=new_load_setting_filtered,
                                                                   for_channels=True,
                                                                   dict_of_add_load_info=dict_of_add_load_info,
                                                                   data_type_to_index_dict=dict_to_join_type_to_index,
                                                                   dict_of_add_basic_info=dict_to_join_add_basic_info)
                            input_data_obj_list.append(deepcopy(input_data_obj))

                if smooth_bool:
                    for ch_i in range(self.channels):
                        if ch_i in store_for_these_channels:
                            if filt_bool:
                                key_i = self.key +'_channel_' + str(ch_i) +'_filtered' + input_data_settings.dict_of_add_basic_info[self.key]['filter_type_str_channel']
                                key_i_smooth = self.key + '_channel_' + str(ch_i) + '_filtered'+'_smoothed'

                            else:
                                key_i = self.key +'_channel_' + str(ch_i)
                                key_i_smooth = self.key +'_channel_' + str(ch_i) +'_smoothed'
                            new_load_setting_smoothed = LoadSettingsFilteredSmoothDict()
                            new_load_setting_smoothed.add_load_info(filename_data=key_i_smooth,
                                        direc=input_data_settings.gen_path,
                                        pulse=input_data_settings.pulse)
                            smooth_type_str =input_data_settings.dict_of_add_basic_info[self.key]['smooth_type_str_channel']
                            input_data_settings.add_smoothed_entry(
                                key_i, type_smooth=filt_smooth_dict['type_smooth'],
                                dict_smooth_kwargs=filt_smooth_dict['dict_smooth_kwargs'],
                                new_load_setting_smoothed=new_load_setting_smoothed,
                                                                   for_channels=True,
                                                                   dict_of_add_load_info=dict_of_add_load_info,
                                                                   data_type_to_index_dict=dict_to_join_type_to_index,
                                                                   dict_of_add_basic_info=dict_to_join_add_basic_info, smooth_type_str=smooth_type_str)

                            input_data_obj_list.append(deepcopy(input_data_obj))

                if not filt_bool and not smooth_bool:
                    for ch_i in range(self.channels):
                        if ch_i in store_for_these_channels:
                            key_i = self.key + '_channel_' + str(ch_i)
                            add_str = ''
                            add_str_label = ''
                            if filt_bool:
                                add_str = add_str + '_filtered'
                                add_str_label = add_str_label + ' filtered'
                            if smooth_bool:
                                add_str = add_str + '_smoothed'
                                add_str_label = add_str_label + ' smoothed'
                            key_i = key_i + add_str
                            dict_to_join_add_basic_info[key_i] = {**dict_to_join_add_basic_info[key_i], **input_data_settings.dict_of_add_basic_info[self.key]['dict_channels']}

                            dict_to_join_add_basic_info[key_i]['label'] = dict_to_join_add_basic_info[key_i]['label'] + add_str_label
                    # Add to the data list
                    for ch_i in range(self.channels):
                        if ch_i in store_for_these_channels:
                            input_data_obj_list.append(deepcopy(input_data_obj))
                    for  key, index in dict_to_join_type_to_index.items():
                        input_data_obj_list[index].key = key
                        input_data_obj_list[index].get_data_settings(key, dict_of_add_load_info, dict_to_join_add_basic_info)

                    # Load
                    for ch_i in range(self.channels):
                        if ch_i in store_for_these_channels:
                            key_i = self.key +'_channel_' + str(ch_i)
                            print(key_i)

                            my_file_path = Os_path(input_data_obj_list[dict_to_join_type_to_index[key_i]].load_obj.filename_data)
                            if my_file_path.is_file():  # read the npy file
                                input_data_obj_list[dict_to_join_type_to_index[key_i]].time, input_data_obj_list[
                                    dict_to_join_type_to_index[key_i]].data, input_data_obj_list[
                                    dict_to_join_type_to_index[key_i]].error, time_source, data_source, error_source = np.load(
                                    str(my_file_path))
                            else:
                                input_data_obj_list[dict_to_join_type_to_index[key_i]].time = \
                                input_data_obj_list[input_data_settings.data_type_to_index_dict[self.key]].time
                                input_data_obj_list[dict_to_join_type_to_index[key_i]].data = \
                                input_data_obj_list[input_data_settings.data_type_to_index_dict[self.key]].data[:, ch_i]
                                if input_data_obj_list[dict_to_join_type_to_index[key_i]].error is not None:
                                    input_data_obj_list[dict_to_join_type_to_index[key_i]].error = \
                                    input_data_obj_list[input_data_settings.data_type_to_index_dict[self.key]].error[:, ch_i]
                                else:
                                    input_data_obj_list[dict_to_join_type_to_index[key_i]].error = None
                                np.save(
                                    str(my_file_path), np.array([input_data_obj_list[dict_to_join_type_to_index[key_i]].time,
                                                                 input_data_obj_list[dict_to_join_type_to_index[key_i]].data,
                                                                 input_data_obj_list[dict_to_join_type_to_index[key_i]].error,
                                                                 input_data_obj_list[input_data_settings.data_type_to_index_dict[self.key]].time,
                                                                 input_data_obj_list[input_data_settings.data_type_to_index_dict[self.key]].data,
                                                                 input_data_obj_list[input_data_settings.data_type_to_index_dict[self.key]].error]))
                if filt_bool:
                    for ch_i in range(self.channels):
                        if ch_i in store_for_these_channels:
                            key_i_filt = self.key +'_channel_' + str(ch_i) +'_filtered' + input_data_settings.dict_of_add_basic_info[self.key]['filter_type_str_channel']
                            input_data_obj_list[dict_to_join_type_to_index[key_i_filt]].key = key_i_filt
                            input_data_obj_list[dict_to_join_type_to_index[key_i_filt]].get_data_settings(key_i_filt, dict_of_add_load_info, dict_to_join_add_basic_info)
                if smooth_bool:
                    smooth_type_str = input_data_settings.dict_of_add_basic_info[self.key]['smooth_type_str_channel']
                    for ch_i in range(self.channels):
                        if ch_i in store_for_these_channels:
                            if filt_bool:
                                key_i_smooth = self.key +'_channel_' + str(ch_i) +'_filtered' + input_data_settings.dict_of_add_basic_info[self.key]['filter_type_str_channel'] + '_smoothed' +smooth_type_str
                            else:
                                key_i_smooth = self.key +'_channel_' + str(ch_i) + '_smoothed'+smooth_type_str
                            input_data_obj_list[dict_to_join_type_to_index[key_i_smooth]].key = key_i_smooth
                            input_data_obj_list[dict_to_join_type_to_index[key_i_smooth]].get_data_settings(key_i_smooth,
                                                                                                          dict_of_add_load_info,
                                                                                                          dict_to_join_add_basic_info)
            if "z_smoothed" in self.key:
                print(dict_to_join_add_basic_info)
                print(dict_to_join_type_to_index)
                print(dict_of_add_load_info)
                print(self.key, self.channels)
                raise ValueError("ffdsf")
        return dict_to_join_add_basic_info, dict_to_join_type_to_index, dict_of_add_load_info

    def add_time_delay_vals(self, input_data_obj_list, data_type_to_index_dict):
        # Add time delay
        if self.add_time_delay:
            my_file_path = Os_path(input_data_obj_list[data_type_to_index_dict[self.key]].load_obj.filename_data)

            if my_file_path.is_file():  # read the npy file
                input_data_obj_list[data_type_to_index_dict[self.key]].time, input_data_obj_list[data_type_to_index_dict[self.key]].data, input_data_obj_list[data_type_to_index_dict[self.key]].error, time_source, data_source, error_source = np.load(str(my_file_path))
            else:
                if self.t_delay_kwargs['time_delay_key'] is None:
                    raise ValueError("can't time delay without input delay")
                if  input_data_obj_list[data_type_to_index_dict[self.t_delay_kwargs['time_delay_key']]].time is not None:
                    input_data_obj_list[data_type_to_index_dict[self.key]].time = input_data_obj_list[data_type_to_index_dict[self.key_undelayed]].time -  input_data_obj_list[data_type_to_index_dict[self.t_delay_kwargs['time_delay_key']]].time
                    input_data_obj_list[data_type_to_index_dict[self.key]].data = input_data_obj_list[data_type_to_index_dict[self.key_undelayed]].data
                    if input_data_obj_list[data_type_to_index_dict[self.key_undelayed]].error is not None:
                        input_data_obj_list[data_type_to_index_dict[self.key]].error = input_data_obj_list[data_type_to_index_dict[self.key_undelayed]].error
                    else:
                        input_data_obj_list[data_type_to_index_dict[self.key]].error = None
                else:
                    input_data_obj_list[data_type_to_index_dict[self.key]].time = None
                    input_data_obj_list[data_type_to_index_dict[self.key]].data = None
                    input_data_obj_list[data_type_to_index_dict[self.key]].error = None
                np.save(
                    str(my_file_path), np.array([input_data_obj_list[data_type_to_index_dict[self.key]].time,
                                                 input_data_obj_list[data_type_to_index_dict[self.key]].data,
                                                 input_data_obj_list[data_type_to_index_dict[self.key]].error,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_undelayed]].time,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_undelayed]].data,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_undelayed]].error]))

    def add_filtered_vals(self, input_data_obj_list, data_type_to_index_dict, filter_source_key):
        # Add filtered version

        if self.add_filtered:
            if self.add_smoothed:
                input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]] = False
            my_file_path = Os_path(input_data_obj_list[data_type_to_index_dict[self.key]].load_obj.filename_data)
            if my_file_path.is_file():  # read the npy file
                input_data_obj_list[data_type_to_index_dict[self.key]].time, input_data_obj_list[data_type_to_index_dict[self.key]].data, input_data_obj_list[data_type_to_index_dict[self.key]].error, time_source, data_source, error_source = np.load(str(my_file_path))
            else:
                if input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].time is not None:
                    print('self.key_unfiltered', self.key_unfiltered)
                    if filter_source_key is None:
                        raise ValueError("can't filter without link to filtered values")
                    if self.type_filter == 'standard':
                        print("adssdsa", input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].time, input_data_obj_list[data_type_to_index_dict[filter_source_key]].time)
                        bool_filter_array = UsefulFiltering.filter_around_values(input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].time, input_data_obj_list[data_type_to_index_dict[filter_source_key]].time, self.filter_func_kwargs['excl_before'], self.filter_func_kwargs['excl_after'], filter_check=self.filter_func_kwargs['excl_check'])
                    elif self.type_filter == 'elm_cycle':
                        bool_filter_array = UsefulFiltering.filter_around_values_cycle(input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].time, input_data_obj_list[data_type_to_index_dict[filter_source_key]].time, self.filter_func_kwargs['excl_before'], self.filter_func_kwargs['excl_after'], filter_check=self.filter_func_kwargs['excl_check'])
                    else:
                        raise ValueError("No valid filter type")
                    bool_filter_array = np.logical_not(bool_filter_array)
                    input_data_obj_list[data_type_to_index_dict[self.key]].time = \
                    input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].time[bool_filter_array]
                    input_data_obj_list[data_type_to_index_dict[self.key]].data = \
                    input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].data[bool_filter_array]
                    if input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].error is not None:
                        input_data_obj_list[data_type_to_index_dict[self.key]].error = \
                        input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].error[bool_filter_array]
                    else:
                        input_data_obj_list[data_type_to_index_dict[self.key]].error = None
                else:
                    input_data_obj_list[data_type_to_index_dict[self.key]].time = None
                    input_data_obj_list[data_type_to_index_dict[self.key]].data = None
                    input_data_obj_list[data_type_to_index_dict[self.key]].error = None
                np.save(
                    str(my_file_path), np.array([input_data_obj_list[data_type_to_index_dict[self.key]].time,
                                                 input_data_obj_list[data_type_to_index_dict[self.key]].data,
                                                 input_data_obj_list[data_type_to_index_dict[self.key]].error,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].time,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].data,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_unfiltered]].error]))

    def add_smoothed_vals(self, input_data_obj_list, data_type_to_index_dict):
        # Add_smoothed version

        if self.add_smoothed:
            my_file_path = Os_path(input_data_obj_list[data_type_to_index_dict[self.key]].load_obj.filename_data)
            if my_file_path.is_file():  # read the npy file
                input_data_obj_list[data_type_to_index_dict[self.key]].time, input_data_obj_list[
                    data_type_to_index_dict[self.key]].data, input_data_obj_list[
                    data_type_to_index_dict[self.key]].error, time_source, data_source, error_source = np.load(
                    str(my_file_path))
            else:
                if self.type_smooth == 'gaussian':
                    time_return = input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].time
                    data_return = input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].data
                    error_return = input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].error
                    if data_return is not None:
                        if np.shape(time_return)[0] != np.shape(data_return)[0]:
                            raise ValueError("Input from "+str(self.key_unsmoothed)+' for '+ str(self.key)+'' + str(np.shape(time_return))+' and ' + str(np.shape(data_return)))

                        # Use a gaussian smoothing kernel of width
                        if data_return.ndim == 2:  # 2D arrays
                            if np.shape(data_return)[0] == np.shape(time_return)[0]:
                                data_array = np.zeros((np.shape(data_return)[0], np.shape(data_return)[1]))
                                for i in range(np.shape(data_return)[1]):
                                    if np.shape(data_return)[0] > 50000:
                                        data_array[:, i] = UsefulSmoothing.gaussian_smoothing_kernel_sparse_slow(
                                            data_return[:, i], self.smooth_func_kwargs["gaussian_smooth_fwhm"], time_return, forward_only=self.smooth_func_kwargs["gaussian_smooth_forward_only"], backward_only=self.smooth_func_kwargs["gaussian_smooth_backward_only"]
                                        )
                                    else:
                                        data_array[:, i] = UsefulSmoothing.gaussian_smoothing_kernel_sparse(
                                            data_return[:, i], self.smooth_func_kwargs["gaussian_smooth_fwhm"], time_return, forward_only=self.smooth_func_kwargs["gaussian_smooth_forward_only"], backward_only=self.smooth_func_kwargs["gaussian_smooth_backward_only"]
                                        )
                                data_return = data_array
                        else:
                            if np.shape(data_return)[0] > 50000:
                                data_return = UsefulSmoothing.gaussian_smoothing_kernel_sparse_slow(
                                    data_return, self.smooth_func_kwargs["gaussian_smooth_fwhm"], time_return, forward_only=self.smooth_func_kwargs["gaussian_smooth_forward_only"], backward_only=self.smooth_func_kwargs["gaussian_smooth_backward_only"]
                                )
                            else:
                                data_return = UsefulSmoothing.gaussian_smoothing_kernel_sparse(
                                    data_return, self.smooth_func_kwargs["gaussian_smooth_fwhm"], time_return, forward_only=self.smooth_func_kwargs["gaussian_smooth_forward_only"], backward_only=self.smooth_func_kwargs["gaussian_smooth_backward_only"]
                                )

                        if error_return is not None:
                            if len(error_return) > 50000:
                                error_return = UsefulSmoothing.gaussian_smoothing_kernel_sparse_slow(
                                    error_return, self.smooth_func_kwargs["gaussian_smooth_fwhm"], time_return, forward_only=self.smooth_func_kwargs["gaussian_smooth_forward_only"], backward_only=self.smooth_func_kwargs["gaussian_smooth_backward_only"]
                                )
                            else:
                                error_return = UsefulSmoothing.gaussian_smoothing_kernel_sparse_slow(
                                    error_return, self.smooth_func_kwargs["gaussian_smooth_fwhm"], time_return, forward_only=self.smooth_func_kwargs["gaussian_smooth_forward_only"], backward_only=self.smooth_func_kwargs["gaussian_smooth_backward_only"]
                                )

                        if np.shape(time_return)[0] != np.shape(data_return)[0]:
                            raise ValueError("not saving the same shape for some reason "+ str(self.key)+'' + str(np.shape(time_return))+' and ' + str(np.shape(data_return)))

                    input_data_obj_list[data_type_to_index_dict[self.key]].time = time_return
                    input_data_obj_list[data_type_to_index_dict[self.key]].data = data_return
                    input_data_obj_list[data_type_to_index_dict[self.key]].error = error_return
                elif self.type_smooth == 'poly_smooth':
                    time_return = input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].time
                    data_return = input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].data
                    error_return = input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].error
                    if np.shape(time_return)[0] != np.shape(data_return)[0]:
                        raise ValueError("Input from " + str(self.key_unsmoothed) + ' for ' + str(self.key) + '' + str(
                            np.shape(time_return)) + ' and ' + str(np.shape(data_return)))

                    data_return = UsefulSmoothing.running_poly_smooth(
                        self.smooth_func_kwargs["new_time_range"], time_return, data_return, y_err=error_return, bandwidth=1., order=3, kernel_str='gaussian'
                    )
                    if error_return is not None:
                        error_return = UsefulSmoothing.running_poly_smooth(
                            points, x, y, y_err=None, bandwidth=1., order=3, kernel_str='gaussian'
                        )

                    if np.shape(time_return)[0] != np.shape(data_return)[0]:
                        raise ValueError("not saving the same shape for some reason " + str(self.key) + '' + str(
                            np.shape(time_return)) + ' and ' + str(np.shape(data_return)))

                    input_data_obj_list[data_type_to_index_dict[self.key]].time = time_return
                    input_data_obj_list[data_type_to_index_dict[self.key]].data = data_return
                    input_data_obj_list[data_type_to_index_dict[self.key]].error = error_return
                elif self.type_smooth == 'median':
                    time_return = input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].time
                    data_return = input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].data
                    error_return = input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].error
                    if np.shape(time_return)[0] != np.shape(data_return)[0]:
                        raise ValueError("Input from " + str(self.key_unsmoothed) + ' for ' + str(self.key) + '' + str(
                            np.shape(time_return)) + ' and ' + str(np.shape(data_return)))
                    print(self.key_unsmoothed, 'median')
                    if data_return.ndim >= 2:  # 2D arrays
                        data_return_2 = np.zeros(np.shape(data_return))
                        if np.shape(data_return)[0] == np.shape(time_return)[0]:
                            for k in range(np.shape(data_return)[1]):
                                data_return_2[:, k] = UsefulSmoothing.running_median_smooth(data_return[:, k],
                                                                                    self.smooth_func_kwargs["median_x_range"],
                                                                                    x_data=time_return, debug=False,
                                                                                    rr=self.smooth_func_kwargs["rr"],
                                                                                    lr=self.smooth_func_kwargs["lr"],
                                                                                    verbose=True, max_points_per_chunk=1000000)
                        elif np.shape(data_return)[1] == np.shape(time_return)[0]:
                            for k in range(np.shape(data_return)[0]):
                                data_return_2[:, k] = UsefulSmoothing.running_median_smooth(data_return[k, :],
                                                                                    self.smooth_func_kwargs["median_x_range"],
                                                                                    x_data=time_return, debug=False,
                                                                                    rr=self.smooth_func_kwargs["rr"],
                                                                                    lr=self.smooth_func_kwargs["lr"],
                                                                                    verbose=True, max_points_per_chunk=1000000)
                        else:
                            raise ValueError("msimatch dims")
                        data_return = data_return_2
                    else:
                        data_return = UsefulSmoothing.running_median_smooth(data_return, self.smooth_func_kwargs["median_x_range"],
                                                              x_data=time_return, debug=False,
                                                              rr=self.smooth_func_kwargs["rr"],
                                                              lr=self.smooth_func_kwargs["lr"],
                                                              verbose=True, max_points_per_chunk=1000000)
                    if error_return is not None:
                        if error_return.ndim >= 2:
                            error_return_2 = np.zeros(np.shape(error_return))
                            if np.shape(error_return)[0] == np.shape(time_return)[0]:
                                for k in range(np.shape(error_return)[1]):
                                    error_return_2[:, k] = UsefulSmoothing.running_median_smooth(error_return[:, k],
                                                                                    self.smooth_func_kwargs["median_x_range"],
                                                                                    x_data=time_return, debug=False,
                                                                                    rr=self.smooth_func_kwargs["rr"],
                                                                                    lr=self.smooth_func_kwargs["lr"],
                                                                                    verbose=True, max_points_per_chunk=1000000)
                            elif np.shape(error_return)[1] == np.shape(time_return)[0]:
                                for k in range(np.shape(error_return)[0]):
                                    error_return_2[k, :] = UsefulSmoothing.running_median_smooth(error_return[k, :],
                                                                                    self.smooth_func_kwargs["median_x_range"],
                                                                                    x_data=time_return, debug=False,
                                                                                    rr=self.smooth_func_kwargs["rr"],
                                                                                    lr=self.smooth_func_kwargs["lr"],
                                                                                    verbose=True, max_points_per_chunk=1000000)
                            else:
                                raise ValueError("msimatch dims")
                            error_return = error_return_2
                        else:
                            error_return = UsefulSmoothing.running_median_smooth(error_return,
                                                                  self.smooth_func_kwargs["median_x_range"],
                                                                  x_data=time_return, debug=False,
                                                                  rr=self.smooth_func_kwargs["rr"],
                                                                  lr=self.smooth_func_kwargs["lr"],
                                                                  verbose=True, max_points_per_chunk=1000000)

                    if np.shape(time_return)[0] != np.shape(data_return)[0]:
                        raise ValueError("not saving the same shape for some reason " + str(self.key) + '' + str(
                            np.shape(time_return)) + ' and ' + str(np.shape(data_return)))

                    input_data_obj_list[data_type_to_index_dict[self.key]].time = time_return
                    input_data_obj_list[data_type_to_index_dict[self.key]].data = data_return
                    input_data_obj_list[data_type_to_index_dict[self.key]].error = error_return
                else:
                    raise ValueError("no recognised smoothing type")
                if input_data_obj_list[data_type_to_index_dict[self.key]].data is not None:
                    input_data_obj_list[data_type_to_index_dict[self.key]].time = np.reshape(
                        input_data_obj_list[data_type_to_index_dict[self.key]].time,
                        (np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].time)[0],)
                    )
                    if data_return.ndim < 2:  # 2D arrays
                        input_data_obj_list[data_type_to_index_dict[self.key]].data = np.reshape(
                            input_data_obj_list[data_type_to_index_dict[self.key]].data,
                            (np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].data)[0],)
                        )
                    if input_data_obj_list[data_type_to_index_dict[self.key]].error is not None:
                        if error_return.ndim < 2:  # 2D arrays
                            input_data_obj_list[data_type_to_index_dict[self.key]].error = np.reshape(
                                input_data_obj_list[data_type_to_index_dict[self.key]].error,
                                (np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].error)[0], )
                            )
                print(np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].time))
                print(np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].data))
                print(np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].error))

                print(np.shape(input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].time))
                print(np.shape(input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].data))
                print(np.shape(input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].error))

                np.save(
                    str(my_file_path), np.array([input_data_obj_list[data_type_to_index_dict[self.key]].time,
                                                 input_data_obj_list[data_type_to_index_dict[self.key]].data,
                                                 input_data_obj_list[data_type_to_index_dict[self.key]].error,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].time,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].data,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_unsmoothed]].error]))

    def add_channel_split_vals_self_split(self, input_data_obj_list, data_type_to_index_dict):
        # Add_interped version
        if self.add_split_channel:
            my_file_path = Os_path(input_data_obj_list[data_type_to_index_dict[self.key]].load_obj.filename_data)
            if my_file_path.is_file():  # read the npy file
                warnings.warn('loading a value the is given a time range in the input settings')
                input_data_obj_list[data_type_to_index_dict[self.key]].time, input_data_obj_list[
                    data_type_to_index_dict[self.key]].data, input_data_obj_list[
                    data_type_to_index_dict[self.key]].error, time_source, data_source, error_source = np.load(
                    str(my_file_path))
            else:
                time_old = input_data_obj_list[data_type_to_index_dict[self.key_source]].time
                data_old = input_data_obj_list[data_type_to_index_dict[self.key_source]].data
                error_old = input_data_obj_list[data_type_to_index_dict[self.key_source]].error

                if data_old is not None:
                    if self.bound_type_data_or_time == 'data':
                        where_data = data_old
                    else:
                        where_data = time_old
                    if self.list_of_bounds[0] is not None:
                        bool_before = self.list_of_bounds[0] < where_data
                        use_before = True
                    if self.list_of_bounds[1] is not None:
                        bool_after = self.list_of_bounds[1] > where_data
                        use_aft = True
                    if use_before and use_aft:
                        bool_tot = np.logical_and(bool_before, bool_after)
                    elif use_before:
                        bool_tot = bool_before
                    elif use_aft:
                        bool_tot = bool_after
                    else:
                        bool_tot = None
                    if bool_tot is not None:
                        where_bounds = np.where(bool_tot)
                    else:
                        where_bounds = np.full(np.shape(time_old), True)
                    time_return = time_old[where_bounds]
                    data_return = data_old[where_bounds]
                    if error_old is not None:
                        error_return = error_old[where_bounds]
                    else:
                        error_return = None
                else:
                    time_return = None
                    data_return = None
                    error_return = None
                input_data_obj_list[data_type_to_index_dict[self.key]].time = time_return
                input_data_obj_list[data_type_to_index_dict[self.key]].data = data_return
                input_data_obj_list[data_type_to_index_dict[self.key]].error = error_return

                if data_old is not None:
                    input_data_obj_list[data_type_to_index_dict[self.key]].time = np.reshape(
                        input_data_obj_list[data_type_to_index_dict[self.key]].time,
                        (np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].time)[0],)
                    )
                    input_data_obj_list[data_type_to_index_dict[self.key]].data = np.reshape(
                        input_data_obj_list[data_type_to_index_dict[self.key]].data,
                        (np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].data)[0],)
                    )
                    if error_old is not None:
                        input_data_obj_list[data_type_to_index_dict[self.key]].error = np.reshape(
                            input_data_obj_list[data_type_to_index_dict[self.key]].error,
                            (np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].error)[0], )
                        )

                np.save(
                    str(my_file_path), np.array([input_data_obj_list[data_type_to_index_dict[self.key]].time,
                                                 input_data_obj_list[data_type_to_index_dict[self.key]].data,
                                                 input_data_obj_list[data_type_to_index_dict[self.key]].error,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_source]].time,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_source]].data,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_source]].error]))
    def add_interped_vals(self, input_data_obj_list, data_type_to_index_dict):
        # Add_interped version

        if self.add_interped:
            my_file_path = Os_path(input_data_obj_list[data_type_to_index_dict[self.key]].load_obj.filename_data)
            if my_file_path.is_file():  # read the npy file
                input_data_obj_list[data_type_to_index_dict[self.key]].time, input_data_obj_list[
                    data_type_to_index_dict[self.key]].data, input_data_obj_list[
                    data_type_to_index_dict[self.key]].error, time_source, data_source, error_source = np.load(
                    str(my_file_path))
            else:
                time_old = input_data_obj_list[data_type_to_index_dict[self.key_interped]].time
                data_old = input_data_obj_list[data_type_to_index_dict[self.key_interped]].data
                error_old = input_data_obj_list[data_type_to_index_dict[self.key_interped]].error
                if time_old is not None and data_old is not None:
                    print(np.shape(time_old), np.shape(data_old), self.key_interped)
                    interp_obj_data = interp1d(time_old, data_old)
                    time_return = input_data_obj_list[data_type_to_index_dict[self.key_interpolated_on]].time
                    print(time_return, time_old, self.key_interpolated_on, self.key)
                    if time_return is None:
                        warnings.warn("cant interp, there is no time data to interp onto")
                        time_return = None
                        data_return = None
                        error_return = None
                    else:
                        where_in_interp_range = np.where(np.logical_and(time_return > np.nanmin(time_old), time_return < np.nanmax(time_old)))
                        data_return = np.full(np.shape(time_return), np.nan)
                        data_return_where = interp_obj_data(time_return[where_in_interp_range])
                        data_return[where_in_interp_range] = data_return_where
                        if error_old is not None:
                            error_return = np.full(np.shape(time_return), np.nan)
                            interp_obj_error = interp1d(time_old, error_old)
                            error_return_where = interp_obj_error(time_return[where_in_interp_range])
                            error_return[where_in_interp_range] = error_return_where
                        else:
                            error_return = None
                    input_data_obj_list[data_type_to_index_dict[self.key]].time = time_return

                    input_data_obj_list[data_type_to_index_dict[self.key]].time = time_return
                    input_data_obj_list[data_type_to_index_dict[self.key]].data = data_return
                    input_data_obj_list[data_type_to_index_dict[self.key]].error = error_return
                    if time_return is not None:
                        input_data_obj_list[data_type_to_index_dict[self.key]].time = np.reshape(
                            input_data_obj_list[data_type_to_index_dict[self.key]].time,
                            (np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].time)[0],)
                        )
                    if data_return is not None:
                        input_data_obj_list[data_type_to_index_dict[self.key]].data = np.reshape(
                            input_data_obj_list[data_type_to_index_dict[self.key]].data,
                            (np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].data)[0],)
                        )
                    if error_old is not None:
                        if error_return is not None:
                            input_data_obj_list[data_type_to_index_dict[self.key]].error = np.reshape(
                                input_data_obj_list[data_type_to_index_dict[self.key]].error,
                                (np.shape(input_data_obj_list[data_type_to_index_dict[self.key]].error)[0], )
                            )
                else:
                    input_data_obj_list[data_type_to_index_dict[self.key_interped]].time = None
                    input_data_obj_list[data_type_to_index_dict[self.key_interped]].data = None
                    input_data_obj_list[data_type_to_index_dict[self.key_interped]].error = None
                np.save(
                    str(my_file_path), np.array([input_data_obj_list[data_type_to_index_dict[self.key]].time,
                                                 input_data_obj_list[data_type_to_index_dict[self.key]].data,
                                                 input_data_obj_list[data_type_to_index_dict[self.key]].error,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_interped]].time,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_interped]].data,
                                                 input_data_obj_list[data_type_to_index_dict[self.key_interped]].error]))

    def add_basic_info(self, key, label, units):
        self.is_used = True
        self.key = key
        self.label = label
        self.units = units

    def add_new_graph(self, list_of_plots_used):  # Add overwriting parts
        self.list_of_plots_used = list_of_plots_used


class InputDataSettings(object):
    """
    storing a dictionary of data settings that can be added to later
    """

    def __init__(self):
        self.default_required_class_to_load = 'ClassAllDataObj'
        self.figure_title = 'Template'
        self.filter_source_key = None
        self.filter_around_source = None
        self.save_settings = SaveSettings()
        self.current_index = 0
        self.current_index_plot = 0
        self.current_index_set_type = 0
        self.data_type_to_index_dict = {}
        self.plot_to_index_dict = {}
        self.dict_of_data_choices = {}
        self.dict_of_plot_settings = {}
        self.dict_save_single_index = {}
        parts = ['cache_stored_tests']
        self.cache_path = Os_path(self.save_settings.gen_path).joinpath(*parts)
        self.cache_path = str(self.cache_path)
        self.dict_of_add_basic_info = {}
        self.dict_of_add_load_info = {}

        self.dict_of_sets_to_index_sets = {}

        #####################
        # Make sets of similar data
        self.initial_fill_dict_of_sets_to_index_sets('r_type')
        self.initial_fill_dict_of_sets_to_index_sets('te_type')

        ###
        # Put sets in a list
        set_list = [SetSimilarData() for _ in range(self.current_index_set_type)]

        #######################
        self.fill_dict_of_add_basic_info('test_x', " a new test for x", "m", gen_label='x')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data", "error", "time"]], [["analysis_obj"], ["x", "x_err", "time"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_x', new_load_setting)
        self.fill_add_new_graph('test_x', "xyz_plot", "x")

        self.fill_add_new_graph('test_x', "xyz_plot_overplot", "x")
        self.fill_add_new_graph('test_x', "xyz_plot_overplot", "x")

        ################################
        self.fill_dict_of_add_basic_info('test_x1', " a new test for x1", "m", gen_label='x')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data", "time"]], [["analysis_obj"], ["x1", "time"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_x1', new_load_setting)
        #################################
        self.fill_dict_of_add_basic_info('test_y', " a new test for y", "ms", gen_label='y')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data", "time"]], [["analysis_obj"], ["y", "time"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_y', new_load_setting)
        self.fill_add_new_graph('test_y', "xyz_plot", "y")
        self.fill_add_new_graph('test_y', "xyz_plot", "y")
        self.fill_add_new_graph('test_y', "xyz_plot_overplot", "y")
        self.fill_add_new_graph('test_y', "xyz_plot_overplot", "y")

        ##################################
        self.fill_dict_of_add_basic_info('test_z', " a new test for z", "mss", gen_label='z')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data", "time"]], [["analysis_obj"], ["z", "time"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_z', new_load_setting)
        ##################################
        self.fill_dict_of_add_basic_info('time_bound', "Time bounds", "s", gen_label='z')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data", "time"]], [["analysis_obj"], ["t_bounds", "t_bounds"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('time_bound', new_load_setting)


        ##################################
        self.fill_dict_of_add_basic_info('test_r', " a new test for r", "m", gen_label='r')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data", "time"]], [["analysis_obj"], ["r", "time"]])
        new_load_setting.add_load_info(point_data_obj)
        new_load_setting.add_to_set(x_set='r_type')
        self.fill_dict_of_add_load_info('test_r', new_load_setting)
        self.fill_add_new_graph('test_r', "2d_test_plot", "x", link_to_z_key='test_n', link_to_y_key='test_te', set_num=0)

        #################################
        self.fill_dict_of_add_basic_info('test_te', " a new test for te", "eV", gen_label='Te')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data", "time"]], [["analysis_obj"], ["te", "time"]])
        new_load_setting.add_load_info(point_data_obj)
        new_load_setting.add_to_set(x_set='te_type')
        self.fill_dict_of_add_load_info('test_te', new_load_setting)
        self.fill_add_new_graph('test_te', "2d_test_plot", "y", link_to_x_key='test_r', link_to_z_key='test_n', set_num=0)

        #################################
        self.fill_dict_of_add_basic_info('test_te1', " a new test for te1", "eV", gen_label='Te')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data", "time"]], [["analysis_obj"], ["te1", "time"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_te1', new_load_setting)
        self.fill_add_new_graph('test_te1', "2d_test_plot", "y", link_to_x_key='test_r1', set_num=1)

        ##################################
        self.fill_dict_of_add_basic_info('test_r1', " a new test for r1", "m", gen_label='r')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data", "time"]], [["analysis_obj"], ["r1", "time"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_r1', new_load_setting)
        self.fill_add_new_graph('test_r1', "2d_test_plot", "x", link_to_y_key='test_te1', set_num=1)


        ##################################
        self.fill_dict_of_add_basic_info('test_n', " a new test for n", "$m^{-3}$", gen_label='n')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data", "time"]], [["analysis_obj"], ["n", "time"]])
        new_load_setting.add_load_info(point_data_obj)
        new_load_setting.add_to_set(x_set='r_type', y_set='te_type')  # If you have 2d data then this and the values you plot against mustbe in sets to match the ax
        self.fill_dict_of_add_load_info('test_n', new_load_setting)
        self.fill_add_new_graph('test_n', "2d_test_plot", "z", link_to_x_key='test_r', link_to_y_key='test_te', set_num=0)
        ##################################
        self.fill_dict_of_add_basic_info('loaded_x', " loaded x", "$m$", gen_label='x')
        new_load_setting = LoadSettingsFile()
        new_load_setting.add_load_info('x_delete.npy', self.save_settings.gen_path, filename_time='time_delete.npy')
        self.fill_dict_of_add_load_info('loaded_x', new_load_setting)


        self.fill_add_new_graph('test_x', "xyz_plot_2", "x", set_num=0, overplot_link_num=0)  # This adds the time
        self.fill_add_new_graph('test_x', "xyz_plot_2", "y", set_num=0, overplot_link_num=0)
        self.fill_add_new_graph('test_x1', "xyz_plot_2", "x", set_num=1, overplot_link_num=0)  # This adds the time
        self.fill_add_new_graph('test_x1', "xyz_plot_2", "y", set_num=1, overplot_link_num=0)
        # self.fill_add_new_graph('test_x1', "xyz_plot_2", "x", set_num=1, overplot_link_num=1)  # This adds the time
        # self.fill_add_new_graph('test_x1', "xyz_plot_2", "y", set_num=1, overplot_link_num=1)
        # self.fill_add_new_graph('test_y', "xyz_plot_2", "x", set_num=0)
        # self.fill_add_new_graph('test_y', "xyz_plot_2", "y", set_num=0)

        # self.fill_add_new_graph('time_bound', "xyz_plot_2", "x", set_num=2, overplot_link_num=0, h_plot_axspan=True, alpha=0.2, important_plot=False)
        # self.fill_add_new_graph('time_bound', "xyz_plot_2", "y", set_num=2, overplot_link_num=0, h_plot_axspan=True, alpha=0.2, important_plot=False)
        # self.fill_add_new_graph('loaded_x', "xyz_plot_2", "x", set_num=0)
        # self.fill_add_new_graph('loaded_x', "xyz_plot_2", "y", set_num=0)
        # self.fill_add_new_graph('loaded_x', "xyz_plot_2", "x", set_num=3, h_plot_axline=True, important_plot=False)
        # self.fill_add_new_graph('loaded_x', "xyz_plot_2", "y", set_num=3, h_plot_axline=True, important_plot=False)


        self.fill_plot_to_index_dict('xyz_plot', x_type=["time"], y_type=["data"], z_type=[None], plot_on=False, sorting_type_xyz_linking=['index_link'], sorting_type_ax_index=['appending'], type_plot=['line_plot'], n_rows_cols_ax=[2, None])
        self.fill_plot_to_index_dict('xyz_plot_2', x_type=["time", "time"], y_type=["data", "data"], z_type=[None, None], plot_on=False, sorting_type_xyz_linking=['index_link'], sorting_type_ax_index=['appending'], type_plot=['line_plot', 'line_plot'], n_rows_cols_ax=[2, None], sets_used=[0, 1], set_caption_abc=True, share_ax_x=True, index_stack_direction='vertical')
        # self.fill_plot_to_index_dict('xyz_plot_2', x_type=["time"], y_type=["data"], z_type=[None], plot_on=True, sorting_type_xyz_linking=['index_link'], sorting_type_ax_index=['appending'], type_plot=['line_plot', 'line_plot', 'axspan_plot', 'line_plot', 'axline_plot'], n_rows_cols_ax=[2, None], sets_used=[0, 1, 2, 3, 4], set_caption_abc=True, share_ax_x=True)
        self.fill_plot_to_index_dict('xyz_plot_overplot', x_type=["time"], y_type=["data"], z_type=[None], plot_on=False, sorting_type_xyz_linking=['index_link'], sorting_type_ax_index=['appending'], type_plot=['line_plot'], n_rows_cols_ax=[None, 2])
        self.fill_plot_to_index_dict('2d_test_plot', x_type=["data"], y_type=["data"], z_type=["data"], plot_on=False, sorting_type_xyz_linking=['all_comb'], sorting_type_ax_index=['appending'], type_plot=['2d_colour_plot', 'line_plot'], n_rows_cols_ax=[None, 2], sets_used=[0, 1])


        ##################################
        self.fill_dict_of_add_basic_info('test_label_x', " test label x", "", gen_label='test label x')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data"]], [["analysis_obj"], ["test_label_x"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_label_x', new_load_setting)
        ##################################
        self.fill_dict_of_add_basic_info('test_label_y', " test label y", "", gen_label='test label y')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data"]], [["analysis_obj"], ["test_label_y"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_label_y', new_load_setting)
        ##################################
        self.fill_dict_of_add_basic_info('test_label_z', " test label z", "", gen_label='test label z')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data"]], [["analysis_obj"], ["test_label_z"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_label_z', new_load_setting)
        ##################################
        self.fill_dict_of_add_basic_info('test_label_x2', " test label x", "", gen_label='test label x')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data"]], [["analysis_obj"], ["test_label_x2"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_label_x2', new_load_setting)
        ##################################
        self.fill_dict_of_add_basic_info('test_label_y2', " test label y", "", gen_label='test label y')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data"]], [["analysis_obj"], ["test_label_y2"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_label_y2', new_load_setting)
        ##################################
        self.fill_dict_of_add_basic_info('test_label_z2', " test label z", "", gen_label='test label z')
        new_load_setting = LoadSettingsTestDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
        point_data_obj = PointToObject([["obj"], ["data"]], [["analysis_obj"], ["test_label_z2"]])
        new_load_setting.add_load_info(point_data_obj)
        self.fill_dict_of_add_load_info('test_label_z2', new_load_setting)

        self.fill_add_new_graph('test_x', "test_text", "x", set_num=0, overplot_link_num=0)
        self.fill_add_new_graph('test_x1', "test_text", "y", set_num=0, overplot_link_num=0)
        self.fill_add_new_graph('test_label_x', "test_text", "x", set_num=1, overplot_link_num=0)
        self.fill_add_new_graph('test_label_y', "test_text", "y", set_num=1, overplot_link_num=0)
        self.fill_add_new_graph('test_label_z', "test_text", "z", set_num=1, overplot_link_num=0)

        self.fill_add_new_graph('test_label_x2', "test_text", "x", set_num=1, overplot_link_num=0)
        self.fill_add_new_graph('test_label_y2', "test_text", "y", set_num=1, overplot_link_num=0)
        self.fill_add_new_graph('test_label_z2', "test_text", "z", set_num=1, overplot_link_num=0)
        self.fill_add_new_graph('test_x', "test_text", "x", set_num=2, overplot_link_num=0)
        self.fill_add_new_graph('test_x1', "test_text", "y", set_num=2, overplot_link_num=0)
        self.fill_add_new_graph('test_label_x', "test_text", "x", set_num=3, overplot_link_num=0)
        self.fill_add_new_graph('test_label_y', "test_text", "y", set_num=3, overplot_link_num=0)
        self.fill_add_new_graph('test_label_z', "test_text", "z", set_num=3, overplot_link_num=0)

        self.fill_add_new_graph('test_label_x2', "test_text", "x", set_num=3, overplot_link_num=0)
        self.fill_add_new_graph('test_label_y2', "test_text", "y", set_num=3, overplot_link_num=0)
        self.fill_add_new_graph('test_label_z2', "test_text", "z", set_num=3, overplot_link_num=0)

        self.fill_plot_to_index_dict('test_text', x_type=["data", "data", "data", "data"], y_type=["data", "data", "data", "data"], z_type=[None, "data", None, "data"], plot_on=True, sorting_type_xyz_linking=['index_link'], sorting_type_ax_index=['appending'], type_plot=['line_plot', 'add_text', 'line_plot', 'add_text'], n_rows_cols_ax=[None, 2], sets_used=[0, 1, 2, 3])


        n_signals_added = 0
        for key, index in self.data_type_to_index_dict.items():
            if index > n_signals_added:
                n_signals_added = index
        self.n_signals_added = n_signals_added + 1

        n_plots_added = 0
        for key, index in self.plot_to_index_dict.items():

            if index > n_plots_added:
                n_plots_added = index
        self.n_plots_added = n_plots_added + 1

    def add_filtered_channel(self, key, type_filter, dict_filter_kwargs={}):
        self.dict_of_add_basic_info[key]['dict_channels_filter_smooth_settings']['type_filter'] = type_filter
        self.dict_of_add_basic_info[key]['dict_channels_filter_smooth_settings']['dict_filter_kwargs'] = dict_filter_kwargs
        self.dict_of_add_basic_info[key]['dict_channels_filter_smooth_settings']['filter_bool'] = True
        self.dict_of_add_basic_info[key]['filter_type_str_channel'] = dict_filter_kwargs['f_str_type']

    def add_smoothed_channel(self, key, type_smooth='gaussian', dict_smooth_kwargs={}, smooth_type_str=None):
        self.dict_of_add_basic_info[key]['dict_channels_filter_smooth_settings']['type_smooth'] = type_smooth
        self.dict_of_add_basic_info[key]['dict_channels_filter_smooth_settings']['dict_smooth_kwargs'] = dict_smooth_kwargs
        self.dict_of_add_basic_info[key]['dict_channels_filter_smooth_settings']['smooth_bool'] = True
        self.dict_of_add_basic_info[key]['smooth_type_str_channel'] = smooth_type_str

    def add_time_delay_entry(self, key, dict_delay_kwargs={}, new_load_setting_time_delay=None, for_channels=False, dict_of_add_load_info=None,
                                                               data_type_to_index_dict=None, dict_of_add_basic_info=None, required_class_to_load=None):
        # default, updated in add_filtered_entry or add_smoothed_entry
        if for_channels:
            dict_of_add_load_info_move_to = dict_of_add_load_info
            data_type_to_index_dict_move_to = data_type_to_index_dict
            dict_of_add_basic_info_move_to = dict_of_add_basic_info
        else:
            dict_of_add_load_info_move_to = self.dict_of_add_load_info
            data_type_to_index_dict_move_to = self.data_type_to_index_dict
            dict_of_add_basic_info_move_to = self.dict_of_add_basic_info
        dict_of_add_load_info_move_to[key + '_time_delay'] = deepcopy(dict_of_add_load_info_move_to[key])
        data_type_to_index_dict_move_to[key + '_time_delay'] = self.current_index
        self.current_index = self.current_index + 1
        dict_of_add_basic_info_move_to[key + '_time_delay'] = deepcopy(dict_of_add_basic_info_move_to[key])

        self.fill_dict_of_add_load_info(key + '_time_delay', new_load_setting_time_delay,
                                        dict_of_add_load_info=dict_of_add_load_info_move_to)
        dict_of_add_load_info_move_to[key + '_time_delay']['t_delay_kwargs'] = dict_delay_kwargs
        dict_of_add_load_info_move_to[key + '_time_delay']['add_time_delay'] = True  # must be after fill_dict_of_add_load_info
        dict_of_add_load_info_move_to[key + '_time_delay']['key_undelayed'] = key
        if required_class_to_load is not None:
            dict_of_add_basic_info_move_to[key + '_time_delay']["required_class_to_load"] = required_class_to_load

    def add_interped_entry(self, key, interp_key, dict_filter_kwargs={}, new_load_setting_interped=None, for_channels=False, dict_of_add_load_info=None,
                                                               data_type_to_index_dict=None, dict_of_add_basic_info=None, required_class_to_load=None):
        # default, updated in add_filtered_entry or add_smoothed_entry
        if for_channels:
            dict_of_add_load_info_move_to = dict_of_add_load_info
            data_type_to_index_dict_move_to = data_type_to_index_dict
            dict_of_add_basic_info_move_to = dict_of_add_basic_info
        else:
            dict_of_add_load_info_move_to = self.dict_of_add_load_info
            data_type_to_index_dict_move_to = self.data_type_to_index_dict
            dict_of_add_basic_info_move_to = self.dict_of_add_basic_info
        dict_of_add_load_info_move_to[key + '_interped_' + interp_key] = deepcopy(dict_of_add_load_info_move_to[key])
        data_type_to_index_dict_move_to[key + '_interped_' + interp_key] = self.current_index
        self.current_index = self.current_index + 1
        dict_of_add_basic_info_move_to[key + '_interped_' + interp_key] = deepcopy(dict_of_add_basic_info_move_to[key])
        if required_class_to_load is not None:
            dict_of_add_basic_info_move_to[key + '_interped_' + interp_key]["required_class_to_load"] = required_class_to_load
        # dict_of_add_basic_info_move_to[key + '_filtered']['label'] = dict_of_add_basic_info_move_to[key + str_smoothed]['label'] + ' interped ' + dict_of_add_basic_info_move_to[key]['label']
        new_load_setting_interped.filename_data = new_load_setting_interped.filename_data + '_interped_' + interp_key + '.npy'
        self.fill_dict_of_add_load_info(key + '_interped_' + interp_key, new_load_setting_interped, dict_of_add_load_info=dict_of_add_load_info_move_to)
        dict_of_add_load_info_move_to[key + '_interped_' + interp_key]['add_interped'] = True
        dict_of_add_load_info_move_to[key + '_interped_' + interp_key]['key_interped'] = key
        dict_of_add_load_info_move_to[key + '_interped_' + interp_key]['key_interpolated_on'] = interp_key

    def add_filtered_entry(self, key, type_filter, dict_filter_kwargs={}, new_load_setting_filt=None, for_channels=False, dict_of_add_load_info=None,
                                                               data_type_to_index_dict=None, dict_of_add_basic_info=None, required_class_to_load=None):
        # default, updated in add_filtered_entry or add_smoothed_entry
        str_filt_add = '_filtered' + dict_filter_kwargs['f_str_type']

        if for_channels:
            dict_of_add_load_info_move_to = dict_of_add_load_info
            data_type_to_index_dict_move_to = data_type_to_index_dict
            dict_of_add_basic_info_move_to = dict_of_add_basic_info
        else:
            dict_of_add_load_info_move_to = self.dict_of_add_load_info
            data_type_to_index_dict_move_to = self.data_type_to_index_dict
            dict_of_add_basic_info_move_to = self.dict_of_add_basic_info
        dict_of_add_load_info_move_to[key + str_filt_add] = deepcopy(dict_of_add_load_info_move_to[key])
        data_type_to_index_dict_move_to[key + str_filt_add] = self.current_index
        self.current_index = self.current_index + 1
        dict_of_add_basic_info_move_to[key + str_filt_add] = deepcopy(dict_of_add_basic_info_move_to[key])
        # dict_of_add_basic_info_move_to[key + '_filtered']['label'] = dict_of_add_basic_info_move_to[key + '_filtered']['label'] + ' filtered '
        self.fill_dict_of_add_load_info(key + str_filt_add, new_load_setting_filt, dict_of_add_load_info=dict_of_add_load_info_move_to)
        dict_of_add_load_info_move_to[key + str_filt_add]['add_filtered'] = True
        dict_of_add_load_info_move_to[key + str_filt_add]['key_unfiltered'] = key
        if required_class_to_load is not None:
            dict_of_add_basic_info_move_to[key + str_filt_add]["required_class_to_load"] = required_class_to_load
        # self.dict_of_add_load_info[key]['add_filtered'] = False
        dict_of_add_load_info_move_to[key + str_filt_add]['type_filter'] = type_filter
        dict_of_add_load_info_move_to[key + str_filt_add]['filter_func_kwargs'] = dict_filter_kwargs
        # if key == 'interferometer_data_outboard':
        #     print(dict_of_add_basic_info_move_to[key + str_filt_add]['required_class_to_load'])
        #     quit()
    def add_smoothed_entry(self, key, type_smooth='gaussian', dict_smooth_kwargs={}, new_load_setting_smoothed=None,
                           for_channels=False, dict_of_add_load_info=None,
                                                               data_type_to_index_dict=None, dict_of_add_basic_info=None, required_class_to_load=None, forward_direction=False, backward_direction=False, smooth_type_str=None):
        if smooth_type_str is None:
            raise ValueError("smooth_type_str ,must be specified as _smtype_0_ with other number")
        if required_class_to_load is None:
            required_class_to_load = self.default_required_class_to_load
        if for_channels:
            dict_of_add_load_info_move_to = dict_of_add_load_info
            data_type_to_index_dict_move_to = data_type_to_index_dict
            dict_of_add_basic_info_move_to = dict_of_add_basic_info
        else:
            dict_of_add_load_info_move_to = self.dict_of_add_load_info
            data_type_to_index_dict_move_to = self.data_type_to_index_dict
            dict_of_add_basic_info_move_to = self.dict_of_add_basic_info
        print(key, dict_of_add_load_info)
        print(key, dict_of_add_load_info)
        str_smoothed = ''
        str_smoothed_label = ''
        if type_smooth!='gaussian':
            str_smoothed = str_smoothed+'_median'
            str_smoothed_label = str_smoothed_label +' median '
        if forward_direction:
            str_smoothed = str_smoothed+'_smoothed_forward'
            str_smoothed_label = str_smoothed_label +' smoothed forward '
        elif backward_direction:
            str_smoothed = str_smoothed+'_smoothed_backward'
            str_smoothed_label = str_smoothed_label +' smoothed backward '
        else:
            str_smoothed = str_smoothed+'_smoothed'
            str_smoothed_label = str_smoothed_label +' smoothed '
        str_smoothed = str_smoothed + smooth_type_str
        dict_of_add_load_info_move_to[key + str_smoothed] = deepcopy(dict_of_add_load_info_move_to[key])
        data_type_to_index_dict_move_to[key + str_smoothed] = self.current_index
        self.current_index = self.current_index + 1

        dict_of_add_basic_info_move_to[key + str_smoothed] = deepcopy(dict_of_add_basic_info_move_to[key])
        dict_of_add_basic_info_move_to[key + str_smoothed]['label'] = dict_of_add_basic_info_move_to[key + str_smoothed]['label'] + str_smoothed_label
        self.fill_dict_of_add_load_info(key + str_smoothed, new_load_setting_smoothed, dict_of_add_load_info=dict_of_add_load_info_move_to)
        dict_of_add_load_info_move_to[key +str_smoothed]['add_filtered'] = False
        dict_of_add_load_info_move_to[key +str_smoothed]['key_unsmoothed'] = key
        dict_of_add_load_info_move_to[key +str_smoothed]['add_smoothed'] = True
        dict_of_add_load_info_move_to[key +str_smoothed]['type_smooth'] = type_smooth
        dict_of_add_load_info_move_to[key +str_smoothed]['smooth_func_kwargs'] = dict_smooth_kwargs

    def add_split_as_channel_entry(self, key_from, list_of_bounds, type_bounds, dict_args_for_split_channel, bound_type_data_or_time, required_class_to_load=None, string_type=None):
        if string_type is None:
            raise ValueError("string_type ,must be specified as _type_0_ with other number")
        if required_class_to_load is None:
            required_class_to_load = self.default_required_class_to_load
        dict_args_for_split_channel = deepcopy(dict_args_for_split_channel)
        bound_type_data_or_time_options = ['time', 'data']
        if bound_type_data_or_time not in bound_type_data_or_time_options:
            raise ValueError("bound_type_data_or_time must be in bound_type_data_or_time_options")

        if len(list_of_bounds) != len(type_bounds):
            raise ValueError("list_of_bounds and type_bounds must be same length")
        type_bounds_options = ['and', 'or']
        for i in range(len(list_of_bounds)):
            if type(list_of_bounds[i]) != list:
                raise ValueError("list_of_bounds must be a list of lists")
            if type(type_bounds) != list:
                raise ValueError("type_bounds must be a list")
            if np.logical_xor(np.logical_and(list_of_bounds[i][0] is None, list_of_bounds[i][1] is None), type_bounds[i] is None):
                print(type_bounds[i])
                print(list_of_bounds[i])
                raise ValueError("settings must be defined for list_of_bounds if type_bounds is defined or vice versa")
            if type_bounds[i] is not None:
                if type_bounds[i] not in type_bounds_options:
                    raise ValueError("type_bounds " + str(
                        type_bounds[i]) + " val not in type_bounds_options" + str(
                        type_bounds_options))
        for i in range(len(list_of_bounds)):

            new_key = key_from + string_type + str(i)
            # print('new_key', new_key)
            # if key_from=='pin_reconstr_imp':#'interferometer_data_outboard_filtered_smoothed_interped':
            #     print(new_key)
            #     quit()
            if bound_type_data_or_time_options == 'data':
                label_unit = self.dict_of_add_basic_info[key_from]['units']
            else:
                label_unit = 's'
            new_label = self.dict_of_add_basic_info[key_from]['label']+' between '+str(list_of_bounds[i][0]) +\
                        ' and '+str(list_of_bounds[i][1])+' '+label_unit
            dict_args_for_split_channel['filename_data'] = new_key
            self.fill_dict_of_add_basic_info(
                key=new_key, label=new_label, units=self.dict_of_add_basic_info[key_from]['units'],
                gen_label=self.dict_of_add_basic_info[key_from]['gen_label'], required_class_to_load=required_class_to_load
            )

            new_load_setting = LoadSettingsChannelDict()  # new_load_setting = LoadSettingsBasic(uid, dda, data_type_str)
            new_load_setting.add_load_info(**dict_args_for_split_channel)
            self.fill_dict_of_add_load_info(new_key, new_load_setting)
            self.dict_of_add_load_info[new_key]['add_split_channel'] = True
            self.dict_of_add_load_info[new_key]['key_source'] = key_from
            self.dict_of_add_load_info[new_key]['list_of_bounds'] = list_of_bounds[i]
            self.dict_of_add_load_info[new_key]['bound_type_data_or_time'] = bound_type_data_or_time # 'data' or 'time'

    def fill_add_new_graph(self, key, list_of_plots_used, xyz='x', link_to_x_key=None, link_to_y_key=None,
                           link_to_z_key=None, set_num=0, overplot_link_num=None, marker=None, linestyle=None,
                           alpha=None, color=None, cmap=None, norm=None, h_plot_axspan=None, legend_as_x=False,
                           legend_as_y=False, h_plot_axline=None, important_plot=True, store_for_these_channels=None,
                           graph_channel_filtered=False, graph_channel_smoothed=False, overwrite_use_si_units=False,
                           min_max_value_list=[[None, None]], min_max_data_type_list=[None], save_this=False, save_this_path=None,
                           ignore_for_ax_range_setting=False, min_max_bool_type_list=[None], arrow_width=0.001,
                           circle_radius=5., fontsize=None, take_colour_from_set=None, equal_xy=False,
                           key_bounds=[None, None], error_on=True, thin_error_bar_num=1, linewidth=1.,
                           add_onto_twin_ax=False, leg_loc=None, overwrite_legend_label=None, hatch=None):

        if type(key_bounds) != list:
            raise ValueError("key_bounds must be list")
        if len(key_bounds) != 2:
            raise ValueError("key_bounds must be length 2")
        if len(min_max_value_list) != len(min_max_bool_type_list):
            raise ValueError("min_max_value_list and min_max_bool_type_list must be same length")
        if len(min_max_value_list) != len(min_max_data_type_list):
            raise ValueError("min_max_value_list and min_max_bool_type_list must be same length")
        min_max_bool_type_list_options = ['and', 'or']
        min_max_data_type_list_options = ['data', 'time', 'error']
        for i in range(len(min_max_value_list)):
            if type(min_max_value_list[i]) != list:
                raise ValueError("min_max_value_list must be a list of lists")
            if type(min_max_bool_type_list) != list:
                raise ValueError("min_max_bool_type_list must be a list")
            if np.logical_xor(np.logical_and(min_max_value_list[i][0] is None, min_max_value_list[i][1] is None), min_max_bool_type_list[i] is None):
                raise ValueError("settings must be defined for min_max_value_list if min_max_bool_type_list is defined or vice versa")
            if min_max_bool_type_list[i] is not None:
                if min_max_bool_type_list[i] not in min_max_bool_type_list_options:
                    raise ValueError("min_max_bool_type_list "+str(min_max_bool_type_list[i])+" val not in min_max_bool_type_list_options"+str(min_max_bool_type_list_options))
        for i in range(len(min_max_data_type_list)):
            if min_max_data_type_list[i] is not None:
                if min_max_data_type_list[i] not in min_max_data_type_list_options:
                    raise ValueError("min_max_data_type_list "+str(min_max_bool_type_list[i])+" val not in min_max_data_type_list_options"+str(min_max_data_type_list_options))

        if save_this:
            if save_this_path is None:
                raise ValueError("can't save with no path to save selected")
        if store_for_these_channels is not None:
            dict_of_add_basic_info = self.dict_of_add_basic_info[key]['dict_channels']
            dict_of_add_basic_info['store_for_these_channels'] = store_for_these_channels
            dict_of_add_basic_info['graph_channel_filtered'] = graph_channel_filtered
            dict_of_add_basic_info['graph_channel_smoothed'] = graph_channel_smoothed
        else:
            dict_of_add_basic_info = self.dict_of_add_basic_info[key]
        dict_of_add_basic_info['list_of_plots_used'].append(list_of_plots_used)
        dict_of_add_basic_info['xyz_list'].append(xyz)
        dict_of_add_basic_info['link_to_x_key'].append(link_to_x_key)
        dict_of_add_basic_info['link_to_y_key'].append(link_to_y_key)
        dict_of_add_basic_info['link_to_z_key'].append(link_to_z_key)
        dict_of_add_basic_info['set_num'].append(set_num)
        dict_of_add_basic_info['overplot_link_num'].append(overplot_link_num)
        dict_of_add_basic_info['marker'].append(marker)
        dict_of_add_basic_info['linestyle'].append(linestyle)
        dict_of_add_basic_info['alpha'].append(alpha)
        dict_of_add_basic_info['color'].append(color)
        dict_of_add_basic_info['cmap'].append(cmap)
        dict_of_add_basic_info['norm'].append(norm)
        dict_of_add_basic_info['h_plot_axspan'].append(h_plot_axspan)
        dict_of_add_basic_info['h_plot_axline'].append(h_plot_axline)
        dict_of_add_basic_info['legend_as_x'].append(legend_as_x)
        dict_of_add_basic_info['legend_as_y'].append(legend_as_y)
        dict_of_add_basic_info['important_plot'].append(important_plot)
        dict_of_add_basic_info['overwrite_use_si_units'].append(overwrite_use_si_units)
        dict_of_add_basic_info['min_max_value_list'].append(min_max_value_list)
        dict_of_add_basic_info['min_max_bool_type_list'].append(min_max_bool_type_list)
        dict_of_add_basic_info['min_max_data_type_list'].append(min_max_data_type_list)
        dict_of_add_basic_info['save_this'].append(save_this)
        dict_of_add_basic_info['save_this_path'].append(save_this_path)
        dict_of_add_basic_info['ignore_for_ax_range_setting'].append(ignore_for_ax_range_setting)
        dict_of_add_basic_info['arrow_width'].append(arrow_width)
        dict_of_add_basic_info['circle_radius'].append(circle_radius)
        dict_of_add_basic_info['fontsize'].append(fontsize)
        dict_of_add_basic_info['take_colour_from_set'].append(take_colour_from_set)
        dict_of_add_basic_info['equal_xy'].append(equal_xy)
        dict_of_add_basic_info['error_on'].append(error_on)
        dict_of_add_basic_info['thin_error_bar_num'].append(thin_error_bar_num)
        dict_of_add_basic_info['linewidth'].append(linewidth)
        dict_of_add_basic_info['add_onto_twin_ax'].append(add_onto_twin_ax)
        dict_of_add_basic_info['leg_loc'].append(leg_loc)
        dict_of_add_basic_info['hatch'].append(hatch)
        dict_of_add_basic_info['overwrite_legend_label'].append(overwrite_legend_label)
        if len(dict_of_add_basic_info['zorder']) == 0:
            dict_of_add_basic_info['zorder'].append(0)
        else:
            dict_of_add_basic_info['zorder'].append(dict_of_add_basic_info['zorder'][-1])

    @staticmethod
    def check_input_same_as_set(value, sets_used, string):
        if type(value) != list:
            raise ValueError(string+" must be a list not "+str(type(value)))
        if len(value) == 1:
            value_replace = []
            for i in range(len(sets_used)):
                value_replace.append(value[0])
            value = value_replace
        if len(sets_used) != len(value):
            print(value, sets_used)
            raise ValueError(string+" must be a same length as sets_used "+str(type(value)))
        return value

    def fill_plot_to_index_dict(self, key, x_type, y_type, z_type, plot_on, share_ax_x=False, share_ax_y=False, sorting_type_xyz_linking='all_comb', sorting_type_ax_index='appending', type_plot='line_plot', n_rows_cols_ax=[None, None], sets_used=[0], set_caption_abc=False, usetex=True, colour_sorting_type='loop_by_data_type', label_order_of_mag=False, labels_type='specific', ax_labels_type='general', index_stack_direction='vertical', proportion_legend_vert=1., proportion_legend_horiz=0.2, legend_below=False, shift_marker_labels_frac=[0., 0.], figure_title_on=True, save_to_svg=False, errorbar_background=True, errorbar_standard_setting=0, fig_size=None, fig_suplots_adjust_dict={}, make_room_for_legend=False, fix_leg_label_size=None, fix_x_label_size=None, fix_y_label_size=None, labelpad_twin_ax=30): # all these
        # sorting_type_xyz_linking = 'all_comb' or 'index', 'index_not_z', 'all_comb_not_z'  # The types of presorted plots link between the saved xyz values
        # sorting_type_ax_index = 'appending' or 'single', 'column', 'rows'  # The types of presorted plots link between the indicies
        # labels_type = 'specific' 'off_if_not_multiple' 'off'
        # ax_labels_type = 'specific', 'gen_if_multiple',  'general'
        if proportion_legend_vert > 1.:
            raise ValueError('Cant be bigger than the plot')
        if proportion_legend_horiz > 1.:
            raise ValueError('Cant be bigger than the plot')

        if len(shift_marker_labels_frac)!= 2:
            raise ValueError('shift_marker_labels_frac must be shift in x and y')
        ax_labels_type_options = np.array(['specific', 'gen_if_multiple',  'general'])
        if ax_labels_type not in ax_labels_type_options:
            raise ValueError('ax_labels_type was input ' + ax_labels_type + ' must be in: \n'+', '.join(ax_labels_type_options))
        labels_type_options = ['specific', 'off_if_not_multiple', 'off']
        index_stack_direction_options = ['horizontal', 'vertical', 'stack']
        if index_stack_direction not in index_stack_direction_options:
            raise ValueError('index_stack_direction was input ' + index_stack_direction + ' must be in: \n' + ', '.join(index_stack_direction_options))
        if labels_type not in labels_type_options:
            raise ValueError('labels_type was input ' + labels_type + ' must be in: \n'+', '.join(labels_type_options))
        x_type = self.check_input_same_as_set(x_type, sets_used, "x_type")
        y_type = self.check_input_same_as_set(y_type, sets_used, "y_type")
        z_type = self.check_input_same_as_set(z_type, sets_used, "z_type")

        sorting_type_xyz_linking =  self.check_input_same_as_set(sorting_type_xyz_linking, sets_used, "sorting_type_xyz_linking")
        sorting_type_ax_index = self.check_input_same_as_set(sorting_type_ax_index, sets_used, "sorting_type_ax_index")
        type_plot = self.check_input_same_as_set(type_plot, sets_used, "type_plot")
        if len(n_rows_cols_ax) != 2:
            raise ValueError("n_rows_cols_ax must be length 2")
        self.dict_of_plot_settings[key] = {}
        self.dict_of_plot_settings[key]['plot_key'] = key
        self.dict_of_plot_settings[key]['plot_on'] = plot_on
        self.dict_of_plot_settings[key]['sorting_type_xyz_linking'] = sorting_type_xyz_linking
        self.dict_of_plot_settings[key]['sorting_type_ax_index'] = sorting_type_ax_index
        self.dict_of_plot_settings[key]['type_plot'] = type_plot
        self.dict_of_plot_settings[key]['n_rows_cols_ax'] = n_rows_cols_ax
        self.dict_of_plot_settings[key]['x_type'] = x_type
        self.dict_of_plot_settings[key]['y_type'] = y_type
        self.dict_of_plot_settings[key]['z_type'] = z_type
        self.dict_of_plot_settings[key]['share_ax_x'] = share_ax_x
        self.dict_of_plot_settings[key]['share_ax_y'] = share_ax_y
        self.dict_of_plot_settings[key]['sets_used'] = sets_used
        self.dict_of_plot_settings[key]['set_caption_abc'] = set_caption_abc
        self.dict_of_plot_settings[key]['usetex'] = usetex
        self.dict_of_plot_settings[key]['colour_sorting_type'] = colour_sorting_type
        self.dict_of_plot_settings[key]['label_order_of_mag'] = label_order_of_mag
        self.dict_of_plot_settings[key]['ax_labels_type'] = ax_labels_type
        self.dict_of_plot_settings[key]['labels_type'] = labels_type
        self.dict_of_plot_settings[key]['index_stack_direction'] = index_stack_direction
        self.dict_of_plot_settings[key]['proportion_legend_vert'] = proportion_legend_vert
        self.dict_of_plot_settings[key]['proportion_legend_horiz'] = proportion_legend_horiz
        self.dict_of_plot_settings[key]['legend_below'] = legend_below
        self.dict_of_plot_settings[key]['shift_marker_labels_frac'] = shift_marker_labels_frac
        self.dict_of_plot_settings[key]['figure_title_on'] = figure_title_on
        self.dict_of_plot_settings[key]['errorbar_background'] = errorbar_background
        self.dict_of_plot_settings[key]['errorbar_standard_setting'] = errorbar_standard_setting
        self.dict_of_plot_settings[key]['fig_size'] = fig_size
        self.dict_of_plot_settings[key]['fig_suplots_adjust_dict'] = fig_suplots_adjust_dict
        self.dict_of_plot_settings[key]['make_room_for_legend'] = make_room_for_legend
        self.dict_of_plot_settings[key]['save_to_svg'] = save_to_svg
        self.dict_of_plot_settings[key]['fix_leg_label_size'] = fix_leg_label_size
        self.dict_of_plot_settings[key]['fix_x_label_size'] = fix_x_label_size
        self.dict_of_plot_settings[key]['fix_y_label_size'] = fix_y_label_size
        self.dict_of_plot_settings[key]['labelpad_twin_ax'] = labelpad_twin_ax

        # Add to dictionary to say where in the list it is
        self.plot_to_index_dict[key] = self.current_index_plot
        self.current_index_plot = self.current_index_plot + 1

    def initialise_the_dict(self, dict_of_add_basic_info):
        # dict_of_add_basic_info["label"] = label
        # dict_of_add_basic_info["units"] = units
        # dict_of_add_basic_info["gen_label"] = gen_label

        # Initialise
        dict_of_add_basic_info['list_of_plots_used'] = []
        dict_of_add_basic_info['xyz_list'] = []
        dict_of_add_basic_info['link_to_x_key'] = []
        dict_of_add_basic_info['link_to_y_key'] = []
        dict_of_add_basic_info['link_to_z_key'] = []
        dict_of_add_basic_info['set_num'] = []
        dict_of_add_basic_info['overplot_link_num'] = []
        dict_of_add_basic_info['marker'] = []
        dict_of_add_basic_info['linestyle'] = []
        dict_of_add_basic_info['alpha'] = []
        dict_of_add_basic_info['color'] = []
        dict_of_add_basic_info['cmap'] = []
        dict_of_add_basic_info['norm'] = []
        dict_of_add_basic_info['h_plot_axspan'] = []
        dict_of_add_basic_info['h_plot_axline'] = []
        dict_of_add_basic_info['legend_as_y'] = []
        dict_of_add_basic_info['legend_as_x'] = []
        dict_of_add_basic_info['important_plot'] = []
        dict_of_add_basic_info['overwrite_use_si_units'] = []
        dict_of_add_basic_info['min_max_value_list'] = []
        dict_of_add_basic_info['min_max_bool_type_list'] = []
        dict_of_add_basic_info['min_max_data_type_list'] = []
        dict_of_add_basic_info['save_this'] = []
        dict_of_add_basic_info['save_this_path'] = []
        dict_of_add_basic_info['ignore_for_ax_range_setting'] = []
        dict_of_add_basic_info['arrow_width'] = []
        dict_of_add_basic_info['circle_radius'] = []
        dict_of_add_basic_info['fontsize'] = []
        dict_of_add_basic_info['take_colour_from_set'] = []
        dict_of_add_basic_info['equal_xy'] = []
        dict_of_add_basic_info['error_on'] = []
        dict_of_add_basic_info['thin_error_bar_num'] = []
        dict_of_add_basic_info['linewidth'] = []
        dict_of_add_basic_info['add_onto_twin_ax'] = []
        dict_of_add_basic_info['leg_loc'] = []
        dict_of_add_basic_info['hatch'] = []
        dict_of_add_basic_info['overwrite_legend_label'] = []
        dict_of_add_basic_info['key_bounds'] = []
        dict_of_add_basic_info['zorder'] = []
        dict_of_add_basic_info['dict_channels'] = {}

    def add_dict_of_save_file(self, save_on, save_name_key, list_save_signals, save_type):
        self.dict_save_single_index[save_name_key] = {}
        self.dict_save_single_index[save_name_key]['list_save_signals'] = list_save_signals
        self.dict_save_single_index[save_name_key]['save_type'] = save_type
        self.dict_save_single_index[save_name_key]['save_on'] = save_on

    def fill_dict_of_add_basic_info(self, key, label, units, gen_label, dict_of_add_basic_info=None, data_type_to_index_dict=None, required_class_to_load=None):
        if required_class_to_load is None:
            required_class_to_load = self.default_required_class_to_load
        if dict_of_add_basic_info is None:
            if data_type_to_index_dict is None:
                return_dict = False
                dict_of_add_basic_info = self.dict_of_add_basic_info
                data_type_to_index_dict = self.data_type_to_index_dict
            else:
                raise ValueError("data_type_to_index_dict and dict_of_add_basic_info must both be Noen or neither")
        else:
            if data_type_to_index_dict is None:
                raise ValueError("data_type_to_index_dict and dict_of_add_basic_info must both be Noen or neither")
            return_dict = True

        dict_of_add_basic_info[key] = {}
        dict_of_add_basic_info[key]["label"] = label
        dict_of_add_basic_info[key]["units"] = units
        dict_of_add_basic_info[key]["gen_label"] = gen_label
        dict_of_add_basic_info[key]["required_class_to_load"] = required_class_to_load

        self.initialise_the_dict(dict_of_add_basic_info[key])
        dict_of_add_basic_info[key]['dict_channels'] = {}
        dict_of_add_basic_info[key]['dict_channels_filter_smooth_settings'] = {}
        dict_of_add_basic_info[key]['dict_channels_filter_smooth_settings']['filter_bool'] = False
        dict_of_add_basic_info[key]['dict_channels_filter_smooth_settings']['smooth_bool'] = False

        self.initialise_the_dict(dict_of_add_basic_info[key]['dict_channels'])
        # self.dict_of_add_basic_info[key]['times_added'] = 0
        data_type_to_index_dict[key] = self.current_index
        self.current_index = self.current_index + 1
        if return_dict:
            return dict_of_add_basic_info, data_type_to_index_dict

    def fill_dict_of_add_load_info(self, key, load_obj, split_into_channels=False, dict_of_add_load_info=None, added_parts_already=False, next_required_class=None, large_file=False):
        if dict_of_add_load_info is None:
            return_dict = False
            dict_of_add_load_info =  self.dict_of_add_load_info
        else:
            return_dict = True
        if added_parts_already:
            dict_of_add_load_info[key]['load_obj_next_list'].append(load_obj)
            dict_of_add_load_info[key]['load_obj_next_required_class'].append(next_required_class)
        else:
            dict_of_add_load_info[key] = {}
            dict_of_add_load_info[key]['load_obj'] = load_obj
            dict_of_add_load_info[key]['load_obj_next_list'] = []
            dict_of_add_load_info[key]['load_obj_next_required_class'] = []
            
            # default, updated in add_filtered_entry or add_smoothed_entry
            dict_of_add_load_info[key]['add_filtered'] = False
            dict_of_add_load_info[key]['add_smoothed'] = False
            dict_of_add_load_info[key]['add_time_delay'] = False
            dict_of_add_load_info[key]['add_interped'] = False
            dict_of_add_load_info[key]['add_split_channel'] = False
            dict_of_add_load_info[key]['split_into_channels'] = split_into_channels
            dict_of_add_load_info[key]['large_file'] = large_file
            if return_dict:
                return dict_of_add_load_info
    def initial_fill_dict_of_sets_to_index_sets(self, set_key):
        self.dict_of_sets_to_index_sets[set_key] = {}
        self.dict_of_sets_to_index_sets[set_key]["index"] = self.current_index_set_type
        self.current_index_set_type = self.current_index_set_type + 1
    def fill_dict_of_sets_to_index_sets(self, set_key, data_name, dimension=0):
        self.dict_of_sets_to_index_sets[set_key]["data_name"] = data_name
        self.dict_of_sets_to_index_sets[set_key]["dimension"] = dimension

    def Delete(self):
        self.fill_dict_of_settings(
            "core_n_hrts_avg_smooth", "core_n_hrts_avg_smooth", uid=None, dda=None, data_type_str='',
            elm_filter_bool=False, smooth_bool=False,
            percent_to_smooth=None, make_empty_bool=True, label='core density from hrts smooth', units='$m^{-3}$',
            opposite_ax_min_max=False, list_of_plots_used=[]
        )

        ################################################################################################################
        # Adding plots
        self.dict_of_plot_settings = {}

        self.fill_dict_of_settings_plots(
            "xyz", "xyz", plot_on=True, make_plots_per_c=True, x_ax=["time"], y_ax=["c", "z"],
            z_ax=["c", None],
            x_ax_repeat=1, y_ax_repeat=1, share_ax_x=True, share_ax_y=False,
            inc_ax_x_later=False, inc_ax_y_later=True,
            use_time_boundary="long", overwrite_linewidth=3.0, z_link_y_list=[0, None],
            type_plot_list_y=["2d_imshow", "lineplot"]  # This z_link_y_list=[0] links z to the first value in y_ax
        )
        ################################################################################################################
        # Making Variables

        self.dict_of_all_available_plots = {}
        self.fill_dict_of_all_avialable_plots(
            'c', 'c', attribute_name='input_data_obj_array_interpolated', is_long_list=True, ax_label=''
        )

        self.fill_dict_of_all_avialable_plots(
            'time', 'time', attribute_name='frame_time', ax_label='Time',
            attribute_name_raw='input_data_obj_array_interpolated', is_long_list_raw=True, data_arg='time_return',
            data_arg_err='time_error', attribute_name_raw_error='frame_time_err', error_attribute_name='frame_time_err',
            overwrite_raw_label="Time", overwrite_raw_units='s', units="s"
        )


        n_diagnostics_added = 0
        for key, index in self.data_type_to_index_dict.items():
            if index > n_diagnostics_added:
                n_diagnostics_added = index
        self.n_diagnostics_added = n_diagnostics_added + 1

        n_plots_added = 0
        for key, index in self.plot_to_index_dict.items():

            if index > n_plots_added:
                n_plots_added = index
        self.n_plots_added = n_plots_added + 1

    def fill_dict_of_all_avialable_plots(self, key, filename_str, attribute_name='', is_long_list=False, ax_label='',
                                         attribute_name_raw=None, is_long_list_raw=None, data_arg='data_return',
                                         error_attribute_name=None, attribute_name_raw_error=None,
                                         data_arg_err='error_return', marker_use='x', colour_use=None,
                                         linestyle_use='-', is_z_pos=False, text_plot=None, create_as_smooth=False,
                                         original_attribute=None, original_attribute_err=None, time_to_smooth=None,
                                         percent_to_smooth=None, dict_overplot_plot_setting_list=None,
                                         gaussian_smooth=False, gaussian_smooth_fwhm=None,
                                         dict_overplot_rank_list=None, overwrite_raw_label=None,
                                         overwrite_raw_units=None, units='', no_interp=False, no_raw=False,
                                         alpha_use=None, cmap_use=None, norm_cmap_use=None, make_empty_bool=False):

        if attribute_name_raw is None:
            attribute_name_raw = attribute_name
        if is_long_list_raw is None:
            is_long_list_raw = is_long_list
        self.dict_of_all_available_plots[key] = {}
        self.dict_of_all_available_plots[key]["filename_str"] = filename_str
        self.dict_of_all_available_plots[key]["attribute_name"] = attribute_name
        self.dict_of_all_available_plots[key]["is_long_list"] = is_long_list
        self.dict_of_all_available_plots[key]["ax_label"] = ax_label
        self.dict_of_all_available_plots[key]["attribute_name_raw"] = attribute_name_raw
        self.dict_of_all_available_plots[key]["is_long_list_raw"] = is_long_list_raw
        self.dict_of_all_available_plots[key]["data_arg"] = data_arg
        self.dict_of_all_available_plots[key]["no_interp"] = no_interp
        self.dict_of_all_available_plots[key]["no_raw"] = no_raw
        if error_attribute_name is None:
            # This value must be None in every object uses
            self.dict_of_all_available_plots[key]["error_attribute_name"] = 'none_error'
        else:
            self.dict_of_all_available_plots[key]["error_attribute_name"] = error_attribute_name
        if attribute_name_raw_error is None:
            self.dict_of_all_available_plots[key]["attribute_name_raw_error"] = 'none_error'
        else:
            self.dict_of_all_available_plots[key]["attribute_name_raw_error"] = attribute_name_raw_error
        self.dict_of_all_available_plots[key]["marker_use"] = marker_use
        self.dict_of_all_available_plots[key]["colour_use"] = colour_use
        self.dict_of_all_available_plots[key]["alpha_use"] = alpha_use
        self.dict_of_all_available_plots[key]["linestyle_use"] = linestyle_use
        self.dict_of_all_available_plots[key]["norm_cmap_use"] = norm_cmap_use
        self.dict_of_all_available_plots[key]["cmap_use"] = cmap_use
        self.dict_of_all_available_plots[key]["data_arg_err"] = data_arg_err
        self.dict_of_all_available_plots[key]["is_z_pos"] = is_z_pos
        self.dict_of_all_available_plots[key]["create_as_smooth"] = create_as_smooth
        self.dict_of_all_available_plots[key]["gaussian_smooth"] = gaussian_smooth
        self.dict_of_all_available_plots[key]["gaussian_smooth_fwhm"] = gaussian_smooth_fwhm
        self.dict_of_all_available_plots[key]["original_attribute"] = original_attribute
        self.dict_of_all_available_plots[key]["original_attribute_err"] = original_attribute_err
        self.dict_of_all_available_plots[key]["time_to_smooth"] = time_to_smooth
        self.dict_of_all_available_plots[key]["percent_to_smooth"] = percent_to_smooth
        self.dict_of_all_available_plots[key]["text_plot"] = text_plot
        self.dict_of_all_available_plots[key]["dict_overplot_plot_setting_list"] = dict_overplot_plot_setting_list
        self.dict_of_all_available_plots[key]["dict_overplot_rank_list"] = dict_overplot_rank_list
        self.dict_of_all_available_plots[key]["overwrite_raw_label"] = overwrite_raw_label
        self.dict_of_all_available_plots[key]["overwrite_raw_units"] = overwrite_raw_units
        self.dict_of_all_available_plots[key]["units"] = units
        self.dict_of_all_available_plots[key]["make_empty_bool"] = make_empty_bool

    def fill_dict_of_settings_was_data_req_model(self, key, used_model_predict):
        self.dict_of_data_choices[key]["used_model_predict"] = used_model_predict

    def fill_dict_of_settings(self, key, filename_str, uid, dda, data_type_str, elm_filter_bool, smooth_bool,
                              percent_to_smooth, is_sav_bool=False, make_empty_bool=False, use_as_nu=False,
                              use_as_psol=False, use_as_fi=False, list_of_plots_used=[], label='', beta_str='',
                              beta=None, units='', x_is_data=False, time_dependent=True, marker_use='', colour_use=None,
                              linestyle_use='-', elm_cycle_filter_bool=False, opposite_ax_min_max=False,
                              time_to_smooth=None, dict_overplot_plot_setting_list=None, dict_overplot_rank_list=None,
                              only_x_ax=False, only_y_ax=False, key_match_comb_for_plot=[], key_match_comb=[],
                              key_match_comb_use_as_x=[], no_interp=False, list_of_plots_used_for_z=[],
                              key_match_comb_for_plot_z=[], key_match_comb_z_to_use_as_x=[], alpha_use='',
                              no_error=False,
                              dict_overplot_plot_type=None, key_match_comb_only_use_as_z=None, cmap_use=None,
                              norm_cmap_use=None, gaussian_smooth=False, gaussian_smooth_fwhm=None, type_plot_use=''):

        if len(key_match_comb_for_plot) != len(key_match_comb):
            raise ValueError("key_match_comb_for_plot and key_match_comb must be same length")
        if len(key_match_comb_for_plot_z) != len(key_match_comb) and len(list_of_plots_used_for_z) > 1:
            raise ValueError("key_match_comb_for_plot_z and key_match_comb must be same length")
        if len(key_match_comb_use_as_x) != len(key_match_comb):
            raise ValueError("key_match_comb_use_as_x and key_match_comb must be same length")
        if key in self.dict_of_data_choices:
            raise LookupError('The key \'' + str(key) + '\' is already in the dictionary: self.dict_of_data_choices')

        if dict_overplot_plot_setting_list is not None:
            if not isinstance(dict_overplot_plot_setting_list, list):
                raise ValueError(
                    "dict_overplot_plot_setting_list must be type list not " +
                    str(type(dict_overplot_plot_setting_list))
                )

        if dict_overplot_rank_list is not None:
            if not isinstance(dict_overplot_rank_list, list):
                raise ValueError("dict_overplot_rank_list must be type list not " + str(type(dict_overplot_rank_list)))

        if dict_overplot_plot_setting_list is not None:
            if dict_overplot_rank_list is not None:
                if len(dict_overplot_plot_setting_list) != len(dict_overplot_rank_list):
                    raise ValueError(
                        "dict_overplot_plot_setting_list must be same length as dict_overplot_rank_list " +
                        str(len(dict_overplot_plot_setting_list)) + ", " + str(len(dict_overplot_rank_list))
                    )
            else:
                if dict_overplot_plot_type is None:
                    raise ValueError("dict_overplot_rank_list can\'t be NONE if dict_overplot_plot_setting_list is not")
        else:
            if dict_overplot_rank_list is not None:
                raise ValueError("dict_overplot_plot_setting_list can\'t be NONE if dict_overplot_rank_list is not")
            else:
                warnings.warn("dict_overplot_plot_setting_list can\'t be NONE if dict_overplot_rank_list is not")

        if dict_overplot_plot_type is not None:
            if not isinstance(dict_overplot_plot_type, list):
                raise ValueError("dict_overplot_plot_type must be type list not " + str(type(dict_overplot_plot_type)))

        if dict_overplot_plot_setting_list is not None:
            if dict_overplot_plot_type is not None:
                if len(dict_overplot_plot_setting_list) != len(dict_overplot_plot_type):
                    raise ValueError(
                        "dict_overplot_plot_setting_list must be same length as dict_overplot_plot_type " +
                        str(len(dict_overplot_plot_setting_list)) + ", " + str(len(dict_overplot_plot_type))
                    )
            else:
                if dict_overplot_rank_list is None:
                    raise ValueError("dict_overplot_plot_type can\'t be NONE if dict_overplot_plot_setting_list is not")
                else:
                    warnings.warn("dict_overplot_plot_type can\'t be NONE if dict_overplot_plot_setting_list is not")
        else:
            if dict_overplot_plot_type is not None:
                raise ValueError("dict_overplot_plot_setting_list can\'t be NONE if dict_overplot_plot_type is not")

        self.dict_of_data_choices[key] = {}
        self.dict_of_data_choices[key]["filename_str"] = filename_str
        self.dict_of_data_choices[key]["uid"] = uid
        self.dict_of_data_choices[key]["dda"] = dda
        self.dict_of_data_choices[key]["data_type_str"] = data_type_str
        self.dict_of_data_choices[key]["elm_filter_bool"] = elm_filter_bool
        self.dict_of_data_choices[key]["smooth_bool"] = smooth_bool
        self.dict_of_data_choices[key]["percent_to_smooth"] = percent_to_smooth
        self.dict_of_data_choices[key]["gaussian_smooth"] = gaussian_smooth
        self.dict_of_data_choices[key]["gaussian_smooth_fwhm"] = gaussian_smooth_fwhm
        self.dict_of_data_choices[key]["is_sav_bool"] = is_sav_bool
        self.dict_of_data_choices[key]["make_empty_bool"] = make_empty_bool
        self.dict_of_data_choices[key]["use_as_nu"] = use_as_nu
        self.dict_of_data_choices[key]["use_as_psol"] = use_as_psol
        self.dict_of_data_choices[key]["use_as_fi"] = use_as_fi
        self.dict_of_data_choices[key]["label"] = label
        self.dict_of_data_choices[key]["beta_str"] = beta_str  # unused?
        self.dict_of_data_choices[key]["beta"] = beta
        self.dict_of_data_choices[key]["units"] = units
        self.dict_of_data_choices[key]["x_is_data"] = x_is_data  # To use the ppf x data instead of the data
        self.dict_of_data_choices[key]["time_dependent"] = time_dependent
        self.dict_of_data_choices[key]["marker_use"] = marker_use
        self.dict_of_data_choices[key]["alpha_use"] = alpha_use
        self.dict_of_data_choices[key]["type_plot_use"] = type_plot_use
        self.dict_of_data_choices[key]["colour_use"] = colour_use
        self.dict_of_data_choices[key]["linestyle_use"] = linestyle_use
        self.dict_of_data_choices[key]["norm_cmap_use"] = norm_cmap_use
        self.dict_of_data_choices[key]["cmap_use"] = cmap_use
        self.dict_of_data_choices[key]["elm_cycle_filter_bool"] = elm_cycle_filter_bool
        self.dict_of_data_choices[key]["opposite_ax_min_max"] = opposite_ax_min_max
        self.dict_of_data_choices[key]["time_to_smooth"] = time_to_smooth
        self.dict_of_data_choices[key]["dict_overplot_plot_setting_list"] = dict_overplot_plot_setting_list
        self.dict_of_data_choices[key]["dict_overplot_rank_list"] = dict_overplot_rank_list
        self.dict_of_data_choices[key]["dict_overplot_plot_type"] = dict_overplot_plot_type
        self.dict_of_data_choices[key]["only_x_ax"] = only_x_ax
        self.dict_of_data_choices[key]["only_y_ax"] = only_y_ax
        self.dict_of_data_choices[key]["key_match_comb_use_as_x"] = key_match_comb_use_as_x
        self.dict_of_data_choices[key]["key_match_comb_for_plot"] = key_match_comb_for_plot
        self.dict_of_data_choices[key]["key_match_comb_for_plot_z"] = key_match_comb_for_plot_z
        self.dict_of_data_choices[key]["key_match_comb"] = key_match_comb
        self.dict_of_data_choices[key]["key_match_comb_z_to_use_as_x"] = key_match_comb_z_to_use_as_x
        self.dict_of_data_choices[key]["key_match_comb_only_use_as_z"] = key_match_comb_only_use_as_z
        self.dict_of_data_choices[key]["no_interp"] = no_interp
        self.dict_of_data_choices[key]["no_raw"] = False  # always False
        self.dict_of_data_choices[key]["no_error"] = no_error
        if not type(list_of_plots_used) == list:
            raise TypeError("must input a list in list_of_plots_used")
        if not type(list_of_plots_used_for_z) == list:
            raise TypeError("must input a list in list_of_plots_used_for_z")

        self.dict_of_data_choices[key]["list_of_plots_used"] = list_of_plots_used
        self.dict_of_data_choices[key]["list_of_plots_used_for_z"] = list_of_plots_used_for_z

        # Add to dictionary to say where in the list it is
        self.data_type_to_index_dict[filename_str] = self.current_index
        self.current_index = self.current_index + 1

    def fill_dict_of_settings_plots(self, key, filename_str, plot_on=False, make_plots_per_c=False,
                                    make_plots_square=False, x_ax=[], y_ax=[], z_ax=[], x_ax_repeat=None,
                                    y_ax_repeat=None,
                                    share_ax_x=False, share_ax_y=False, inc_ax_x_later=False, inc_ax_y_later=False,
                                    split_later=False, gen_x_label=None, gen_y_label=None, overwrite_units_x=None,
                                    overwrite_units_y=None, reset_plt_index_list_x=None, reset_plt_index_list_y=None,
                                    gen_xlabel_overwite_bool=False, gen_ylabel_overwite_bool=False,
                                    enable_x_errors=True, enable_y_errors=True, enable_2D=True, overwrite_marker_x=None,
                                    overwrite_marker_y=None, overwrite_colour_x=None, overwrite_colour_y=None,
                                    overwrite_linestyle_x=None, overwrite_linestyle_y=None,
                                    prefer_y_marker_settings=True, y_ax_label_ax_legend=False, x_ax_comb=None,
                                    y_ax_comb=None, opposite_ax_min_max_x=None, opposite_ax_min_max_y=None,
                                    exponent_in_legend_x=False, exponent_in_legend_y=False,
                                    prefer_y_overwrite_index_settings=True, plot_raw_data_all=True,
                                    plot_interp_data_all=True, use_time_boundary=None, use_general_si_prefix=False,
                                    overwrite_linewidth=1.0, use_c_as_combination=False, type_plot_list_x=None,
                                    type_plot_list_y=None, overwrite_units_z=None, overwrite_marker_z=None,
                                    overwrite_linestyle_z=None, overwrite_colour_z=None, type_plot_list_z=None,
                                    gen_z_label=None, reset_plt_index_list_z=None, opposite_ax_min_max_z=None,
                                    overwrite_alpha_x=None,
                                    overwrite_alpha_y=None, overwrite_alpha_z=None, overwrite_cmap_x=None,
                                    overwrite_cmap_y=None, overwrite_cmap_z=None, overwrite_norm_cmap_x=None,
                                    overwrite_norm_cmap_y=None, overwrite_norm_cmap_z=None, use_c_as_combination2=False,
                                    z_link_x_list=None, z_link_y_list=None):
        if z_link_x_list is None:  # link between y and z, the index of x
            z_link_x_list = []
            for i in range(len(x_ax)):
                z_link_x_list.append(i)
        else:
            if len(z_link_x_list) != len(x_ax):
                raise IOError('The input: \'z_link_x_list\' is not the correct length')
        if z_link_y_list is None:  # link between y and z, the index of y
            z_link_y_list = []
            for i in range(len(y_ax)):
                z_link_y_list.append(i)
        else:
            if len(z_link_y_list) != len(y_ax):
                raise IOError('The input: \'z_link_y_list\' is not the correct length')
        if opposite_ax_min_max_x is None:
            opposite_ax_min_max_x = []
            for i in range(len(x_ax)):
                opposite_ax_min_max_x.append(False)
        else:
            if len(opposite_ax_min_max_x) != len(x_ax):
                raise IOError('The input: \'opposite_ax_min_max_x\' is not the correct length')
        if opposite_ax_min_max_y is None:
            opposite_ax_min_max_y = []
            for i in range(len(y_ax)):
                opposite_ax_min_max_y.append(False)
        else:
            if len(opposite_ax_min_max_y) != len(y_ax):
                raise IOError('The input: \'opposite_ax_min_max_y\' is not the correct length')
        if opposite_ax_min_max_z is None:
            opposite_ax_min_max_z = []
            for i in range(len(z_ax)):
                opposite_ax_min_max_z.append(False)
        else:
            if len(opposite_ax_min_max_z) != len(z_ax):
                raise IOError('The input: \'opposite_ax_min_max_z\' is not the correct length')
        if overwrite_units_x is None:
            overwrite_units_x = []
            for i in range(len(x_ax)):
                overwrite_units_x.append(None)
        else:
            if len(overwrite_units_x) != len(x_ax):
                raise IOError('The input: \'overwrite_units_x\' is not the correct length')
        if overwrite_units_y is None:
            overwrite_units_y = []
            for i in range(len(y_ax)):
                overwrite_units_y.append(None)
        else:
            if len(overwrite_units_y) != len(y_ax):
                raise IOError('The input: \'overwrite_units_y\' is not the correct length')
        if overwrite_units_z is None:
            overwrite_units_z = []
            for i in range(len(z_ax)):
                overwrite_units_z.append(None)
        else:
            if len(overwrite_units_z) != len(z_ax):
                raise IOError('The input: \'overwrite_units_z\' is not the correct length')

        if overwrite_alpha_x is None:
            overwrite_alpha_x = []
            for i in range(len(x_ax)):
                overwrite_alpha_x.append(None)
        else:
            if len(overwrite_alpha_x) != len(x_ax):
                raise IOError('The input: \'overwrite_alpha_x\' is not the correct length')
        if overwrite_alpha_y is None:
            overwrite_alpha_y = []
            for i in range(len(y_ax)):
                overwrite_alpha_y.append(None)
        else:
            if len(overwrite_alpha_y) != len(y_ax):
                raise IOError('The input: \'overwrite_alpha_y\' is not the correct length')
        if overwrite_alpha_z is None:
            overwrite_alpha_z = []
            for i in range(len(z_ax)):
                overwrite_alpha_z.append(None)
        else:
            if len(overwrite_alpha_z) != len(z_ax):
                raise IOError('The input: \'overwrite_alpha_z\' is not the correct length')

        if gen_x_label is None:
            gen_x_label = []
            for i in range(len(x_ax)):
                gen_x_label.append('')
        else:
            if len(gen_x_label) != len(x_ax):
                raise IOError('The input: \'gen_x_label\' is not the correct length')
        if gen_y_label is None:
            gen_y_label = []
            for i in range(len(y_ax)):
                gen_y_label.append('')
        else:
            if len(gen_y_label) != len(y_ax):
                raise IOError('The input: \'gen_y_label\' is not the correct length')
        if gen_z_label is None:
            gen_z_label = []
            for i in range(len(z_ax)):
                gen_z_label.append('')
        else:
            if len(gen_z_label) != len(z_ax):
                raise IOError('The input: \'gen_z_label\' is not the correct length')
        if reset_plt_index_list_x is None:
            reset_plt_index_list_x = []
            for i in range(len(x_ax)):
                reset_plt_index_list_x.append(False)
        else:
            # Check if it is the same lenght as x_ax
            if len(reset_plt_index_list_x) != len(x_ax):
                raise IOError('The input: \'reset_plt_index_list_x\' is not the correct length')
        if reset_plt_index_list_y is None:
            reset_plt_index_list_y = []
            for i in range(len(y_ax)):
                reset_plt_index_list_y.append(False)
        else:
            # Check if it is the same lenght as x_ax
            if len(reset_plt_index_list_y) != len(y_ax):
                raise IOError('The input: \'reset_plt_index_list_y\' is not the correct length')
        if reset_plt_index_list_z is None:
            reset_plt_index_list_z = []
            for i in range(len(z_ax)):
                reset_plt_index_list_z.append(False)
        else:
            # Check if it is the same lenght as x_ax
            if len(reset_plt_index_list_z) != len(z_ax):
                raise IOError('The input: \'reset_plt_index_list_z\' is not the correct length')
        if x_ax_comb is None:
            x_ax_comb = []
            for i in range(len(x_ax)):
                x_ax_comb.append(np.nan)
        else:
            if len(x_ax_comb) != len(x_ax):
                raise IOError('The input: \'x_ax_comb\' is not the correct length')
        if y_ax_comb is None:
            y_ax_comb = []
            for i in range(len(y_ax)):
                y_ax_comb.append(np.nan)
        else:
            if len(y_ax_comb) != len(y_ax):
                raise IOError('The input: \'y_ax_comb\' is not the correct length')
        default_plot_str = 'lineplot'
        plot_str_options_list = ['lineplot', '2d_imshow', 'text_plot', 'scatter']
        if prefer_y_marker_settings:
            if type_plot_list_y is not None:
                if not type(type_plot_list_y) == list:
                    if not type(type_plot_list_y) == str:
                        raise TypeError("must input a list or string in type_plot_list_y")
                    type_plot_y = []
                    for i in range(len(y_ax)):
                        type_plot_y.append(type_plot_list_y)
                    type_plot_list_y = type_plot_y
                if len(type_plot_list_y) != len(y_ax):
                    raise IOError('The input: \'type_plot_list_y\' is not the correct length')
            else:
                type_plot_y = []
                for i in range(len(y_ax)):
                    type_plot_y.append(default_plot_str)
                type_plot_list_y = type_plot_y
            for i in type_plot_list_y:
                if i not in plot_str_options_list:
                    raise ValueError(
                        "type_plot_list_y input must be in plot_str_options_list:" + str(plot_str_options_list))
            if type_plot_list_z is not None:
                if not type(type_plot_list_z) == list:
                    if not type(type_plot_list_z) == str:
                        raise TypeError("must input a list or string in type_plot_list_z")
                    type_plot_z = []
                    for i in range(len(z_ax)):
                        type_plot_z.append(type_plot_list_z)
                    type_plot_list_z = type_plot_z
                if len(type_plot_list_z) != len(z_ax):
                    raise IOError('The input: \'type_plot_list_z\' is not the correct length')
            else:
                type_plot_z = []
                for i in range(len(z_ax)):
                    type_plot_z.append(default_plot_str)
                type_plot_list_z = type_plot_z
            for i in type_plot_list_z:
                if i not in plot_str_options_list:
                    raise ValueError(
                        "type_plot_list_z input must be in plot_str_options_list:" + str(plot_str_options_list))

            if overwrite_marker_y is None:
                overwrite_marker_y = []
                for i in range(len(y_ax)):
                    overwrite_marker_y.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_marker_y) != len(y_ax):
                    raise IOError('The input: \'overwrite_marker_y\' is not the correct length')
            if overwrite_marker_z is None:
                overwrite_marker_z = []
                for i in range(len(z_ax)):
                    overwrite_marker_z.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_marker_z) != len(z_ax):
                    raise IOError('The input: \'overwrite_marker_z\' is not the correct length')
            if overwrite_colour_y is None:
                overwrite_colour_y = []
                for i in range(len(y_ax)):
                    overwrite_colour_y.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_colour_y) != len(y_ax):
                    raise IOError('The input: \'overwrite_colour_y\' is not the correct length')
            if overwrite_colour_z is None:
                overwrite_colour_z = []
                for i in range(len(z_ax)):
                    overwrite_colour_z.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_colour_z) != len(z_ax):
                    raise IOError('The input: \'overwrite_colour_z\' is not the correct length')
            if overwrite_linestyle_y is None:
                overwrite_linestyle_y = []
                for i in range(len(y_ax)):
                    overwrite_linestyle_y.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_linestyle_y) != len(y_ax):
                    raise IOError('The input: \'overwrite_linestyle_y\' is not the correct length')
            if overwrite_cmap_y is None:
                overwrite_cmap_y = []
                for i in range(len(y_ax)):
                    overwrite_cmap_y.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_cmap_y) != len(y_ax):
                    raise IOError('The input: \'overwrite_cmap_y\' is not the correct length')
            if overwrite_norm_cmap_y is None:
                overwrite_norm_cmap_y = []
                for i in range(len(y_ax)):
                    overwrite_norm_cmap_y.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_norm_cmap_y) != len(y_ax):
                    raise IOError('The input: \'overwrite_norm_cmap_y\' is not the correct length')

            if overwrite_linestyle_z is None:
                overwrite_linestyle_z = []
                for i in range(len(z_ax)):
                    overwrite_linestyle_z.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_linestyle_z) != len(z_ax):
                    raise IOError('The input: \'overwrite_linestyle_z\' is not the correct length')
            if overwrite_cmap_z is None:
                overwrite_cmap_z = []
                for i in range(len(y_ax)):
                    overwrite_cmap_z.append(None)
            else:
                # Check if it is the same length as x_ax
                if len(overwrite_cmap_z) != len(y_ax):
                    raise IOError('The input: \'overwrite_cmap_z\' is not the correct length')
            if overwrite_norm_cmap_z is None:
                overwrite_norm_cmap_z = []
                for i in range(len(y_ax)):
                    overwrite_norm_cmap_z.append(None)
            else:
                # Check if it is the same length as x_ax
                if len(overwrite_norm_cmap_z) != len(y_ax):
                    raise IOError('The input: \'overwrite_norm_cmap_z\' is not the correct length')
        else:
            if type_plot_list_x is not None:
                if not type(type_plot_list_x) == list:
                    if not type(type_plot_list_x) == str:
                        raise TypeError("must input a list or string in type_plot_list_x")
                    type_plot_x = []
                    for i in range(len(y_ax)):
                        type_plot_x.append(type_plot_list_x)
                    type_plot_list_x = type_plot_x
                if len(type_plot_list_x) != len(y_ax):
                    raise IOError('The input: \'type_plot_list_x\' is not the correct length')
            else:
                type_plot_x = []
                for i in range(len(y_ax)):
                    type_plot_x.append(default_plot_str)
                type_plot_list_x = type_plot_x
            for i in type_plot_list_x:
                if i not in plot_str_options_list:
                    raise ValueError(
                        "type_plot_list_x input must be in plot_str_options_list:" + str(plot_str_options_list))

            if overwrite_marker_x is None:
                overwrite_marker_x = []
                for i in range(len(x_ax)):
                    overwrite_marker_x.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_marker_x) != len(x_ax):
                    raise IOError('The input: \'overwrite_marker_x\' is not the correct length')

            if overwrite_colour_x is None:
                overwrite_colour_x = []
                for i in range(len(x_ax)):
                    overwrite_colour_x.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_colour_x) != len(x_ax):
                    raise IOError('The input: \'overwrite_colour_x\' is not the correct length')
            if overwrite_linestyle_x is None:
                overwrite_linestyle_x = []
                for i in range(len(x_ax)):
                    overwrite_linestyle_x.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_linestyle_x) != len(x_ax):
                    raise IOError('The input: \'overwrite_linestyle_x\' is not the correct length')
            if overwrite_cmap_x is None:
                overwrite_cmap_x = []
                for i in range(len(x_ax)):
                    overwrite_cmap_x.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_cmap_x) != len(x_ax):
                    raise IOError('The input: \'overwrite_cmap_x\' is not the correct length')
            if overwrite_norm_cmap_x is None:
                overwrite_norm_cmap_x = []
                for i in range(len(x_ax)):
                    overwrite_norm_cmap_x.append(None)
            else:
                # Check if it is the same lenght as x_ax
                if len(overwrite_norm_cmap_x) != len(x_ax):
                    raise IOError('The input: \'overwrite_norm_cmap_x\' is not the correct length')

        if use_time_boundary is not None:
            if isinstance(use_time_boundary, str):
                use_time_boundary_options = ["long", "short", "med"]
                if use_time_boundary not in use_time_boundary_options:
                    raise IOError('The input: \'use_time_boundary\' is not a valid string ')
            else:
                if len(use_time_boundary) != 2:
                    raise IOError('The input: \'use_time_boundary\' must be start and end time or string ')

        for i in range(len(x_ax)):
            if gen_x_label[i] == '' and share_ax_x:
                warnings.warn("need a general label if sharing ax x")
        for i in range(len(y_ax)):
            if gen_y_label[i] == '' and share_ax_y:
                warnings.warn("need a general label if sharing ax y")
        if key in self.dict_of_plot_settings:
            raise LookupError('The key \'' + str(key) + '\' is already in the dictionary: self.dict_of_plot_settings')
        self.dict_of_plot_settings[key] = {}
        self.dict_of_plot_settings[key]["filename_str"] = filename_str
        self.dict_of_plot_settings[key]["plot_on"] = plot_on
        self.dict_of_plot_settings[key]["make_plots_per_c"] = make_plots_per_c
        self.dict_of_plot_settings[key]["make_plots_square"] = make_plots_square
        self.dict_of_plot_settings[key]["x_ax"] = x_ax
        self.dict_of_plot_settings[key]["y_ax"] = y_ax
        self.dict_of_plot_settings[key]["z_ax"] = z_ax
        self.dict_of_plot_settings[key]["x_ax_repeat"] = x_ax_repeat
        self.dict_of_plot_settings[key]["y_ax_repeat"] = y_ax_repeat
        self.dict_of_plot_settings[key]["share_ax_x"] = share_ax_x
        self.dict_of_plot_settings[key]["share_ax_y"] = share_ax_y
        self.dict_of_plot_settings[key]["inc_ax_x_later"] = inc_ax_x_later
        self.dict_of_plot_settings[key]["inc_ax_y_later"] = inc_ax_y_later
        self.dict_of_plot_settings[key]["split_later"] = split_later  # Split the graphs into another axis
        self.dict_of_plot_settings[key]["gen_x_label"] = gen_x_label  # A general x label to add to the initital one
        self.dict_of_plot_settings[key]["gen_y_label"] = gen_y_label  # A general y label to add to the initital one
        # if not None, then the units are overwritten to this value
        self.dict_of_plot_settings[key]["overwrite_units_x"] = overwrite_units_x
        # if not None, then teh units are overwritten to this value
        self.dict_of_plot_settings[key]["overwrite_units_y"] = overwrite_units_y
        # if not None, then teh units are overwritten to this value
        self.dict_of_plot_settings[key]["overwrite_units_z"] = overwrite_units_z
        self.dict_of_plot_settings[key]["reset_plt_index_list_x"] = reset_plt_index_list_x
        # If the plot index needs to be reset for the graph to overplot
        self.dict_of_plot_settings[key]["reset_plt_index_list_y"] = reset_plt_index_list_y
        # If the plot index needs to be reset for the graph to overplot
        self.dict_of_plot_settings[key]["reset_plt_index_list_z"] = reset_plt_index_list_z
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["gen_xlabel_overwite_bool"] = gen_xlabel_overwite_bool
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["gen_ylabel_overwite_bool"] = gen_ylabel_overwite_bool
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["prefer_y_marker_settings"] = prefer_y_marker_settings
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_marker_x"] = overwrite_marker_x
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_marker_y"] = overwrite_marker_y
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_marker_z"] = overwrite_marker_z
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_alpha_x"] = overwrite_alpha_x
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_alpha_y"] = overwrite_alpha_y
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_alpha_z"] = overwrite_alpha_z
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_colour_x"] = overwrite_colour_x
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_colour_y"] = overwrite_colour_y
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_colour_z"] = overwrite_colour_z
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_linestyle_x"] = overwrite_linestyle_x
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_linestyle_y"] = overwrite_linestyle_y
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["overwrite_linestyle_z"] = overwrite_linestyle_z
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["enable_x_errors"] = enable_x_errors
        # If the general label should overwrite the old one
        self.dict_of_plot_settings[key]["enable_y_errors"] = enable_y_errors
        # put the axis on as a legend instead
        self.dict_of_plot_settings[key]["y_ax_label_ax_legend"] = y_ax_label_ax_legend
        # is the index of which y_ax to combine with and is not a combination. If index is np.nan
        # it will do the combination of all other np.nan in y_ax_comb
        self.dict_of_plot_settings[key]["x_ax_comb"] = x_ax_comb
        # is the index of which x_ax to combine with and is not a combination.
        # If index is np.nan it will do the combination of all other np.nan in x_ax_comb
        self.dict_of_plot_settings[key]["y_ax_comb"] = y_ax_comb
        self.dict_of_plot_settings[key]["opposite_ax_min_max_x"] = opposite_ax_min_max_x
        self.dict_of_plot_settings[key]["opposite_ax_min_max_y"] = opposite_ax_min_max_y
        self.dict_of_plot_settings[key]["opposite_ax_min_max_z"] = opposite_ax_min_max_z
        self.dict_of_plot_settings[key]["exponent_in_legend_x"] = exponent_in_legend_x
        self.dict_of_plot_settings[key]["exponent_in_legend_y"] = exponent_in_legend_y
        self.dict_of_plot_settings[key]["prefer_y_overwrite_index_settings"] = prefer_y_overwrite_index_settings
        self.dict_of_plot_settings[key]["plot_raw_data_all"] = plot_raw_data_all
        self.dict_of_plot_settings[key]["plot_interp_data_all"] = plot_interp_data_all
        self.dict_of_plot_settings[key]["use_time_boundary"] = use_time_boundary  # "long" or "short"
        self.dict_of_plot_settings[key]["use_general_si_prefix"] = use_general_si_prefix  #
        self.dict_of_plot_settings[key]["overwrite_linewidth"] = overwrite_linewidth  #
        self.dict_of_plot_settings[key]["use_c_as_combination"] = use_c_as_combination  #
        self.dict_of_plot_settings[key]["use_c_as_combination2"] = use_c_as_combination2  #
        self.dict_of_plot_settings[key]["type_plot_list_x"] = type_plot_list_x  #
        self.dict_of_plot_settings[key]["type_plot_list_y"] = type_plot_list_y  #
        self.dict_of_plot_settings[key]["type_plot_list_z"] = type_plot_list_z  #
        self.dict_of_plot_settings[key]["overwrite_cmap_x"] = overwrite_cmap_x  #
        self.dict_of_plot_settings[key]["overwrite_cmap_y"] = overwrite_cmap_y  #
        self.dict_of_plot_settings[key]["overwrite_cmap_z"] = overwrite_cmap_z  #
        self.dict_of_plot_settings[key]["overwrite_norm_cmap_x"] = overwrite_norm_cmap_x  #
        self.dict_of_plot_settings[key]["overwrite_norm_cmap_y"] = overwrite_norm_cmap_y  #
        self.dict_of_plot_settings[key]["overwrite_norm_cmap_z"] = overwrite_norm_cmap_z  #
        self.dict_of_plot_settings[key]["gen_z_label"] = gen_z_label  # A general y label to add to the initital one
        self.dict_of_plot_settings[key]["z_link_x_list"] = z_link_x_list  #
        self.dict_of_plot_settings[key]["z_link_y_list"] = z_link_y_list  #

        # Add to dictionary to say where in the list it is
        self.plot_to_index_dict[filename_str] = self.current_index_plot
        self.current_index_plot = self.current_index_plot + 1

    def make_triple_prod_entries(self):
        new_key_list, _ = self.make_triple_prod_indices_list(self.dict_of_data_choices)

        for i in range(len(new_key_list)):
            self.fill_dict_of_settings(
                new_key_list[i], new_key_list[i], uid=None, dda=None, data_type_str='', elm_filter_bool=False,
                smooth_bool=False, percent_to_smooth=None, make_empty_bool=True,
                label=r'$\frac{n_{u}^{}f_{i}^{1/2}}{P_{sol}^{5/7}}$ from ' + new_key_list[i], beta_str='', beta=1.,
                list_of_plots_used=['cvstime_plots', 'z_predicted_time_plots', 'cnormvstime_plots', 'csynthetic_plots']
            )

    def make_triple_prod_entries_2_comb_parameters(self):
        new_key_list, _, type_term_saved = \
            self.make_triple_prod_indices_list_2_parmas(self.dict_of_data_choices, type_term=True)
        for i in range(len(new_key_list)):
            self.fill_dict_of_settings(
                new_key_list[i], new_key_list[i], uid=None, dda=None, data_type_str='', elm_filter_bool=False,
                smooth_bool=False, percent_to_smooth=None, make_empty_bool=True,
                label=type_term_saved[i] + ' \n from ' + new_key_list[i], beta_str='', beta=1.,
                list_of_plots_used=['cvstime_plots']
            )

    @staticmethod
    def make_triple_prod_indices_list(dict_of_data_choices):
        nu_keys = []
        psol_keys = []
        fi_keys = []
        for keys, values in dict_of_data_choices.items():
            if dict_of_data_choices[keys]["use_as_nu"]:
                nu_keys.append(keys)
            if dict_of_data_choices[keys]["use_as_psol"]:
                psol_keys.append(keys)
            if dict_of_data_choices[keys]["use_as_fi"]:
                fi_keys.append(keys)

        new_key_list = []
        new_key_list_saved_parts = []
        for i in range(len(nu_keys)):
            for j in range(len(psol_keys)):
                for k in range(len(fi_keys)):
                    new_key_list.append(nu_keys[i] + psol_keys[j] + fi_keys[k])
                    new_key_list_saved_parts.append(
                        [nu_keys[i] + psol_keys[j] + fi_keys[k], nu_keys[i], psol_keys[j], fi_keys[k]]
                    )
        return new_key_list, new_key_list_saved_parts

    @staticmethod
    def make_triple_prod_indices_list_2_parmas(dict_of_data_choices, type_term=False):
        nu_keys = []
        psol_keys = []
        fi_keys = []
        if type_term:
            list_combinations1 = []
            list_combinations2 = []
            list_combinations3 = []
        else:
            list_combinations1 = None
            list_combinations2 = None
            list_combinations3 = None
        for keys, values in dict_of_data_choices.items():
            if dict_of_data_choices[keys]["use_as_nu"]:
                nu_keys.append(keys)
                if type_term:
                    list_combinations1.append('$nu$ ')
            if dict_of_data_choices[keys]["use_as_psol"]:
                psol_keys.append(keys)
                if type_term:
                    list_combinations2.append('$p_{SOL}^{-5/7}$ ')
            if dict_of_data_choices[keys]["use_as_fi"]:
                fi_keys.append(keys)
                if type_term:
                    list_combinations3.append('$f_{I}^{1/2}$ ')

        new_key_list = [list(items)[0][i] + list(items)[1][j] for items in
                        combinations((nu_keys, fi_keys, psol_keys), 2) for i in
                        range(len(list(items)[0])) for j in range(len(list(items)[1]))]

        if type_term:
            type_term_saved = [
                list(items)[0][i] + list(items)[1][j]
                for items in combinations((list_combinations1, list_combinations3, list_combinations2), 2)
                for i in range(len(list(items)[0]))
                for j in range(len(list(items)[1]))
            ]
        else:
            type_term_saved = None
        new_key_list_saved_parts = [
            [list(items)[0][i] + list(items)[1][j], list(items)[0][i], list(items)[1][j]]
            for items in combinations((nu_keys, fi_keys, psol_keys), 2)
            for i in range(len(list(items)[0]))
            for j in range(len(list(items)[1]))
        ]

        return new_key_list, new_key_list_saved_parts, type_term_saved


class InputDataSettingsEquilibrium(InputDataSettings):
    """
    storing a dictionary of data settings that can be added to later for equilibrium data
    """

    def __init__(self, input_uid, src):
        self.current_index = 0
        self.current_index_plot = 0
        self.data_type_to_index_dict = {}
        self.plot_to_index_dict = {}

        self.dict_of_data_choices = {}
        self.fill_dict_of_settings(
            "_bnd_data", "_bnd_data", uid=input_uid, dda=src, data_type_str="fbnd", elm_filter_bool=False,
            smooth_bool=False, percent_to_smooth=None, label='psi at LCFS'
        )  # Only store things we will need again as attributes

        self.fill_dict_of_settings(
            "_axs_data", "_axs_data", uid=input_uid, dda=src, data_type_str="faxs", elm_filter_bool=False,
            smooth_bool=False, percent_to_smooth=None, label='psi flux func at mag axis'
        )  # Only store things we will need again as attributes
        self.fill_dict_of_settings(
            "_psi_data", "_psi_data", uid=input_uid, dda=src, data_type_str="psi", elm_filter_bool=False,
            smooth_bool=False, percent_to_smooth=None, label='2d flux func'
        )  # Only store things we will need again as attributes
        self.fill_dict_of_settings(
            "_psi_r", "_psi_r", uid=input_uid, dda=src, data_type_str="psir", elm_filter_bool=False, smooth_bool=False,
            percent_to_smooth=None, label='computational grid r values'
        )  # Only store things we will need again as attributes
        self.fill_dict_of_settings(
            "_psi_z", "_psi_z", uid=input_uid, dda=src, data_type_str="psiz", elm_filter_bool=False, smooth_bool=False,
            percent_to_smooth=None, label='computational grid z values'
        )  # Only store things we will need again as attributes
        self.fill_dict_of_settings(
            "_r_bphi", "_r_bphi", uid=input_uid, dda=src, data_type_str="bvac", elm_filter_bool=False,
            smooth_bool=False, percent_to_smooth=None, label='magnetic vacuum field, (current flux function)'
        )  # Only store things we will need again as attributes

        if not input_uid == "sgibson":
            self.fill_dict_of_settings(
                "_rxpm", "_rxpm", uid=input_uid, dda=src, data_type_str="rxpm", elm_filter_bool=False,
                smooth_bool=False, percent_to_smooth=None, label='r value at major x point'
            )  # Only store things we will need again as attributes
            self.fill_dict_of_settings(
                "_zxpm", "_zxpm", uid=input_uid, dda=src, data_type_str="zxpm", elm_filter_bool=False,
                smooth_bool=False, percent_to_smooth=None, label='z value at major x point'
            )  # Only store things we will need again as attributes
        self.fill_dict_of_settings(
            "_rxpl", "_rxpl", uid=input_uid, dda=src, data_type_str="rxpl", elm_filter_bool=False, smooth_bool=False,
            percent_to_smooth=None, label='r value at lower x point'
        )  # Only store things we will need again as attributes
        self.fill_dict_of_settings(
            "_zxpl", "_zxpl", uid=input_uid, dda=src, data_type_str="zxpl", elm_filter_bool=False, smooth_bool=False,
            percent_to_smooth=None, label='z value at lower x point'
        )  # Only store things we will need again as attributes
        self.fill_dict_of_settings(
            "_rxpu", "_rxpu", uid=input_uid, dda=src, data_type_str="rxpu", elm_filter_bool=False, smooth_bool=False,
            percent_to_smooth=None, label='r value at upper x point'
        )  # Only store things we will need again as attributes
        self.fill_dict_of_settings(
            "_zxpu", "_zxpu", uid=input_uid, dda=src, data_type_str="zxpu", elm_filter_bool=False, smooth_bool=False,
            percent_to_smooth=None, label='z value at upper x point'
        )  # Only store things we will need again as attributes
        self.fill_dict_of_settings(
            "_axisr", "_axisr", uid=input_uid, dda=src, data_type_str="rmag", elm_filter_bool=False, smooth_bool=False,
            percent_to_smooth=None, label='r value at magnetic axis'
        )  # Only store things we will need again as attributes
        self.fill_dict_of_settings(
            "_axisz", "_axisz", uid=input_uid, dda=src, data_type_str="zmag", elm_filter_bool=False, smooth_bool=False,
            percent_to_smooth=None, label='z value at magnetic axis'
        )  # Only store things we will need again as attributes
        # Must be last in the class
        n_diagnostics_added = 0
        for key, index in self.data_type_to_index_dict.items():
            if index > n_diagnostics_added:
                n_diagnostics_added = index
        self.n_diagnostics_added = n_diagnostics_added + 1


class InputData(object):
    """
    Get the data from the ppf
    """

    def __init__(self, dict_of_data_choices, pulse, hmode_settings=None, max_time=None, min_time=None):
        # Setup the object ready to accept any data
        self.time_return = None
        self.data_return = None
        self.error_return = None
        self.time_error = None
        self.time_return_cam_rng = None
        self.data_return_cam_rng = None
        self.label = None
        self.opposite_ax_min_max = None
        self.list_of_plots_used = None
        self.list_of_plots_used_for_z = None
        self.beta = None
        self.used_model_predict = None
        self.units = None
        self.time_dependent = None
        self.marker_use = None
        self.alpha_use = None
        self.colour_use = None
        self.linestyle_use = None
        self.norm_cmap_use = None
        self.type_plot_use = None
        self.cmap_use = None
        self.dict_overplot_rank_list = None
        self.dict_overplot_plot_type = None
        self.dict_overplot_plot_setting_list = None
        self.only_x_ax = None
        self.only_y_ax = None
        self.key_match_comb_for_plot = None
        self.key_match_comb = None
        self.key_match_comb_use_as_x = None

        self.pulse = pulse
        self.save_settings = SaveSettings()
        self.key = None
        self.dict_of_data_choices = dict_of_data_choices
        self.max_time = max_time
        self.min_time = min_time
        self.hmode_settings = hmode_settings
        self.uid = None
        self.none_error = None

    def clear_dat_and_errors(self):
        self.time_return = None
        self.data_return = None
        self.error_return = None
        self.time_error = None

    def clear_dat_and_errors_to_empty_list(self):
        self.time_return = []
        self.data_return = []
        self.error_return = []
        self.time_error = []

    def uid_check(self, correct_uid='jetppf'):
        """
        Check the correct uid and changes it if it isnt
        """
        if self.uid != correct_uid:
            ppfuid(correct_uid, "r")
            self.uid = correct_uid

    def get_elmtimes(self, elm_time, elm_data):
        """
        Load the elms between the max and min time
        """

        self.time_return_cam_rng = None
        self.data_return_cam_rng = None

        # Detect the ELMs in the signal
        mphf = 0.090  # minimum peak height fraction
        mpd = 80  # mpd is minimum distance between peaks
        # 3.E13
        # print(mphf*np.max(elm_data))
        # import matplotlib.pyplot as plt
        v = 0.0072 // np.mean(np.diff(elm_time))
        # plt.plot(elm_time, elm_data)
        # print(np.mean(np.diff(elm_time)))
        # print(v)
        # plt.show()
        # quit()
        self.data_return = detect_peaks(
            elm_data, mph=mphf * np.max(elm_data), mpd=v
        )  # mph = minimum peak height, mpd is minimum distance between peaks
        self.time_return = elm_time[self.data_return]
        self.time_error = None
        self.error_return = None
        self.time_error = None
        self.label = self.dict_of_data_choices['elmpeaks']["label"]
        self.opposite_ax_min_max = self.dict_of_data_choices['elmpeaks']["opposite_ax_min_max"]

    def get_plot_settings(self, key):
        self.list_of_plots_used = self.dict_of_data_choices[key]["list_of_plots_used"]
        self.list_of_plots_used_for_z = self.dict_of_data_choices[key]["list_of_plots_used_for_z"]
        self.key_match_comb_for_plot_z = self.dict_of_data_choices[key]["key_match_comb_for_plot_z"]
        self.key_match_comb_z_to_use_as_x = self.dict_of_data_choices[key]["key_match_comb_z_to_use_as_x"]
        self.key_match_comb_only_use_as_z = self.dict_of_data_choices[key]["key_match_comb_only_use_as_z"]
        self.beta = self.dict_of_data_choices[key]["beta"]
        self.used_model_predict = self.dict_of_data_choices[key]["used_model_predict"]
        self.units = self.dict_of_data_choices[key]["units"]
        self.time_dependent = self.dict_of_data_choices[key]["time_dependent"]
        self.marker_use = self.dict_of_data_choices[key]["marker_use"]
        self.colour_use = self.dict_of_data_choices[key]["colour_use"]
        self.alpha_use = self.dict_of_data_choices[key]["alpha_use"]
        self.linestyle_use = self.dict_of_data_choices[key]["linestyle_use"]
        self.norm_cmap_use = self.dict_of_data_choices[key]["norm_cmap_use"]
        self.cmap_use = self.dict_of_data_choices[key]["cmap_use"]
        self.dict_overplot_rank_list = self.dict_of_data_choices[key]["dict_overplot_rank_list"]
        self.dict_overplot_plot_type = self.dict_of_data_choices[key]["dict_overplot_plot_type"]
        self.dict_overplot_plot_setting_list = self.dict_of_data_choices[key]["dict_overplot_plot_setting_list"]
        self.only_x_ax = self.dict_of_data_choices[key]["only_x_ax"]
        self.only_y_ax = self.dict_of_data_choices[key]["only_y_ax"]
        self.key_match_comb_for_plot = self.dict_of_data_choices[key]["key_match_comb_for_plot"]
        self.key_match_comb = self.dict_of_data_choices[key]["key_match_comb"]
        self.key_match_comb_use_as_x = self.dict_of_data_choices[key]["key_match_comb_use_as_x"]

    def get_data(self, key, verbose=False):
        """
        Load all the data using dictionary setings
        :return:
        """
        if not self.dict_of_data_choices[key]["make_empty_bool"]:
            output_path = UsefulSetPaths.get_metadata_path_jet_non_camtype(self.pulse, self.save_settings.gen_path)
            Usefulmethods.path_check_and_make(str(output_path))
            if self.dict_of_data_choices[key]["is_sav_bool"]:
                parts = [str(self.pulse) + '_' + self.dict_of_data_choices[key]["filename_str"]]
                filename_gen = Os_path(output_path).joinpath(*parts)
                filename_gen = str(filename_gen)
            else:
                parts = [
                    str(self.pulse) + '_' + self.dict_of_data_choices[key]["data_type_str"] +
                    '_' + self.dict_of_data_choices[key]["filename_str"]
                ]
                filename_gen = Os_path(output_path).joinpath(*parts)
                filename_gen = str(filename_gen)
            if verbose:
                print(filename_gen)
            filename = filename_gen + '_saved.npy'
            if verbose:
                print(filename)

            my_file_path = Os_path(filename)

            print("input_data ", str(my_file_path), my_file_path.is_file())
            if my_file_path.is_file():  # read the npy file
                time_return, data_return, error_return, time_source, data_source, error_source = np.load(filename)
                if np.shape(time_source)[0] != np.shape(data_source)[0] and np.shape(time_source)[0] != 0:
                    if data_source is not None:
                        if np.shape(data_source)[0] > 1:
                            data_source = np.reshape(data_source, (-1,))
                        else:
                            data_source = np.reshape(data_source, (-1,))
                            data_source = np.reshape(data_source, (-1,))
                    else:
                        data_source = data_source
                    if error_source is not None:
                        if np.shape(error_source)[0] > 1:
                            error_source = np.reshape(error_source, (-1,))
                        else:
                            error_source = np.reshape(error_source, (-1,))
                            error_source = np.reshape(error_source, (-1,))
                    else:
                        error_source = error_source
                    if data_return is not None:
                        if np.shape(data_return)[0] > 1:
                            data_return = np.reshape(data_return, (-1,))
                        else:
                            data_return = np.reshape(data_return, (-1,))
                            data_return = np.reshape(data_return, (-1,))
                    else:
                        data_return = data_return
                    if error_return is not None:
                        if np.shape(error_return)[0] > 1:
                            error_return = np.reshape(error_return, (-1,))
                        else:
                            error_return = np.reshape(error_return, (-1,))
                            error_return = np.reshape(error_return, (-1,))
                    else:
                        error_return = error_return

                # If empty data then set to None
                if time_return is not None:
                    if len(time_return) < 1:
                        time_return = None
                if data_return is not None:
                    if len(data_return) < 1:
                        data_return = None
                if error_return is not None:
                    if len(error_return) < 1:
                        error_return = None
                if time_source is not None:
                    if len(time_source) < 1:
                        time_source = None
                if data_source is not None:
                    if len(data_source) < 1:
                        data_source = None
                if error_source is not None:
                    if len(error_source) < 1:
                        error_source = None

            else:
                print('no filtered and smoothed ' + self.dict_of_data_choices[key]["filename_str"] +
                      ' file, making the dataset for ' + self.dict_of_data_choices[key]["data_type_str"])
                if self.dict_of_data_choices[key]["is_sav_bool"]:
                    time_source, data_source, error_source = self.load_sav(filename_gen)
                else:
                    time_source, data_source, error_source = self.load_ppf(
                        key, x_is_data=self.dict_of_data_choices[key]["x_is_data"]
                    )
                if time_source is not None and len(data_source) >= 1:
                    # If 2d, make sure it is time by n
                    if data_source.ndim == 2:
                        if np.shape(time_source)[0] != np.shape(data_source)[0] and np.shape(time_source)[0] != 0:
                            data_source = np.swapaxes(data_source, 0, 1)
                    if error_source is not None:
                        if error_source.ndim == 2:
                            if np.shape(time_source)[0] != np.shape(error_source)[0] and np.shape(time_source)[0] != 0:
                                error_source = np.swapaxes(error_source, 0, 1)

                    # If 2d stored as 1d, change to 2d
                    if data_source.ndim <= 1:
                        if np.shape(time_source)[0] != np.shape(data_source)[0] and np.shape(time_source)[0] != 0:
                            data_source = np.reshape(data_source, (np.shape(time_source)[0], -1))
                    if error_source is not None:
                        if error_source.ndim <= 1:
                            if np.shape(time_source)[0] != np.shape(error_source)[0] and np.shape(time_source)[0] != 0:
                                error_source = np.reshape(error_source, (np.shape(time_source)[0], -1))

                    # If selected in settings, filter for elms
                    if self.dict_of_data_choices[key]["elm_filter_bool"]:
                        if self.dict_of_data_choices[key]["elm_cycle_filter_bool"]:
                            pass
                        else:
                            # Used to narrow down to very close times, must be greater than texcl_before and texcl_after
                            texcl_check = self.hmode_settings.texcl_check_jsat
                            texcl_before = self.hmode_settings.texcl_before_jsat  # Exclusion time either side of an ELM
                            texcl_after = self.hmode_settings.texcl_after_jsat  # Exclusion time either side of an ELM
                            where_elm = []

                            for time_in in time_source:
                                where_elm.append(
                                    self.check_if_camtime_is_elmtime(time_in, texcl_check, texcl_before, texcl_after)
                                )
                            time_filt = time_source[np.logical_not(where_elm)]
                            filt_data = data_source[np.logical_not(where_elm)]
                            where_non_zero = np.where(filt_data > 0.0)
                            if filt_data.ndim <= 1:
                                time_return = time_filt[where_non_zero]
                                data_return = filt_data[where_non_zero]
                                if error_source is not None:
                                    error_return = error_source[where_non_zero]
                                else:
                                    error_return = error_source
                            else:
                                time_return = time_filt
                                data_return = filt_data
                                error_return = error_source
                    else:
                        if time_source is not None:
                            time_return = np.copy(time_source)
                        else:
                            time_return = None
                        if data_source is not None:
                            data_return = np.copy(data_source)
                        else:
                            data_return = None
                        if error_source is not None:
                            error_return = np.copy(error_source)
                        else:
                            error_return = None
                    if self.dict_of_data_choices[key]["smooth_bool"]:
                        if self.dict_of_data_choices[key]["gaussian_smooth"]:
                            # Use a gaussian smoothing kernel of width
                            if len(data_return) > 50000:
                                data_return = UsefulSmoothing.gaussian_smoothing_kernel_sparse_slow(
                                    data_return, self.dict_of_data_choices[key]["gaussian_smooth_fwhm"], time_return
                                )
                            else:
                                data_return = UsefulSmoothing.gaussian_smoothing_kernel_sparse(
                                    data_return, self.dict_of_data_choices[key]["gaussian_smooth_fwhm"], time_return
                                )
                            if error_source is not None:
                                if len(error_return) > 50000:
                                    error_return = UsefulSmoothing.gaussian_smoothing_kernel_sparse_slow(
                                        error_return, self.dict_of_data_choices[key]["gaussian_smooth_fwhm"],
                                        time_return
                                    )
                                else:
                                    error_return = UsefulSmoothing.gaussian_smoothing_kernel_sparse(
                                        error_return, self.dict_of_data_choices[key]["gaussian_smooth_fwhm"],
                                        time_return
                                    )
                        else:
                            data_return = UsefulSmoothing.averaging_smooth_vect(
                                data_return, self.dict_of_data_choices[key]["percent_to_smooth"], time_return,
                                d_x=self.dict_of_data_choices[key]["time_to_smooth"]
                            )

                            if error_source is not None:
                                error_return = UsefulSmoothing.averaging_smooth_vect(
                                    error_return, self.dict_of_data_choices[key]["percent_to_smooth"], time_return,
                                    d_x=self.dict_of_data_choices[key]["time_to_smooth"]
                                )
                    # Streamline formats of output data
                    if data_source is not None:
                        if np.shape(data_source)[0] > 1:
                            data_source_s = np.reshape(data_source, (-1,))
                        else:
                            data_source_s = np.reshape(data_source, (-1,))
                            data_source_s = np.reshape(data_source_s, (-1,))
                    else:
                        data_source_s = data_source

                    if error_source is not None:
                        if np.shape(error_source)[0] > 1:
                            error_source_s = np.reshape(error_source, (-1,))
                        else:
                            error_source_s = np.reshape(error_source, (-1,))
                            error_source_s = np.reshape(error_source_s, (-1,))
                    else:
                        error_source_s = error_source
                    if data_return is not None:
                        if np.shape(data_return)[0] > 1:
                            data_return_s = np.reshape(data_return, (-1,))
                        else:
                            data_return_s = np.reshape(data_return, (-1,))
                            data_return_s = np.reshape(data_return_s, (-1,))
                    else:
                        data_return_s = data_return

                    if error_return is not None:
                        if np.shape(error_return)[0] > 1:
                            error_return_s = np.reshape(error_return, (-1,))
                        else:
                            error_return_s = np.reshape(error_return, (-1,))
                            error_return_s = np.reshape(error_return_s, (-1,))
                    else:
                        error_return_s = error_return
                    np.save(
                        str(my_file_path), np.array([time_return, data_return_s, error_return_s, time_source,
                                                     data_source_s, error_source_s])
                    )
                    data_return = data_return_s
                    error_return = error_return_s
                    data_source = data_source_s
                    error_source = error_source_s
                else:
                    np.save(str(my_file_path), np.array([[], [], [], [], [], []]))
                    time_return = None
                    data_return = None
                    error_return = None
        else:
            time_return = None
            data_return = None
            error_return = None

        # 2D data_processing
        if data_return is not None and time_return is not None:
            if np.shape(data_return)[0] > 1:
                if np.shape(data_return)[0] > np.shape(time_return)[0] and np.shape(time_return)[0] > 1:
                    data_return = np.reshape(data_return, (np.shape(time_return)[0], -1))
        if error_return is not None and time_return is not None:
            if np.shape(error_return)[0] > 1:
                if np.shape(error_return)[0] > np.shape(time_return)[0] and np.shape(time_return)[0] > 1:
                    error_return = np.reshape(error_return, (np.shape(time_return)[0], -1))
        self.label = self.dict_of_data_choices[key]["label"]
        self.opposite_ax_min_max = self.dict_of_data_choices[key]["opposite_ax_min_max"]
        self.time_return = time_return
        self.data_return = data_return
        self.time_error = None
        self.error_return = error_return
        self.time_dependent = self.dict_of_data_choices[key]["time_dependent"]
        self.no_interp = self.dict_of_data_choices[key]["no_interp"]
        self.no_raw = self.dict_of_data_choices[key]["no_raw"]
        self.no_error = self.dict_of_data_choices[key]["no_error"]

        # Error may be None, set this later when combining other data sources
        return time_return, data_return, error_return

    # def set_in_camtime_range(self):
    #     if self.time_dependent:
    #         if self.max_time is None and self.min_time is None:
    #             if self.data_return is not None:
    #                 self.time_return_cam_rng = self.time_return
    #                 self.data_return_cam_rng = self.data_return
    #         else:
    #             if self.data_return is not None:
    #                 where_within_cam_times = self.where_in_cam_times(self.time_return, self.max_time, self.min_time)
    #                 self.time_return_cam_rng = self.time_return[where_within_cam_times]
    #                 if self.data_return.ndim > 1:
    #                     self.data_return_cam_rng = self.data_return[where_within_cam_times, :]
    #                 else:
    #                     self.data_return_cam_rng = self.data_return[where_within_cam_times]
    #
    # @staticmethod
    # def where_in_cam_times(datatimes, max_time, min_time):
    #     """
    #     returns the np.where result within a max, min plus small amount of datatimes
    #     :return:
    #     """
    #     # Where more than min time and less than max time
    #     where_within_cam_times = np.where(np.logical_and(min_time < datatimes, datatimes < max_time))
    #     return where_within_cam_times
    #
    # def check_if_camtime_is_elmtime(self, time, texcl_check_overwrite=None, texcl_before_overwrite=None,
    #                                 texcl_after_overwrite=None):
    #     """
    #     Compares each camera time passed individually to the list of elm times.
    #     If within texcl then it is given the Elm = True in the dictionary
    #     """
    #     if texcl_check_overwrite is None:
    #         # used to narrow down to very close times, must be greater than texcl_before and texcl_after
    #         texcl_check = self.hmode_settings.texcl_check
    #     else:
    #         # used to narrow down to very close times, must be greater than texcl_before and texcl_after
    #         texcl_check = texcl_check_overwrite
    #     if texcl_before_overwrite is None:
    #         texcl_before = self.hmode_settings.texcl_before  # Exclusion time either side of an ELM
    #     else:
    #         texcl_before = texcl_before_overwrite  # Exclusion time either side of an ELM
    #     if texcl_after_overwrite is None:
    #         texcl_after = self.hmode_settings.texcl_after  # Exclusion time either side of an ELM
    #     else:
    #         texcl_after = texcl_after_overwrite  # Exclusion time either side of an ELM
    #
    #     if self.hmode_settings.h_mode_on:
    #         if (np.abs(self.elmtimes - time) < texcl_check).any():  # A rough check, to see if a proper check needed
    #             # Cut down range to speed checking
    #             in_elm_check_times = self.elmtimes[np.where(np.abs(self.elmtimes - time) < texcl_check)]
    #             found_one = False
    #             for i in range(len(in_elm_check_times)):
    #                 if np.logical_and(np.abs(in_elm_check_times[i] - time) < texcl_before,
    #                                   in_elm_check_times[i] - time >= 0.0):
    #                     intra_elm_bool = True
    #                     found_one = True
    #                 elif np.logical_and(np.abs(in_elm_check_times[i] - time) < texcl_after,
    #                                     in_elm_check_times[i] - time <= 0.0):
    #                     intra_elm_bool = True
    #                     found_one = True
    #             if not found_one:
    #                 intra_elm_bool = False
    #         else:
    #             intra_elm_bool = False
    #     else:
    #         intra_elm_bool = False
    #     return intra_elm_bool
    #
    # def load_ppf(self, key, x_is_data=False):
    #     self.uid_check(self.dict_of_data_choices[key]["uid"])
    #
    #     (c, m, data_source, x, time_source, error_source) = ppfget(self.pulse, self.dict_of_data_choices[key]["dda"],
    #                                                                self.dict_of_data_choices[key][
    #                                                                    "data_type_str"])
    #     if x_is_data:
    #         data_source = x
    #         error_source = 0
    #         time_source = []
    #
    #     if np.max(error_source) <= 0.0:
    #         error_source = None
    #     return time_source, data_source, error_source
    #
    # @staticmethod
    # def load_sav(filename_gen):
    #     print(filename_gen)
    #     filename = filename_gen + '.sav'
    #     my_file_path = Os_path(filename)
    #     if my_file_path.is_file():  # read the sav file
    #         sav_dict = sio.readsav(filename, python_dict=True)
    #         extracted_dict = {name: sav_dict['x'][name] for name in
    #                           sav_dict['x'].dtype.names}  # change to dictionary from recarray
    #
    #         time_source = extracted_dict['TIME'][0]
    #         n_conc_err_upper = extracted_dict['NCONC_ERR_UP'][0][1:]
    #         n_conc_err_lower = extracted_dict['NCONC_ERR_LW'][0][1:]
    #         max_err = np.array([n_conc_err_upper, n_conc_err_lower])
    #         error_source = np.max(max_err, axis=0)
    #         data_source = extracted_dict['NCONC'][0][1:]  # ignore sp2
    #         other_vals = False
    #         if other_vals:  # Not routinely used but they exist
    #             los_names_n2conc_direct = extracted_dict['LOS_NAMES'][0][1:]
    #             rvals = extracted_dict['RVALS'][0][1:]
    #
    #     else:
    #         print('no nitrogen file')
    #         time_source = None
    #         data_source = None
    #         error_source = None
    #     return time_source, data_source, error_source
